package AnnotationAndValidator;

import java.lang.annotation.*;

/**
 *
 */
public class Annotation {

    /**
     * @author Administrator
     * 校验非空字段
     */
    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @Inherited
    public @interface NotNull {
        String value();
    }

}
