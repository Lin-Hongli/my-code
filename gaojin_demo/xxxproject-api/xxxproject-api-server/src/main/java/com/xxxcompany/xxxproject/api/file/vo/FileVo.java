package com.xxxcompany.xxxproject.api.file.vo;

import com.xxxcompany.xxxproject.api.base.vo.BaseVo;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class FileVo implements BaseVo {

    @NotBlank(groups = {FileVo.DownloadFile.class}, message = "originFileName为空")
    private String originFileName;
    @NotBlank(groups = {FileVo.DownloadFile.class}, message = "fileName为空")
    private String fileName;
    private boolean keepFile;

    public interface DownloadFile {}
}
