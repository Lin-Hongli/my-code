package com.xxxcompany.xxxproject.api.student.vo;

import com.xxxcompany.xxxproject.api.base.vo.BaseVo;
import com.xxxcompany.xxxproject.api.base.vo.PageVo;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class StudentVo extends PageVo implements BaseVo {

    @NotNull(groups = {
            StudentVo.SelectById.class,
            StudentVo.UpdateById.class,
            StudentVo.DeleteById.class,
    }, message = "id不能为空")
    private Long id;
    @NotBlank(groups = {
            StudentVo.Save.class,
            StudentVo.SelectByName.class
    }, message = "name不能为空")
    private String name;

    public interface SelectById {}
    public interface Save {}
    public interface UpdateById {}
    public interface DeleteById {}
    public interface SelectByName {}
}
