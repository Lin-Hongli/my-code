package com.xxxcompany.xxxproject.api.file.service.impl;

import com.xxxcompany.xxxproject.api.base.service.impl.BaseServiceImpl;
import com.xxxcompany.xxxproject.api.file.service.IFileService;
import com.xxxcompany.xxxproject.api.file.vo.FileVo;
import com.xxxcompany.xxxproject.common.result.Result;
import com.xxxcompany.xxxproject.export.file.dto.FileDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.net.URLDecoder;

@Service("fileService")
public class FileServiceImpl extends BaseServiceImpl implements IFileService {

    @Override
    public Result download(FileVo vo) throws Exception{
        String originFileName = URLDecoder.decode(vo.getOriginFileName(), "utf-8");
        FileDTO fileDTO = new FileDTO();
        fileDTO.setOriginFileName(originFileName);
        fileDTO.setKeepFile(vo.isKeepFile());
        return exportServiceContainer.getFileExportService().download(fileDTO);
    }
}
