package com.xxxcompany.xxxproject.api.file.service;

import com.xxxcompany.xxxproject.api.base.service.IBaseService;
import com.xxxcompany.xxxproject.api.file.vo.FileVo;
import com.xxxcompany.xxxproject.common.result.Result;

public interface IFileService extends IBaseService {

    Result download(FileVo vo) throws Exception;
}
