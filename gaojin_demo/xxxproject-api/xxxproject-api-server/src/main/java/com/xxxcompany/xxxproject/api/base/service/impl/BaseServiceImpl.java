package com.xxxcompany.xxxproject.api.base.service.impl;

import com.xxxcompany.xxxproject.api.base.service.IBaseService;
import com.xxxcompany.xxxproject.api.base.service.container.rpc.ExportServiceContainer;
import com.xxxcompany.xxxproject.api.base.util.StopWatchCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("baseService")
public class BaseServiceImpl implements IBaseService {

    @Autowired
    protected StopWatchCommand stopWatchCommand;

    @Autowired
    protected ExportServiceContainer exportServiceContainer;
}
