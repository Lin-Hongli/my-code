package com.xxxcompany.xxxproject.api.base.service.container;

import com.xxxcompany.xxxproject.api.base.service.IBaseService;
import com.xxxcompany.xxxproject.api.file.service.IFileService;
import com.xxxcompany.xxxproject.api.student.service.IStudentService;
import com.xxxcompany.xxxproject.api.teacher.service.ITeacherService;
import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/*
*   本地新建的Service统一注入到这里，通过get方法提供访问入口
* */
@Component
@Getter
public class ServiceContainer {

    /*
    *   这里统一用@Resource(name="xxxService"), 不用用@Autowired，其中xxxService为对应实现类上@Service("xxxService")的xxxService
    * */
    @Resource(name="fileService")
    private IFileService fileService;
    @Resource(name="studentService")
    private IStudentService studentService;
    @Resource(name="teacherService")
    private ITeacherService teacherService;
}
