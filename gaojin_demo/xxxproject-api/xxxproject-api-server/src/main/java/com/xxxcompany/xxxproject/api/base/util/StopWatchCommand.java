package com.xxxcompany.xxxproject.api.base.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Component
@Slf4j
public class StopWatchCommand {

    private ThreadLocal<StopWatch> threadLocal = new ThreadLocal<>();

    public void watch(String watchName) {
        StopWatch stopWatch = threadLocal.get();
        if(stopWatch == null) {
            StackTraceElement stackTraceElement = new Exception().getStackTrace()[1];
            String className = stackTraceElement.getClassName();
            String methodName = stackTraceElement.getMethodName();
            String watchId = className + "." + methodName;
            stopWatch = new StopWatch(watchId);
            threadLocal.set(stopWatch);
            stopWatch.start(watchName);
        } else {
            stopWatch.stop();
            stopWatch.start(watchName);
        }
    }

    public void prettyPrint() {
        StopWatch stopWatch = threadLocal.get();
        if(stopWatch != null) {
            threadLocal.remove();
            stopWatch.stop();
            log.info("\n" + stopWatch.prettyPrint());
        }
    }
}
