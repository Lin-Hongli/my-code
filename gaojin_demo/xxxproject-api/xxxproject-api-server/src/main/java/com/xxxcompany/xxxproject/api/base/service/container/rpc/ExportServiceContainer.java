package com.xxxcompany.xxxproject.api.base.service.container.rpc;

import com.xxxcompany.xxxproject.api.file.service.IFileService;
import com.xxxcompany.xxxproject.export.file.servive.IFileExportService;
import com.xxxcompany.xxxproject.export.student.service.IStudentExportService;
import com.xxxcompany.xxxproject.export.teacher.service.ITeacherExportService;
import lombok.Getter;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

/*
 *   rpcService统一注入到这里，通过get方法提供访问入口
 * */
@Component
@Getter
public class ExportServiceContainer {

    @Reference(version = "${dubbo.provider.rpc.xxxproject.version}", application = "${dubbo.provider.rpc.xxxproject.name}", group = "${dubbo.provider.rpc.xxxproject.group}", check = false)
    private IFileExportService fileExportService;
    @Reference(version = "${dubbo.provider.rpc.xxxproject.version}", application = "${dubbo.provider.rpc.xxxproject.name}", group = "${dubbo.provider.rpc.xxxproject.group}", check = false)
    private ITeacherExportService teacherExportService;
    @Reference(version = "${dubbo.provider.rpc.xxxproject.version}", application = "${dubbo.provider.rpc.xxxproject.name}", group = "${dubbo.provider.rpc.xxxproject.group}", check = false)
    private IStudentExportService studentExportService;

}
