package com.xxxcompany.xxxproject.api.teacher.service;

import com.xxxcompany.xxxproject.api.base.service.IBaseService;
import com.xxxcompany.xxxproject.api.teacher.vo.TeacherVo;
import com.xxxcompany.xxxproject.common.result.PageResult;
import com.xxxcompany.xxxproject.common.result.Result;

public interface ITeacherService extends IBaseService {

    PageResult pageList(TeacherVo teacherVo);

    Result save(TeacherVo teacherVo);

    Result updateById(TeacherVo teacherVo);

    Result deleteById(TeacherVo teacherVo);

    Result selectByName(TeacherVo teacherVo);

    Result selectById(TeacherVo teacherVo);
}
