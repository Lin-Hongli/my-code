package com.xxxcompany.xxxproject.api.student.service;

import com.xxxcompany.xxxproject.api.base.service.IBaseService;
import com.xxxcompany.xxxproject.api.student.vo.StudentVo;
import com.xxxcompany.xxxproject.common.result.PageResult;
import com.xxxcompany.xxxproject.common.result.Result;

public interface IStudentService extends IBaseService {

    PageResult pageList(StudentVo studentVo);

    Result save(StudentVo studentVo);

    Result updateById(StudentVo studentVo);

    Result deleteById(StudentVo studentVo);

    Result selectByName(StudentVo studentVo);

    Result selectById(StudentVo studentVo);
}
