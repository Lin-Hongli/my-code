package com.xxxcompany.xxxproject.api.teacher.service.impl;

import com.xxxcompany.xxxproject.api.base.service.impl.BaseServiceImpl;
import com.xxxcompany.xxxproject.api.teacher.service.ITeacherService;
import com.xxxcompany.xxxproject.api.teacher.vo.TeacherVo;
import com.xxxcompany.xxxproject.common.result.PageResult;
import com.xxxcompany.xxxproject.common.result.Result;
import com.xxxcompany.xxxproject.export.base.dto.PageDTO;
import com.xxxcompany.xxxproject.export.teacher.dto.TeacherDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

/*
 *   不要写成@Service("teacherServiceImpl") 或 @Service("TeacherService")
 * */
@Service("teacherService")
public class TeacherServiceImpl extends BaseServiceImpl implements ITeacherService {

    @Override
    public PageResult pageList(TeacherVo teacherVo) {
        /*
         *   这里写复杂的逻辑代码
         * */
        PageDTO pageDTO = new PageDTO();
        BeanUtils.copyProperties(teacherVo, pageDTO);
        // 利用exportServiceContainer获取rpcService调用对应的方法
        return exportServiceContainer.getTeacherExportService().pageList(pageDTO);
    }

    @Override
    public Result save(TeacherVo teacherVo) {
        /*
         *   这里写复杂的逻辑代码
         * */
        TeacherDTO teacherDTO = new TeacherDTO();
        BeanUtils.copyProperties(teacherVo, teacherDTO);
        // 利用exportServiceContainer获取rpcService调用对应的方法
        return exportServiceContainer.getTeacherExportService().save(teacherDTO);
    }

    @Override
    public Result updateById(TeacherVo teacherVo) {
        /*
         *   这里写复杂的逻辑代码
         * */
        TeacherDTO teacherDTO = new TeacherDTO();
        BeanUtils.copyProperties(teacherVo, teacherDTO);
        // 利用exportServiceContainer获取rpcService调用对应的方法
        return exportServiceContainer.getTeacherExportService().updateById(teacherDTO);
    }

    @Override
    public Result deleteById(TeacherVo teacherVo) {
        /*
         *   这里写复杂的逻辑代码
         * */
        Long id = teacherVo.getId();
        // 利用exportServiceContainer获取rpcService调用对应的方法
        return exportServiceContainer.getTeacherExportService().deleteById(id);
    }

    @Override
    public Result selectByName(TeacherVo teacherVo) {
        /*
         *   这里写复杂的逻辑代码
         * */
        String name = teacherVo.getName();
        // 利用exportServiceContainer获取rpcService调用对应的方法
        return exportServiceContainer.getTeacherExportService().selectByName(name);
    }

    @Override
    public Result selectById(TeacherVo teacherVo) {
        /*
         *   这里写复杂的逻辑代码
         * */
        Long id = teacherVo.getId();
        // 利用exportServiceContainer获取rpcService调用对应的方法
        return exportServiceContainer.getTeacherExportService().selectById(id);
    }
}
