package com.xxxcompany.xxxproject.api.student.service.impl;

import com.xxxcompany.xxxproject.api.base.service.impl.BaseServiceImpl;
import com.xxxcompany.xxxproject.api.student.service.IStudentService;
import com.xxxcompany.xxxproject.api.student.vo.StudentVo;
import com.xxxcompany.xxxproject.common.result.PageResult;
import com.xxxcompany.xxxproject.common.result.Result;
import com.xxxcompany.xxxproject.export.base.dto.PageDTO;
import com.xxxcompany.xxxproject.export.student.dto.StudentDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
/*
*   不要写成@Service("studentServiceImpl") 或 @Service("StudentService")
* */
@Service("studentService")
public class StudentServiceImpl extends BaseServiceImpl implements IStudentService {

    @Override
    public PageResult pageList(StudentVo studentVo) {
        // TODO
        /*
         *   这里写复杂的逻辑代码
         * */
        PageDTO pageDTO = new PageDTO();
        BeanUtils.copyProperties(studentVo, pageDTO);
        // 利用exportServiceContainer获取rpcService调用对应的方法
        return exportServiceContainer.getStudentExportService().pageList(pageDTO);
    }

    @Override
    public Result save(StudentVo studentVo) {
        /*
         *   这里写复杂的逻辑代码
         * */
        StudentDTO studentDTO = new StudentDTO();
        BeanUtils.copyProperties(studentVo, studentDTO);
        // 利用exportServiceContainer获取rpcService调用对应的方法
        return exportServiceContainer.getStudentExportService().save(studentDTO);
    }

    @Override
    public Result updateById(StudentVo studentVo) {
        /*
         *   这里写复杂的逻辑代码
         * */
        StudentDTO studentDTO = new StudentDTO();
        BeanUtils.copyProperties(studentVo, studentDTO);
        // 利用exportServiceContainer获取rpcService调用对应的方法
        return exportServiceContainer.getStudentExportService().updateById(studentDTO);
    }

    @Override
    public Result deleteById(StudentVo studentVo) {
        /*
         *   这里写复杂的逻辑代码
         * */
        Long id = studentVo.getId();
        // 利用exportServiceContainer获取rpcService调用对应的方法
        return exportServiceContainer.getStudentExportService().deleteById(id);
    }

    @Override
    public Result selectByName(StudentVo studentVo) {
        /*
         *   这里写复杂的逻辑代码
         * */
        String name = studentVo.getName();
        // 利用exportServiceContainer获取rpcService调用对应的方法
        return exportServiceContainer.getStudentExportService().selectByName(name);
    }

    @Override
    public Result selectById(StudentVo studentVo) {
        /*
         *   这里写复杂的逻辑代码
         * */
        Long id = studentVo.getId();
        // 利用exportServiceContainer获取rpcService调用对应的方法
        return exportServiceContainer.getStudentExportService().selectById(id);
    }
}
