package com.xxxcompany.xxxproject.api.base.log;


import com.xxxcompany.xxxproject.common.id.RandomIdGenerator;
import com.xxxcompany.xxxproject.common.util.Constants;
import org.apache.dubbo.common.utils.StringUtils;
import org.apache.dubbo.rpc.*;
import org.slf4j.MDC;

/**
 * 消费者从MDC提取TraceId并传递给RPC生产者
 * @author xuhui
 */
public class ConsumerRpcTraceFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {

        String traceId = MDC.get(Constants.TRACE_ID);
        if (StringUtils.isBlank(traceId)) {
            traceId = new RandomIdGenerator().generateUuid();
        }
        RpcContext.getContext().setAttachment(Constants.TRACE_ID, traceId);

        return invoker.invoke(invocation);
    }

}
