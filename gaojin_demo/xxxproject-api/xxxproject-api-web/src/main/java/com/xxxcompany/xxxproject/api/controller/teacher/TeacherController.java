package com.xxxcompany.xxxproject.api.controller.teacher;

import com.xxxcompany.xxxproject.api.base.controller.BaseController;
import com.xxxcompany.xxxproject.api.base.vo.PageVo;
import com.xxxcompany.xxxproject.api.teacher.vo.TeacherVo;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/*
*   统一使用@RestController，不要用@Controller
* */
@RestController
@RequestMapping("/teacher")
public class TeacherController extends BaseController {

    @RequestMapping("/pageList")
    public Object pageList(@Validated(PageVo.PageList.class) @RequestBody TeacherVo teacherVo, BindingResult bindingResult){
        return getError(teacherVo, bindingResult, () -> {
            // 利用serviceContainer获取本地service调用对应的方法
            return serviceContainer.getTeacherService().pageList(teacherVo);
        });
    }

    @RequestMapping("/selectById")
    public Object selectById(@Validated(TeacherVo.SelectById.class) @RequestBody TeacherVo teacherVo, BindingResult bindingResult) {
        return getError(teacherVo, bindingResult, () -> {
            // 利用serviceContainer获取本地service调用对应的方法
            return serviceContainer.getTeacherService().selectById(teacherVo);
        });
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Object save(@Validated({TeacherVo.Save.class}) @RequestBody TeacherVo teacherVo, BindingResult bindingResult) {
        return getError(teacherVo, bindingResult, () -> {
            // 利用serviceContainer获取本地service调用对应的方法
            return serviceContainer.getTeacherService().save(teacherVo);
        });
    }

    @RequestMapping(value = "/updateById", method = RequestMethod.POST)
    public Object updateById(@Validated({TeacherVo.UpdateById.class}) @RequestBody TeacherVo teacherVo, BindingResult bindingResult) {
        return getError(teacherVo, bindingResult, () -> {
            // 利用serviceContainer获取本地service调用对应的方法
            return serviceContainer.getTeacherService().updateById(teacherVo);
        });
    }

    @RequestMapping(value = "/deleteById", method = RequestMethod.POST)
    public Object deleteById(@Validated({TeacherVo.DeleteById.class}) @RequestBody TeacherVo teacherVo, BindingResult bindingResult) {
        return getError(teacherVo, bindingResult, () -> {
            // 利用serviceContainer获取本地service调用对应的方法
            return serviceContainer.getTeacherService().deleteById(teacherVo);
        });
    }

    @RequestMapping("/selectByName")
    public Object selectByName(@Validated({TeacherVo.SelectByName.class}) @RequestBody TeacherVo teacherVo, BindingResult bindingResult) {
        return getError(teacherVo, bindingResult, () -> {
            // 利用serviceContainer获取本地service调用对应的方法
            return serviceContainer.getTeacherService().selectByName(teacherVo);
        });
    }
}
