package com.xxxcompany.xxxproject.api.base.controller;

import com.xxxcompany.xxxproject.api.base.service.container.ServiceContainer;
import com.xxxcompany.xxxproject.api.base.vo.BaseVo;
import com.xxxcompany.xxxproject.common.result.Result;
import com.xxxcompany.xxxproject.common.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
public class BaseController {

    @Autowired
    protected ServiceContainer serviceContainer;

    public  Object getError(BaseVo vo, Errors errors, SuccessCallBack callBack) {
        try {
            String error = getError(errors);
            if(error != null) {
                return Result.fail(error).set("params", StringUtil.toJSONString(vo));
            }
            return callBack.doCallBack();
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(e).set("params", StringUtil.toJSONString(vo)).remove("exceptionCode");
        }
    }

    private String getError(Errors errors) {
        if(errors.hasErrors()) {
            List<String> errorMessageList = new ArrayList<>();
            Iterator<ObjectError> iterator = errors.getAllErrors().iterator();
            while(iterator.hasNext()) {
                ObjectError error = iterator.next();
                errorMessageList.add(error.getDefaultMessage());
            }
            return StringUtils.join(errorMessageList, ",");
        }
        return null;
    }

    public interface SuccessCallBack {
        public Object doCallBack();
    }
}
