package com.xxxcompany.xxxproject.api;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author xuhui
 */
@EnableCaching
@EnableDubbo(scanBasePackages = "com.xxxcompany.xxxproject.api")
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.xxxcompany.xxxproject.api"})
public class XXXProjectApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(XXXProjectApiApplication.class, args);
    }

}
