package com.xxxcompany.xxxproject.api.controller.student;

import com.xxxcompany.xxxproject.api.base.controller.BaseController;
import com.xxxcompany.xxxproject.api.base.vo.PageVo;
import com.xxxcompany.xxxproject.api.student.vo.StudentVo;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
public class StudentController extends BaseController {

    @RequestMapping("/pageList")
    public Object pageList(@Validated(PageVo.PageList.class) @RequestBody StudentVo studentVo, BindingResult bindingResult){
        return getError(studentVo, bindingResult, () -> {
            // 利用serviceContainer获取本地service调用对应的方法
            return serviceContainer.getStudentService().pageList(studentVo);
        });
    }
    @RequestMapping("/selectById")
    public Object list(@Validated(StudentVo.SelectById.class) @RequestBody StudentVo studentVo, BindingResult bindingResult) {
        return getError(studentVo, bindingResult, () -> {
            // 利用serviceContainer获取本地service调用对应的方法
            return serviceContainer.getStudentService().selectById(studentVo);
        });
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Object save(@Validated(StudentVo.Save.class) @RequestBody StudentVo studentVo, BindingResult bindingResult) {
        return getError(studentVo, bindingResult, () -> {
            // 利用serviceContainer获取本地service调用对应的方法
            return serviceContainer.getStudentService().save(studentVo);
        });
    }

    @RequestMapping(value = "/updateById", method = RequestMethod.POST)
    public Object updateById(@Validated(StudentVo.UpdateById.class) @RequestBody StudentVo studentVo, BindingResult bindingResult) {
        return getError(studentVo, bindingResult, () -> {
            // 利用serviceContainer获取本地service调用对应的方法
            return serviceContainer.getStudentService().updateById(studentVo);
        });
    }

    @RequestMapping(value = "/deleteById", method = RequestMethod.POST)
    public Object deleteById(@Validated(StudentVo.DeleteById.class) @RequestBody StudentVo studentVo, BindingResult bindingResult) {
        return getError(studentVo, bindingResult, () -> {
            // 利用serviceContainer获取本地service调用对应的方法
            return serviceContainer.getStudentService().deleteById(studentVo);
        });
    }

    @RequestMapping("/selectByName")
    public Object selectByName(@Validated(StudentVo.SelectByName.class) @RequestBody StudentVo studentVo, BindingResult bindingResult) {
        return getError(studentVo, bindingResult, () -> {
            // 利用serviceContainer获取本地service调用对应的方法
            return serviceContainer.getStudentService().selectByName(studentVo);
        });
    }
}
