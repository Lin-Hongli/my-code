package com.xxxcompany.xxxproject.api.base.aspect;

import com.alibaba.fastjson.JSONObject;
import com.xxxcompany.xxxproject.common.result.RequestErrorResult;
import com.xxxcompany.xxxproject.common.util.ValidateUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.ContentCachingRequestWrapper;

import javax.servlet.ServletRequest;
import java.nio.charset.Charset;
import java.util.Enumeration;

@ControllerAdvice
@Slf4j
public class RequestErrorAspect {

    @ExceptionHandler(value = {Throwable.class})
    @ResponseBody
    public Object error(Throwable e, ServletRequest request) throws Exception{
        RequestErrorResult requestErrorResult = RequestErrorResult.fail(e);
        String exceptionCode = requestErrorResult.getExceptionCode(e);
        log.info(exceptionCode);
        if (request != null && request instanceof ContentCachingRequestWrapper) {
            ContentCachingRequestWrapper wrapper = (ContentCachingRequestWrapper) request;
            String content = new String(IOUtils.toByteArray(request.getInputStream()), Charset.forName(wrapper.getCharacterEncoding()));
            if(!ValidateUtil.isAvailable(content)) {
                JSONObject o = new JSONObject();
                Enumeration<String> parameterNames = wrapper.getParameterNames();
                while(parameterNames.hasMoreElements()) {
                    String name = parameterNames.nextElement();
                    String value = wrapper.getParameter(name);
                    o.put(name, value);
                }
                content = o.toString();
            }
            requestErrorResult.setRequestContent(content);
            requestErrorResult.setRequestPath(wrapper.getContextPath() + wrapper.getServletPath());
        }
        return requestErrorResult;
    }
}
