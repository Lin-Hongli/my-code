package com.xxxcompany.xxxproject.api.controller.file;

import com.xxxcompany.xxxproject.api.base.controller.BaseController;
import com.xxxcompany.xxxproject.api.file.vo.FileVo;
import com.xxxcompany.xxxproject.common.result.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.OutputStream;

@RestController
@RequestMapping("/file")
@Slf4j
public class FileController extends BaseController {

    @RequestMapping("/download")
    public void download(@Validated(FileVo.DownloadFile.class) FileVo vo, BindingResult bindingResult, HttpServletResponse response) {
        Result validateResult = (Result) getError(vo, bindingResult, () -> {
            try {
                Result result = serviceContainer.getFileService().download(vo);
                if(!result.isSuccess()) {
                    log.info(result.getError());
                    return result;
                }
                byte[] data = (byte[]) result.getData();
                String fileName = vo.getFileName();
                fileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1"); // 处理safari的乱码问题
                response.reset();
                response.setHeader("Content-disposition", String.format("attachment; filename=\"%s\"", fileName));
                response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
                response.setCharacterEncoding("utf-8");
                response.setContentType("application/OCTET-STREAM;charset=UTF-8");
                OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
                outputStream.write(data);
                outputStream.flush();
                outputStream.close();
                return Result.succ();
            } catch (Exception e) {
                e.printStackTrace();
                return Result.fail(e);
            }
        });
        if(!validateResult.isSuccess()) {
            log.info(validateResult.getError());
        }
    }
}
