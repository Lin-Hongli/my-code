package com.xxxcompany.xxxproject.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StopWatch;

@Slf4j
public class StopWatchUtil {

    private static ThreadLocal<StopWatch> threadLocal = new ThreadLocal<>();

    public static void watch(String watchName) {
        StopWatch stopWatch = threadLocal.get();
        if(stopWatch == null) {
            StackTraceElement stackTraceElement = new Exception().getStackTrace()[1];
            String className = stackTraceElement.getClassName();
            String methodName = stackTraceElement.getMethodName();
            String watchId = className + "." + methodName;
            stopWatch = new StopWatch(watchId);
            threadLocal.set(stopWatch);
            stopWatch.start(watchName);
        } else {
            stopWatch.stop();
            stopWatch.start(watchName);
        }
    }

    public static void prettyPrint() {
        StopWatch stopWatch = threadLocal.get();
        if(stopWatch != null) {
            threadLocal.remove();
            stopWatch.stop();
            log.info("\n" + stopWatch.prettyPrint());
        }
    }
}
