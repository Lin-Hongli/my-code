package com.xxxcompany.xxxproject.common.result;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.StringWriter;

@Getter
@Setter
@Slf4j
public class RequestErrorResult {

    private boolean success;
    private String error = "";
    private String requestContent;
    private String requestPath;


    public static RequestErrorResult fail(String error){
        RequestErrorResult requestErrorResult = new RequestErrorResult();
        requestErrorResult.setSuccess(false);
        requestErrorResult.setError(error);
        return requestErrorResult;
    }

    public static RequestErrorResult fail(Throwable e){
        RequestErrorResult requestErrorResult = new RequestErrorResult();
        requestErrorResult.setSuccess(false);
        requestErrorResult.setFailMessage(e);
        return requestErrorResult;
    }

    public String getExceptionCode(Throwable e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw, true));
        return sw.toString();
    }

    private void setFailMessage(Throwable throwable) {
        this.setSuccess(false);
        while(throwable.getCause() != null) {
            throwable = throwable.getCause();
        }
        String message = "系统出错，出错原因【" + throwable.getMessage() + "】";
        this.setError(message);
        log.error(message);
    }
}
