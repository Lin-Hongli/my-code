package com.xxxcompany.xxxproject.common.util;

/**
 * @author wzt on 2020/3/30.
 * @version 1.0
 */
public interface Constants {

    public final static int YES = 1;
    public final static int NO = 0;

    public static final String TRACE_ID = "traceID";

}
