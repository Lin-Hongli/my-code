package com.xxxcompany.xxxproject.common.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 *	处理数据的工具类
 */
public class DataUtil {
		
	/**
	 * 	深度复制对象src
	 */
	public static Serializable deepCopy(Serializable src) {
		ByteArrayOutputStream baos = null;
		ObjectOutputStream oos = null;
		ByteArrayInputStream bais = null;
		ObjectInputStream ois = null;
		try {
			baos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baos);
			oos.writeObject(src);
			byte[] bytes = baos.toByteArray();
			bais = new ByteArrayInputStream(bytes);
			ois = new ObjectInputStream(bais);
			return (Serializable) ois.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			IOUtil.close(ois);
			IOUtil.close(bais);
			IOUtil.close(oos);
			IOUtil.close(baos);
		}
		return null;
	}
}
