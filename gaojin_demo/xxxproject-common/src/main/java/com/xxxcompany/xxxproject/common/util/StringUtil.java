package com.xxxcompany.xxxproject.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class StringUtil {
	
	public static String getUuid() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	public static String toJSONString(Object o) {
		if(o instanceof String) {
			return (String) o;
		} else if(o instanceof Object){
			return JSON.toJSONStringWithDateFormat(JSONArray.toJSON(o), "yyyy-MM-dd HH:mm:ss");
		}
		return null;
	}

	public static String upperFirstWord(String str) {
		if(!ValidateUtil.isAvailable(str)) {return null;}
		char firstWord = str.charAt(0);
		if(firstWord == "_".charAt(0)) {return str;}
		return Character.isUpperCase(firstWord) ?
				str :
				new StringBuffer().append(Character.toUpperCase(firstWord)).append(str.substring(1)).toString();
	}

	public static String spaceToNull(String str) {
		return "".equals(str) ? null : str;
	}

	public static Object nullToSpace(String atm) {
		return null == atm ? "" : atm;
	}
}
