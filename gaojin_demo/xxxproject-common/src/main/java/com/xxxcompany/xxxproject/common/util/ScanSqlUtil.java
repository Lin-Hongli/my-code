package com.xxxcompany.xxxproject.common.util;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.core.io.ClassPathResource;

import java.io.InputStream;

public class ScanSqlUtil {

	public static String getSqlText(Object obj, String fileName, String key)throws Exception {
		InputStream inputStream = null;
		SAXReader reader = null;
		try {
			String filePath = obj.getClass().getResource("").getPath();
			filePath = filePath.substring(filePath.indexOf("com"));
			inputStream = new ClassPathResource(filePath.concat(fileName)).getInputStream();
			reader = new SAXReader();
			Document document = reader.read(inputStream);
			Element rootElm = document.getRootElement();
			return rootElm.element(key).getTextTrim();
		} finally {
			IOUtil.close(inputStream);
		}
	}
}