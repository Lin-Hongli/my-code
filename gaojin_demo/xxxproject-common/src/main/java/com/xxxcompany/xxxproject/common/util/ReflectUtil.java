package com.xxxcompany.xxxproject.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ReflectUtil {

    public static void set(Object o, String fieldName, Object value) throws Exception {
        Field field = o.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(o, value);
    }

    public static Object get(Object o, String fieldName) throws Exception{
        Field field = o.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        return field.get(o);
    }

    public static void coverNotNullProperties(Object sourceData, Object targetData) throws Exception {
        Class<?> sourceDataClass = sourceData.getClass();
        Class<?> targetDataClass = targetData.getClass();
        coverNotNullProperties(sourceDataClass, sourceData, targetDataClass, targetData);
    }

    private static void coverNotNullProperties(Class<?> sourceDataClass, Object sourceData, Class<?> targetDataClass, Object targetData) throws Exception {
        Field[] sourceDataClassDeclaredFields = sourceDataClass.getDeclaredFields();
        for (int i = 0; i < sourceDataClassDeclaredFields.length; i++) {
            Field sourceDataClassDeclaredField = sourceDataClassDeclaredFields[i];
            if(Modifier.isStatic(sourceDataClassDeclaredField.getModifiers())) {continue;}
            if(Modifier.isPublic(sourceDataClassDeclaredField.getModifiers())) {continue;}
            sourceDataClassDeclaredField.setAccessible(true);
            Object sourceDataClassDeclaredFieldValue = sourceDataClassDeclaredField.get(sourceData);
            if(sourceDataClassDeclaredFieldValue != null) {
                setValue(sourceDataClassDeclaredField, sourceDataClassDeclaredFieldValue, targetDataClass, targetData);
            }
        }
        Class<?> sourceDataClassSuperclass = sourceDataClass.getSuperclass();
        if(sourceDataClassSuperclass != null) {
            coverNotNullProperties(sourceDataClassSuperclass, sourceData, targetDataClass, targetData);
        }
    }

    private static void setValue(Field sourceDataClassDeclaredField, Object sourceDataClassDeclaredFieldValue, Class<?> targetDataClass, Object targetData) throws Exception {
        String sourceDataClassDeclaredFieldName = sourceDataClassDeclaredField.getName();
        Class<?> sourceDataClassDeclaredFieldType = sourceDataClassDeclaredField.getType();
        Field[] targetDataClassDeclaredFields = targetDataClass.getDeclaredFields();
        for (int j = 0; j < targetDataClassDeclaredFields.length; j++) {
            Field targetDataClassDeclaredField = targetDataClassDeclaredFields[j];
            if(Modifier.isStatic(targetDataClassDeclaredField.getModifiers())) {continue;}
            if(Modifier.isPublic(targetDataClassDeclaredField.getModifiers())) {continue;}
            String targetDataClassDeclaredFieldName = targetDataClassDeclaredField.getName();
            Class<?> targetDataClassDeclaredFieldType = targetDataClassDeclaredField.getType();
            if(sourceDataClassDeclaredFieldName.equals(targetDataClassDeclaredFieldName) && sourceDataClassDeclaredFieldType == targetDataClassDeclaredFieldType) {
                targetDataClassDeclaredField.setAccessible(true);
                targetDataClassDeclaredField.set(targetData, sourceDataClassDeclaredFieldValue);
                return;
            }
        }
        Class<?> targetDataClassSuperclass = targetDataClass.getSuperclass();
        if(targetDataClassSuperclass != null) {
            setValue(sourceDataClassDeclaredField, sourceDataClassDeclaredFieldValue, targetDataClassSuperclass, targetData);
        }
    }
}
