package com.xxxcompany.xxxproject.common.util;

import java.io.Closeable;
import java.io.IOException;

public class IOUtil {

	public static void close(Closeable closeable) {
		try {
			if(closeable != null) {
				closeable.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
