package com.xxxcompany.xxxproject.common.result;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Slf4j
public class Result implements Serializable {

    private boolean success;
    private String error = "";
    private Object data;
    private Map<String, Object> map = new HashMap<String, Object>();

    private Result() {}

    public static Result succ(){
        Result result = new Result();
        result.setSuccess(true);
        return result;
    }

    public static Result succ(Object data){
        Result result = Result.succ();
        result.setData(data);
        return result;
    }

    public static Result fail(String error){
        Result result = new Result();
        result.setSuccess(false);
        result.setError(error);
        return result;
    }

    public static Result fail(Throwable e){
        Result result = new Result();
        result.setSuccess(false);
        result.setFailMessage(e);
        String exceptionCode = result.getExceptionCode(e);
        log.info(exceptionCode);
        return result.set("exceptionCode", exceptionCode);
    }

    public Result set(String key, Object obj) {
        this.map.put(key, obj);
        return this;
    }

    public Result remove(String key) {
        this.map.remove(key);
        return this;
    }

    private String getExceptionCode(Throwable e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw, true));
        return sw.toString();
    }

    private void setFailMessage(Throwable throwable) {
        this.setSuccess(false);
        while(throwable.getCause() != null) {
            throwable = throwable.getCause();
        }
        String message = "系统出错，出错原因【" + throwable.getMessage() + "】";
        this.setError(message);
        log.error(message);
    }
}
