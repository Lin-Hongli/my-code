package com.xxxcompany.xxxproject.common.result;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.List;

@Getter
@Setter
@Slf4j
public class PageResult implements Serializable {

    private boolean success;
    private String error = "";
    private List<?> dataList;
    private long total;
    private Integer currentPage;
    private Integer pageSize;

    private PageResult() {}

    public static PageResult succ() {
        PageResult pageResult = new PageResult();
        pageResult.setSuccess(true);
        return pageResult;
    }

    public static PageResult fail(String error){
        PageResult pageResult = new PageResult();
        pageResult.setError(error);
        return pageResult;
    }

    public static PageResult fail(Exception e){
        PageResult pageResult = new PageResult();
        pageResult.setSuccess(false);
        pageResult.setFailMessage(e);
        String exceptionCode = pageResult.getExceptionCode(e);
        log.info(exceptionCode);
        return pageResult;
    }

    private void setFailMessage(Throwable throwable) {
        this.setSuccess(false);
        while(throwable.getCause() != null) {
            throwable = throwable.getCause();
        }
        String message = "系统出错，出错原因【" + throwable.getMessage() + "】";
        this.setError(message);
        log.error(message);
    }

    private String getExceptionCode(Throwable e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw, true));
        return sw.toString();
    }
}
