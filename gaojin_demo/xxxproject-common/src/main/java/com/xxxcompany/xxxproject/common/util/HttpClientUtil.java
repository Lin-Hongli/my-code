package com.xxxcompany.xxxproject.common.util;

import java.io.File;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/* 
 * 利用HttpClient进行post请求的工具类 
 */
public class HttpClientUtil {

	public static String doPostByUrlEncoded(String url, Map<String, String> map, String charset) throws Exception{
		HttpClient httpClient = new SSLClient();
		HttpPost httpPost = new HttpPost(url);
		// 设置参数
		List<NameValuePair> list = new ArrayList<NameValuePair>();
		Iterator<Entry<String, String>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, String> elem = iterator.next();
			list.add(new BasicNameValuePair(elem.getKey(), elem.getValue()));
		}
		if (list.size() > 0) {
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list, charset);
			httpPost.setEntity(entity);
		}
		HttpResponse response = httpClient.execute(httpPost);
		return getData(response, charset);
	}

	public static String doPostByMultipart(String url, Map<String, File> fileMap, String charset) throws Exception {
		HttpClient httpClient = new SSLClient();
		HttpPost httpPost = new HttpPost(url);
		// 上传文件
		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		for(Entry<String, File> entry : fileMap.entrySet()) {
			String fieldName = entry.getKey();
			File file = entry.getValue();
			FileBody fb = new FileBody(file);
			builder.addPart(fieldName, fb);
		}
		httpPost.setEntity(builder.build());
		HttpResponse response = httpClient.execute(httpPost);
		return getData(response, charset);
	}

	public static String doPostByJson(String url, String json, String charset) throws Exception {
		HttpClient httpClient = new SSLClient();
		HttpPost httpPost = new HttpPost(url);
		StringEntity jsonEntity = new StringEntity(json, ContentType.APPLICATION_JSON);
		httpPost.setEntity(jsonEntity);
		HttpResponse response = httpClient.execute(httpPost);
		return getData(response, charset);
	}

	public static String doPostByRaw(String url, Map<String, String> headData, Map<String, Object> bodyData, String charset) throws Exception{
		HttpClient httpClient = new SSLClient();
		HttpPost httpPost = new HttpPost(url);
		// 设置head参数
		if(ValidateUtil.isAvailable(headData)) {
			Iterator<Entry<String, String>> iterator = headData.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, String> entry = iterator.next();
				String key = entry.getKey();
				String value = entry.getValue();
				httpPost.addHeader(key,value);
			}
		}
		// 设置body参数
		if(ValidateUtil.isAvailable(bodyData)) {
			List<NameValuePair> list = new ArrayList<NameValuePair>();
			StringEntity stringEntity = new StringEntity(bodyData.toString(), charset);
			httpPost.setEntity(stringEntity);
			if (list.size() > 0) {
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list, charset);
				httpPost.setEntity(entity);
			}
		}
		HttpResponse response = httpClient.execute(httpPost);
		return getData(response, charset);
	}

	private static String getData(HttpResponse response, String charset) throws Exception {
		if (response != null) {
			HttpEntity resEntity = response.getEntity();
			if (resEntity != null) {
				return EntityUtils.toString(resEntity, charset);
			}
		}
		return null;
	}

	static class SSLClient extends DefaultHttpClient {
		public SSLClient() throws Exception{
			super();
			SSLContext ctx = SSLContext.getInstance("TLS");
			X509TrustManager tm = new X509TrustManager() {
				@Override
				public void checkClientTrusted(X509Certificate[] chain,
											   String authType) throws CertificateException {
				}
				@Override
				public void checkServerTrusted(X509Certificate[] chain,
											   String authType) throws CertificateException {
				}
				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}
			};
			ctx.init(null, new TrustManager[]{tm}, null);
			SSLSocketFactory ssf = new SSLSocketFactory(ctx,SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			ClientConnectionManager ccm = this.getConnectionManager();
			SchemeRegistry sr = ccm.getSchemeRegistry();
			sr.register(new Scheme("https", 443, ssf));
		}
	}
}