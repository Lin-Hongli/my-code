package com.xxxcompany.xxxproject.common.base;

import com.alibaba.fastjson.annotation.JSONField;
import com.xxxcompany.xxxproject.common.id.RandomIdGenerator;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class BaseDTO extends RandomIdGenerator implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private Object createrId;
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date modifyTime;
    private Object modifierId;
    private boolean isDelete;
}
