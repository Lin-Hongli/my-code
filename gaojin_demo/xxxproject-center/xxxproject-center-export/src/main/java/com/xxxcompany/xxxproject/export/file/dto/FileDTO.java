package com.xxxcompany.xxxproject.export.file.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class FileDTO implements Serializable {

    private boolean keepFile;
    private String originFileName;
}
