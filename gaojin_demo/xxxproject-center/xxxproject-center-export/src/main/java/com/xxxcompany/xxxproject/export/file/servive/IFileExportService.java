package com.xxxcompany.xxxproject.export.file.servive;

import com.xxxcompany.xxxproject.common.result.Result;
import com.xxxcompany.xxxproject.export.file.dto.FileDTO;

public interface IFileExportService {

    Result download(FileDTO fileDTO);
}
