package com.xxxcompany.xxxproject.export.teacher.dto;

import com.xxxcompany.xxxproject.common.base.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeacherDTO extends BaseDTO {

    private String name;

}
