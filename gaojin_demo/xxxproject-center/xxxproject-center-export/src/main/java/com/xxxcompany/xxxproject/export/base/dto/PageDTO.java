package com.xxxcompany.xxxproject.export.base.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class PageDTO implements Serializable {
    private Integer pageSize;
    private String filter;
    private Integer currentPage;
    private String orderBy;
}
