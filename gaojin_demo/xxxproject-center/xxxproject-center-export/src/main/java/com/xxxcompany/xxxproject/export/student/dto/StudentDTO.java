package com.xxxcompany.xxxproject.export.student.dto;

import com.xxxcompany.xxxproject.common.base.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentDTO extends BaseDTO {

    private String name;

}
