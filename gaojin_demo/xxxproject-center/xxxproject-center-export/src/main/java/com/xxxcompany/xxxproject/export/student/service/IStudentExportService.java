package com.xxxcompany.xxxproject.export.student.service;

import com.xxxcompany.xxxproject.common.result.PageResult;
import com.xxxcompany.xxxproject.common.result.Result;
import com.xxxcompany.xxxproject.export.base.dto.PageDTO;
import com.xxxcompany.xxxproject.export.student.dto.StudentDTO;

import java.io.Serializable;

public interface IStudentExportService {

    PageResult pageList(PageDTO vo);

    Result save(StudentDTO studentDTO);

    Result selectById(Serializable id);

    Result updateById(StudentDTO studentDTO);

    Result deleteById(Serializable id);

    Result selectByName(String name);
}
