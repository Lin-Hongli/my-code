package com.xxxcompany.xxxproject.export.teacher.service;

import com.xxxcompany.xxxproject.common.result.PageResult;
import com.xxxcompany.xxxproject.common.result.Result;
import com.xxxcompany.xxxproject.export.base.dto.PageDTO;
import com.xxxcompany.xxxproject.export.teacher.dto.TeacherDTO;

import java.io.Serializable;

public interface ITeacherExportService {

    PageResult pageList(PageDTO vo);

    Result save(TeacherDTO teacherDTO);

    Result selectById(Serializable id);

    Result updateById(TeacherDTO teacherDTO);

    Result deleteById(Serializable id);

    Result selectByName(String name);
}
