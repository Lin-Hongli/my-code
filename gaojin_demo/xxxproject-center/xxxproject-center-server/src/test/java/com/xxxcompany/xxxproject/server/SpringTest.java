package com.xxxcompany.xxxproject.server;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xxxcompany.xxxproject.common.util.ScanSqlUtil;
import com.xxxcompany.xxxproject.common.util.StringUtil;
import com.xxxcompany.xxxproject.server.student.entity.Student;
import com.xxxcompany.xxxproject.server.student.service.IStudentService;
import com.xxxcompany.xxxproject.server.teacher.entity.Teacher;
import com.xxxcompany.xxxproject.server.teacher.service.ITeacherService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

@SpringBootTest(classes = {XXXProjectCenterApplication.class})
public class SpringTest {

    @Autowired
    private IStudentService studentService;
    @Autowired
    private ITeacherService teacherService;
    @Test
    public void test(){
        for (int i = 0; i < 100; i++) {
            Student student = new Student();
            student.setName("name" + i);
            student.setCreaterId(StringUtil.getUuid());
            student.setCreateTime(new Date());
            studentService.save(student);
        }

    }

    @Test
    public void test1(){
        Teacher teacher = new Teacher();
        teacher.setName("name1");
        teacher.setCreaterId(StringUtil.getUuid());
        teacher.setCreateTime(new Date());
        teacherService.save(teacher);
        System.err.println(teacher.getId());

    }

    @Test
    public void test2(){
        Page<Student> studentPage = new Page<>(5,20);
        IPage<Student> page = studentService.page(studentPage);
        System.err.println(page.getTotal());
        page.getRecords().forEach(student -> System.err.println(StringUtil.toJSONString(student)));

    }

    @Test
    public void test3(){
        Student student = studentService.getById(1320032348840030209L);
        student.setName("name0");
        studentService.updateById(student);
    }

    @Test
    public void test4(){
        System.err.println(studentService.removeById(1320032348840030209L));
    }

    @Test
    public void test6() throws Exception{
        String sql = ScanSqlUtil.getSqlText(studentService, "sql.xml", "list");
        System.err.println(sql);
    }
}
