package com.xxxcompany.xxxproject.server.base.log;


import com.xxxcompany.xxxproject.common.id.RandomIdGenerator;
import com.xxxcompany.xxxproject.common.util.Constants;
import org.apache.dubbo.common.utils.StringUtils;
import org.apache.dubbo.rpc.*;
import org.slf4j.MDC;

/**
 * 生产者被调用时从上下文获取TraceId
 * @author xuhui
 */
public class ProviderRpcTraceFilter implements Filter {

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {

        String traceId = RpcContext.getContext().getAttachment(Constants.TRACE_ID);
        if (StringUtils.isBlank(traceId)) {
            traceId = new RandomIdGenerator().generateUuid();
        }

        //设置日志traceId变量
        MDC.put(Constants.TRACE_ID, traceId);

        try {
            return invoker.invoke(invocation);
        }finally {
            MDC.remove(Constants.TRACE_ID);
        }
    }

}
