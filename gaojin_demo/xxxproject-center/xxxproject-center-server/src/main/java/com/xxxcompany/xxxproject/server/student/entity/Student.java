package com.xxxcompany.xxxproject.server.student.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xxxcompany.xxxproject.server.base.entity.BaseVersionEntity;
import lombok.Getter;
import lombok.Setter;

@TableName("student")
@Getter
@Setter
public class Student extends BaseVersionEntity {

    private String name;
}
