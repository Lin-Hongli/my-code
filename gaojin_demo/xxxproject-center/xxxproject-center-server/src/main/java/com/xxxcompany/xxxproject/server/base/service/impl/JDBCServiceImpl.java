package com.xxxcompany.xxxproject.server.base.service.impl;


import com.xxxcompany.xxxproject.common.result.PageResult;
import com.xxxcompany.xxxproject.common.util.FilterParser;
import com.xxxcompany.xxxproject.common.util.ValidateUtil;
import com.xxxcompany.xxxproject.export.base.dto.PageDTO;
import com.xxxcompany.xxxproject.server.base.service.JDBCService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Slf4j
@Service("jdbcService")
@Transactional
public class JDBCServiceImpl implements JDBCService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public PageResult pageList(String sql, PageDTO pageDTO) throws Exception {
        String filter = pageDTO.getFilter();
        String queryStr = FilterParser.getCondition(filter);
        String where = ValidateUtil.isAvailable(queryStr) ? queryStr : "";
        sql = sql.replace("${filter}", where);
        String orderBy = pageDTO.getOrderBy();
        if(!ValidateUtil.isAvailable(orderBy)) {
            orderBy = " id asc ";
        }
        String order = " order by " + orderBy;
        Integer currentPage = pageDTO.getCurrentPage();
        Integer pageSize = pageDTO.getPageSize();
        if(pageSize <= 0) {
            pageSize = 10;
        }
        String countSql = " select count(*) from (" + sql + ") xxx ";
        String dataSql = "select * from (" + sql + order + ") x limit " + (currentPage - 1) + ", " + pageSize;
        Integer total = jdbcTemplate.queryForObject(countSql, Number.class).intValue();
        List<Map<String, Object>> dataList = jdbcTemplate.queryForList(dataSql);

        PageResult pageResult = PageResult.succ();
        pageResult.setCurrentPage(currentPage);
        pageResult.setPageSize(pageSize);
        pageResult.setTotal(total);
        pageResult.setDataList(dataList);
        return pageResult;
    }
}
