package com.xxxcompany.xxxproject.server.student.service.rpcimpl;


import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.xxxcompany.xxxproject.common.result.PageResult;
import com.xxxcompany.xxxproject.common.result.Result;
import com.xxxcompany.xxxproject.common.util.*;
import com.xxxcompany.xxxproject.export.base.dto.PageDTO;
import com.xxxcompany.xxxproject.export.student.dto.StudentDTO;
import com.xxxcompany.xxxproject.export.student.service.IStudentExportService;
import com.xxxcompany.xxxproject.server.base.rpc.service.BaseExportService;
import com.xxxcompany.xxxproject.server.student.entity.Student;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;

@Slf4j
@Service(version = "${dubbo.provider.rpc.version}", timeout = 60000, retries = -1)
public class StudentExportServiceImpl extends BaseExportService implements IStudentExportService {

    @Override
    public PageResult pageList(PageDTO pageDTO) {
        try {
            StopWatchUtil.watch("获取sql");
            String sql = ScanSqlUtil.getSqlText(this, "sql.xml", "list");
            StopWatchUtil.watch("查询数据");
            PageResult pageResult = serviceContainer.getJdbcService().pageList(sql, pageDTO);
            StopWatchUtil.prettyPrint();
            return pageResult;
        } catch (Exception e) {
            e.printStackTrace();
            return PageResult.fail(e);
        }
    }

    @Override
    public Result save(StudentDTO studentDTO) {
        try {
            Student student = new Student();
            BeanUtils.copyProperties(studentDTO, student);
            student.setCreateTime(new Date());
            student.setCreaterId(StringUtil.getUuid());
            serviceContainer.getStudentService().save(student);
            return Result.succ(student);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(e);
        }
    }

    @Override
    public Result selectById(Serializable id) {
        try {
            Student student = serviceContainer.getStudentService().getById(id);
            if(student == null) {
                return Result.succ();
            }
            StudentDTO studentDTO = new StudentDTO();
            BeanUtils.copyProperties(student, studentDTO);
            return Result.succ(studentDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(e);
        }
    }

    @Override
    public Result updateById(StudentDTO studentDTO) {
        try {
            Serializable id = studentDTO.getId();
            if(!ValidateUtil.isAvailable(id)) {
                return Result.fail("ID为空");
            }
            Student student = serviceContainer.getStudentService().getById(id);
            if(student == null) {
                String message = "查询不到数据【ID = " + studentDTO.getId() + "】";
                log.info(message);
                return Result.fail(message);
            }
            // 把studentDTO的属性覆盖到student, 空值不覆盖
            ReflectUtil.coverNotNullProperties(studentDTO, student);
            student.setModifierId(StringUtil.getUuid());
            student.setModifyTime(new Date());
            // 指定name可以设置为null,其余字段空值不覆盖
            student.setName(studentDTO.getName());
            LambdaUpdateWrapper<Student> wrapper = new LambdaUpdateWrapper<>();
            wrapper.eq(Student::getId, student.getId()).set(Student::getName, student.getName());
            serviceContainer.getStudentService().update(student, wrapper);
            return Result.succ(student);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(e);
        }
    }

    @Override
    public Result deleteById(Serializable id) {
        try {
            return serviceContainer.getStudentService().removeById(id) ? Result.succ() : Result.fail("删除失败");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(e);
        }
    }

    @Override
    public Result selectByName(String name) {
        try {
            return Result.succ(serviceContainer.getStudentService().selectByName(name));
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(e);
        }
    }
}
