package com.xxxcompany.xxxproject.server.base.service;

import com.xxxcompany.xxxproject.common.result.PageResult;
import com.xxxcompany.xxxproject.export.base.dto.PageDTO;

public interface JDBCService {

    PageResult pageList(String sql, PageDTO pageDTO) throws Exception;
}
