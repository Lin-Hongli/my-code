package com.xxxcompany.xxxproject.server.student.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxxcompany.xxxproject.server.student.dao.StudentMapper;
import com.xxxcompany.xxxproject.server.student.entity.Student;
import com.xxxcompany.xxxproject.server.student.service.IStudentService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service("studentService")
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {


    @Override
    public List<Map<String, Object>> selectByName(String name) {
        return baseMapper.selectByName(name);
    }
}
