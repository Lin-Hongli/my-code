package com.xxxcompany.xxxproject.server.base.service.container;

import com.xxxcompany.xxxproject.server.base.service.JDBCService;
import com.xxxcompany.xxxproject.server.student.service.IStudentService;
import com.xxxcompany.xxxproject.server.teacher.service.ITeacherService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Getter
public class ServiceContainer {

    @Resource(name= "jdbcService")
    private JDBCService jdbcService;
    @Autowired
    private IStudentService studentService;
    @Autowired
    private ITeacherService teacherService;
}
