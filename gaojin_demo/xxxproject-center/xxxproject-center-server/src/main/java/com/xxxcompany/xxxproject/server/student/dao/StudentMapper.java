package com.xxxcompany.xxxproject.server.student.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxxcompany.xxxproject.server.student.entity.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

public interface StudentMapper extends BaseMapper<Student> {

    List<Map<String, Object>> selectByName(String name);
}
