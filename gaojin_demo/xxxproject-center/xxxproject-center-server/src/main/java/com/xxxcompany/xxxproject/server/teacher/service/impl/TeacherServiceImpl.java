package com.xxxcompany.xxxproject.server.teacher.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xxxcompany.xxxproject.server.teacher.dao.TeacherMapper;
import com.xxxcompany.xxxproject.server.teacher.entity.Teacher;
import com.xxxcompany.xxxproject.server.teacher.service.ITeacherService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service("teacherService")
@Transactional
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements ITeacherService {

    @Override
    public List<Map<String, Object>> selectByName(String name) {
        return baseMapper.selectByName(name);
    }
}
