package com.xxxcompany.xxxproject.server.base.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.xxxcompany.xxxproject.common.base.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseEntity extends BaseDTO {

    @TableLogic
    private boolean isDelete;
}
