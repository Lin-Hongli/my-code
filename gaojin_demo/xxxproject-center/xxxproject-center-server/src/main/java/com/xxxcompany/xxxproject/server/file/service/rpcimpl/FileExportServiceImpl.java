package com.xxxcompany.xxxproject.server.file.service.rpcimpl;

import com.xxxcompany.xxxproject.common.result.Result;
import com.xxxcompany.xxxproject.common.util.FileUtil;
import com.xxxcompany.xxxproject.common.util.ValidateUtil;
import com.xxxcompany.xxxproject.export.file.dto.FileDTO;
import com.xxxcompany.xxxproject.export.file.servive.IFileExportService;
import com.xxxcompany.xxxproject.server.base.rpc.service.BaseExportService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;

@Service(version = "${dubbo.provider.rpc.version}", timeout = 60000, retries = -1)
public class FileExportServiceImpl extends BaseExportService implements IFileExportService {

    @Value("${file.temp.path}")
    private String tempPath;

    @Override
    public Result download(FileDTO fileDTO) {
        File file = null;
        try {
            Result result = getTempPath();
            if(!result.isSuccess()) {
                return result;
            }
            String tempPath = (String) result.getData();
            String originFileName = fileDTO.getOriginFileName();
            file = new File(tempPath.concat(originFileName));
            if(!file.exists()) {
                return Result.fail("目标文件【" + originFileName + "】不存在");
            }
            byte[] bytes = FileUtil.readFile(file);
            if(bytes == null) {
                return Result.fail("读取目标文件【" + originFileName + "】失败");
            }
            return Result.succ(bytes);
        } finally {
            if(!fileDTO.isKeepFile() && file != null && file.exists()) {
                file.delete();
            }
        }
    }

    protected Result getTempPath() {
        if(!ValidateUtil.isAvailable(tempPath)) {
            return Result.fail("临时文件目录为空，请联系管理员指定");
        }
        File file = new File(tempPath);
        if(!file.exists()) {
            if(!file.mkdirs()) {
                return Result.fail("创建临时文件目录失败");
            }
        }
        return Result.succ(tempPath);
    }
}
