package com.xxxcompany.xxxproject.server.teacher.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xxxcompany.xxxproject.server.teacher.entity.Teacher;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface TeacherMapper extends BaseMapper<Teacher> {

    public List<Map<String, Object>> selectByName(String name);
}
