package com.xxxcompany.xxxproject.server.teacher.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.xxxcompany.xxxproject.server.base.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@TableName("teacher")
@Getter
@Setter
public class Teacher extends BaseEntity {

    private String name;
}
