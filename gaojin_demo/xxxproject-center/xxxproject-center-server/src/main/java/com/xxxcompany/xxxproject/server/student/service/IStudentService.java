package com.xxxcompany.xxxproject.server.student.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxcompany.xxxproject.server.student.entity.Student;

import java.util.List;
import java.util.Map;

public interface IStudentService extends IService<Student> {

    List<Map<String, Object>> selectByName(String name);

}
