package com.xxxcompany.xxxproject.server.base.entity;

import com.baomidou.mybatisplus.annotation.Version;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseVersionEntity extends BaseEntity {
    @Version
    private Integer version;
}
