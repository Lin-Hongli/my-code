package com.xxxcompany.xxxproject.server.teacher.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xxxcompany.xxxproject.server.teacher.entity.Teacher;

import java.util.List;
import java.util.Map;

public interface ITeacherService extends IService<Teacher> {

    List<Map<String, Object>> selectByName(String name);
}
