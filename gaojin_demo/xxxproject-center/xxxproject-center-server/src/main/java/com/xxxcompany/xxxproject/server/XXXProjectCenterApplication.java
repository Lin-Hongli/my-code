package com.xxxcompany.xxxproject.server;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableDubbo(scanBasePackages = "com.xxxcompany.xxxproject.server")
@EnableAutoConfiguration
@EnableTransactionManagement
@ComponentScan(basePackages = {"com.xxxcompany.xxxproject.server"})
public class XXXProjectCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(XXXProjectCenterApplication.class, args);
    }

}
