package com.xxxcompany.xxxproject.server.teacher.service.rpcimpl;


import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.xxxcompany.xxxproject.common.result.PageResult;
import com.xxxcompany.xxxproject.common.result.Result;
import com.xxxcompany.xxxproject.common.util.*;
import com.xxxcompany.xxxproject.export.base.dto.PageDTO;
import com.xxxcompany.xxxproject.export.teacher.dto.TeacherDTO;
import com.xxxcompany.xxxproject.export.teacher.service.ITeacherExportService;
import com.xxxcompany.xxxproject.server.base.rpc.service.BaseExportService;
import com.xxxcompany.xxxproject.server.teacher.entity.Teacher;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.BeanUtils;

import java.io.Serializable;
import java.util.Date;

@Slf4j
@Service(version = "${dubbo.provider.rpc.version}", timeout = 60000, retries = -1)
public class TeacherExportServiceImpl extends BaseExportService implements ITeacherExportService {

    @Override
    public PageResult pageList(PageDTO pageDTO) {
        try {
            StopWatchUtil.watch("获取sql");
            String sql = ScanSqlUtil.getSqlText(this, "sql.xml", "list");
            StopWatchUtil.watch("查询数据");
            PageResult pageResult = serviceContainer.getJdbcService().pageList(sql, pageDTO);
            StopWatchUtil.prettyPrint();
            return pageResult;
        } catch (Exception e) {
            e.printStackTrace();
            return PageResult.fail(e);
        }
    }

    @Override
    public Result save(TeacherDTO teacherDTO) {
        try {
            Teacher teacher = new Teacher();
            BeanUtils.copyProperties(teacherDTO, teacher);
            teacher.setCreateTime(new Date());
            teacher.setCreaterId(StringUtil.getUuid());
            serviceContainer.getTeacherService().save(teacher);
            return Result.succ(teacher);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(e);
        }
    }

    @Override
    public Result selectById(Serializable id) {
        try {
            Teacher teacher = serviceContainer.getTeacherService().getById(id);
            if(teacher == null) {
                return Result.succ();
            }
            TeacherDTO teacherDTO = new TeacherDTO();
            BeanUtils.copyProperties(teacher, teacherDTO);
            return Result.succ(teacherDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(e);
        }
    }

    @Override
    public Result updateById(TeacherDTO teacherDTO) {
        try {
            Serializable id = teacherDTO.getId();
            if(!ValidateUtil.isAvailable(id)) {
                return Result.fail("ID为空");
            }
            Teacher teacher = serviceContainer.getTeacherService().getById(id);
            if(teacher == null) {
                String message = "查询不到数据【ID = " + teacherDTO.getId() + "】";
                log.info(message);
                return Result.fail(message);
            }
            // 把teacherDTO的属性覆盖到student, 空值不覆盖
            ReflectUtil.coverNotNullProperties(teacherDTO, teacher);
            teacher.setModifierId(StringUtil.getUuid());
            teacher.setModifyTime(new Date());
            // 指定name可以设置为null,其余字段空值不覆盖
            teacher.setName(teacherDTO.getName());
            LambdaUpdateWrapper<Teacher> wrapper = new LambdaUpdateWrapper<>();
            wrapper.eq(Teacher::getId, teacher.getId()).set(Teacher::getName, teacher.getName());
            serviceContainer.getTeacherService().update(teacher, wrapper);
            return Result.succ(teacher);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(e);
        }
    }

    @Override
    public Result deleteById(Serializable id) {
        try {
            return serviceContainer.getTeacherService().removeById(id) ? Result.succ() : Result.fail("删除失败");
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(e);
        }
    }

    @Override
    public Result selectByName(String name) {
        return Result.succ(serviceContainer.getTeacherService().selectByName(name));
    }
}
