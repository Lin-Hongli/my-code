package com.lhl.mycode;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;

import java.util.ArrayList;
import java.util.List;
/**
 * MyBatis plus Generator
 * 代码生成器 AutoGenerator 是 MyBatis-Plus 的代码生成器，
 * 通过 AutoGenerator 可以快速生成 Entity、Mapper、Mapper XML、Service、Controller 等各个模块的代码，极大的提升了开发效率。
 * 特别说明:
 * 自定义模板有哪些可用参数？Github Gitee AbstractTemplateEngine 类中方法 getObjectMap 返回 objectMap 的所有值都可用。
 */
public class MyBatisPlusGenerator {
    private  static final String B2C_LOCALHOST="192.168.127.236";
    private  static final String B2C_USERNAME="root";
    private  static final String B2C_PASSWORD="root";

    private  static final String DODO_UAT_LOCALHOST="192.168.126.241";
    private  static final String DODO_UAT_USERNAME="dodo_uat";
    private  static final String DODO_UAT_PASSWORD="dodo_uat3edc$rfV";


    private static final String  localhost = DODO_UAT_LOCALHOST; //数据库地址
    private static final String userName = DODO_UAT_USERNAME;  //数据库账号
    private static final String password = DODO_UAT_PASSWORD;  //数据库密码

    private static String dataBase = "dododg_uatt";  //***数据库名
    private static String table = "wx_personnel_info";  //***表名，多个英文逗号分割
    private static String tablePre = "";  //***表前缀（去除后Entity名）

    private static String entityName="WxPersonnelInfo";//***Entity实体名

    private static String projectName="allGeneratorCode";//***子模块名
    //private static String packageName = "com.lhl.myTools";   //包名(组织名)
    private static String packageName = "";   //包名(组织名)
    //private static String modelName = "code";  //功能模块名
    private static String modelName = "";  //功能模块名
    private static String author = "Lin Hongli";  //注释：@author 作者

    private static boolean isOverride = true;//是否覆盖


    private static String url = "jdbc:mysql://"+localhost+":3306/" + dataBase + "?useUnicode=true&useSSL=false&characterEncoding=utf8&serverTimezone=GMT&allowPublicKeyRetrieval=true";
    private static String driverName = "com.mysql.cj.jdbc.Driver";
    private static String projectPath = System.getProperty("user.dir");

    //1. 全局配置
    private static GlobalConfig glConfig=globalConfig();
    //2. 数据源配置
    private static DataSourceConfig dsConfig = dataSourceConfig();
    //3.包名策略
    private static PackageConfig pkConfig = packageConfig();
    //4.策略配置
    private static StrategyConfig stConfig = strategyConfig();
    //5、自定义配置
    private static InjectionConfig cfg = injectionConfig();
    //6、自定义输出模板
    private static TemplateConfig templateConfig = templateConfig(null);
    public static void main(String[] args) {
        //整合配置
        new AutoGenerator().setGlobalConfig(glConfig)
                .setDataSource(dsConfig)
                //.setTemplateEngine(new FreemarkerTemplateEngine())//设置模板引擎 //@EqualsAndHashCode(callSuper = true)
                .setTemplateEngine(new VelocityTemplateEngine()) //@EqualsAndHashCode(callSuper = false)
                .setPackageInfo(pkConfig)
                .setStrategy(stConfig)
                .setCfg(cfg)
                .setTemplate(templateConfig)
                .execute();
    }


    /**
     * 1、全局配置
     */
    private static GlobalConfig globalConfig() {
        return  new GlobalConfig()
                .setActiveRecord(false)//是否让entity继承Model<T>使其拥有CRUD功能
                .setAuthor(author) //作者
                .setOutputDir(projectPath +"/"+projectName+  "/src/main/java")
                //.setOutputDir("E:\\workspace\\MyBatis-demo\\src\\main\\java")  //生成路径
                .setOpen(false)
                .setDateType(DateType.ONLY_DATE)//表字段与实体属性对应的数据类型
                .setFileOverride(isOverride)//是否文件覆盖，如果多次
                .setServiceName("I%sService") //设置生成的service接口名首字母是否为I
                .setIdType(IdType.AUTO) //主键策略
                .setBaseResultMap(true)
                .setBaseColumnList(true);
        //.setSwagger2(true); //实体属性 Swagger2 注解
    }

    /**
     * 2、数据源配置
     */
    private static DataSourceConfig dataSourceConfig() {
        return  new DataSourceConfig()
                //.setSchemaName("public")
                .setDbType(DbType.MYSQL)
                .setUrl(url)
                .setDriverName(driverName)
                .setUsername(userName)
                .setPassword(password);
    }

    /**
     * 3、包名配置
     */
    private static PackageConfig packageConfig() {

        return  new PackageConfig()
                .setParent(packageName)//父包名（组织名）
                .setController("controller")
                .setEntity("entity")
                .setService("service")
                .setMapper("dao")
                .setXml("mapper")
                .setModuleName(modelName);
    }

    /**
     * 4、生成策略配置
     */
    private static StrategyConfig strategyConfig() {
        return  new StrategyConfig()
                .setCapitalMode(true) // 全局大写命名
                .setNaming(NamingStrategy.underline_to_camel) // 数据库表映射到实体的命名策略
                .setTablePrefix(tablePre)// 表前缀
                .setColumnNaming(NamingStrategy.underline_to_camel)
                .setEntityLombokModel(true)
                .setRestControllerStyle(true)//@RestController
                //.setSuperEntityClass("你自己的父类实体,没有就不用设置!") // Entity的公共父类
                //.setSuperControllerClass("你自己的父类控制器,没有就不用设置!") // Controller的公共父类
                .setEntityTableFieldAnnotationEnable(true) //@TableName("xxx") @TableField("xxx")
                .setChainModel(true) //@Accessors(chain = true)
                // 写于父类中的公共字段
                .setSuperEntityColumns("id")
                .setInclude(table) //生成的表名，多个用逗号隔开
                .setControllerMappingHyphenStyle(true);


    }



    /**
     * 5、自定义配置
     */
    private static InjectionConfig injectionConfig() {

        // 自定义配置
        InjectionConfig cfg =new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // String templatePath = "/templates/mapper.xml.ftl"; // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.vm"; // 如果模板引擎是 velocity

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<FileOutConfig>();
        // 自定义配置会被优先输出

        focList.add(new FileOutConfig(templatePath) {

            @Override
            public String outputFile(TableInfo tableInfo) {
                tableInfo.setEntityName(entityName);
                tableInfo.setServiceName("I"+entityName+"Service");
                tableInfo.setServiceImplName(entityName+"ServiceImpl");
                tableInfo.setControllerName(entityName+"Controller");
                tableInfo.setMapperName(entityName+"Dao");


                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                //return projectPath + "/src/main/resources/mapper/" + pkConfig.getModuleName()+ "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
                return projectPath +"/"+projectName+ "/src/main/resources/mapper/"+ tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });

        /*
        cfg.setFileCreate(new IFileCreate() {
            @Override
            public boolean isCreate(ConfigBuilder configBuilder, FileType fileType, String filePath) {
                // 判断自定义文件夹是否需要创建
                checkDir("调用默认方法创建的目录，自定义目录用");
                if (fileType == FileType.MAPPER) {
                    // 已经生成 mapper 文件判断存在，不想重新生成返回 false
                    return !new File(filePath).exists();
                }
                // 允许生成模板文件
                return true;
            }
        });
        */
        cfg.setFileOutConfigList(focList);
        return cfg;
    };

    /**
     * 6、自定义输出模板
     */
    private static TemplateConfig templateConfig(String xml) {
        return new TemplateConfig()
                //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
                // templateConfig.setEntity("templates/entity2.java");
                // templateConfig.setService();
                // templateConfig.setController();
                .setXml(xml);
    }
}
/**
 * <!--依赖包-->
 * <!--mysql驱动-->
 * <dependency>
 *     <groupId>mysql</groupId>
 *     <artifactId>mysql-connector-java</artifactId>
 *     <version>8.0.16</version>
 * </dependency>
 * <!--模板引擎-->
 *
 * <dependency>
 *     <groupId>org.apache.velocity</groupId>
 *     <artifactId>velocity-engine-core</artifactId>
 *     <version>2.2</version>
 * </dependency>
 * 或
 * <dependency>
 *     <groupId>org.freemarker</groupId>
 *     <artifactId>freemarker</artifactId>
 *     <version>2.3.30</version>
 * </dependency>
 * <!--mybatis-plus-generator-->
 * <dependency>
 *     <groupId>com.baomidou</groupId>
 *     <artifactId>mybatis-plus-generator</artifactId>
 *     <version>3.4.1</version>
 * </dependency>
 *
 */