#!/bin/bash
envNames=(uat cp1 sit te1 prd)
for envName in ${envNames[@]}
do
        for envDir in `find . -type d -name "$envName"`
        do
                #file=./backend-src/main_module/rouces/prd/aaa/application.properties
                #file=./sit/aaa/application.properties
                for file in `find $envDir -type f ! -name "."`
                do
                        envDirPath=`dirname $envDir`
                        #autocfgFilePath编译后配置文件所在目录，文件与envName同级
                        autocfgFilePath=`echo $file| sed "s/\/$envName\//\//g"`
                        autocfgFilePath=`dirname $autocfgFilePath`
                        #fileName=配置文件名称
                        fileName=`basename $file`
                        mkdir -p $autocfgFilePath
                        cp $file $autocfgFilePath/$fileName.$envName.autocfg
                        if [ "$?" == 0 ]
                        then
                                echo "INFO: 执行cp $file $autocfgFilePath/$fileName.$envName.autocfg 成功."
                        else
                                echo "ERROR: 执行cp $file $autocfgFilePath/$fileName.$envName.autocfg 失败."
                                exit 1
                        fi
                        autocfgFilePath=''
                        file=''
                done
                envDir=''
        done
done

