package com.liby.promotion.service.web;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = {MybatisPlusAutoConfiguration.class})
@EnableDubbo(multipleConfig = true, scanBasePackages = "com.liby")
@ComponentScan("com.liby")
@EnableTransactionManagement
public class
PromotionWebApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(PromotionWebApplication.class);
    }

    public static void main(String[] args) {
        //META-INF/dubbo/internal/com.alibaba.dubbo.rpc.filter
        try {
            SpringApplication.run(PromotionWebApplication.class, args);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
