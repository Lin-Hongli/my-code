package com.liby.promotion.material.export.dto;



import lombok.Data;
import java.io.Serializable;
import java.util.Date;

@Data
public class MaterialConfigurationDto implements Serializable {


    private Integer id;

    /**
     * 头图url
     */
    private String headImgUrl;

    /**
     * 中部图url
     */
    private String middleImgUrl;

    /**
     * 背景图url
     */
    private String backImgUrl;

    /**
     * 脚图url
     */
    private String footImgUrl;

    /**
     * 类型 1秒杀 2众筹 3大礼包
     */
    private Integer type;

    private Date createTime;

    private Date updateTime;


}