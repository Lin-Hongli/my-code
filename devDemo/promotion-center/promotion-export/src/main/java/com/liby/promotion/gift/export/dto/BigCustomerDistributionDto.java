package com.liby.promotion.gift.export.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 客户分配量
 * </p>
 *
 * @author weiyunhui
 * @since 2020-12-02
 */
@Data
public class BigCustomerDistributionDto implements Serializable {

    /**
     * 客户配量表 id
     */
    private Integer id;

    /**
     * 删除状态 0正常 1删除
     */
    private Integer deleteStatus;

    /**
     * 插入时间
     */
    private String addTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 客户编码
     */
    private String customerCode;

    /**
     * 客户名称
     */
    private String customerName;

    /**
     * 分配量
     */
    private Integer distributedVolum;

    /**
     * 备注
     */
    private String remark;

    /**
     * 活动id
     */
    private String promId;


}
