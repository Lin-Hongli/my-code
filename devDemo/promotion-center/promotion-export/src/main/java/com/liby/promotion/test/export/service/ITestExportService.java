package com.liby.promotion.test.export.service;

import com.liby.promotion.common.service.rpc.IBaseExportService;
import com.liby.promotion.test.export.dto.TestDTO;

public interface ITestExportService extends IBaseExportService<TestDTO> {


}
