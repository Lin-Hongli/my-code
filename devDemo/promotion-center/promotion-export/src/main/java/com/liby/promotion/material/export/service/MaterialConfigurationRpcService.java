package com.liby.promotion.material.export.service;


import com.liby.promotion.material.export.dto.MaterialConfigurationDto;

/**
 * @param
 * @author lcd
 * @description 素材配置rpc
 * @date
 * @return
 **/
public interface MaterialConfigurationRpcService {

    //获取素材配置
    MaterialConfigurationDto get(Integer type);

    //保存修改
    void saveOrUpdateConfig(MaterialConfigurationDto dto);
}
