package com.liby.promotion.gift.export.service;


import com.liby.promotion.gift.export.dto.BigPromotionInfoDto;

import java.util.List;

/**
 * <p>
 * 大礼包活动表 服务类
 * </p>
 *
 * @author weiyunhui
 * @since 2020-11-19
 */
public interface BigPromotionInfoExportService {
    /**
     * 大礼包新增
     *
     * @param bigPromotionInfoDto
     * @return
     */
    int saveGift(BigPromotionInfoDto bigPromotionInfoDto);

    /**
     * 根据ID查询大礼包
     *
     * @param id
     * @return
     */
    BigPromotionInfoDto queryById(Long id);

    /**
     * 修改大礼包上下架状态
     *
     * @param id
     * @param status
     * @return
     */
    int updateStatusById(Long id, int status);

    /**
     * 查询所有大礼包数据
     *
     * @param pageNp
     * @param pageSize
     * @param bigPromotionInfoDto
     * @return
     */
    List<BigPromotionInfoDto> selectAll(int pageNp, int pageSize, BigPromotionInfoDto bigPromotionInfoDto);

}
