package com.liby.promotion.gift.export.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 省区分配量
 * </p>
 *
 * @author weiyunhui
 * @since 2020-12-02
 */

@Data
public class BigProvincialDistributionDto implements Serializable {
    /**
     * 省区分配量表 id
     */
    private Integer id;

    /**
     * 省区编码
     */
    private String provincialCode;

    /**
     * 省区名称
     */
    private String provincialName;

    /**
     * 分配量
     */
    private Integer distributedVolum;

    /**
     * 备注
     */
    private String remark;

    /**
     * 活动id 
     */
    private String promId;

    /**
     * 删除状态 0正常 1删除
     */
    private Integer deleteStatus;

    /**
     * 插入时间
     */
    private String addTime;

    /**
     * 更新时间
     */
    private String updateTime;

}
