package com.liby.promotion.gift.export.dto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 大礼包活动星级配置表
 * </p>
 *
 * @author weiyunhui
 * @since 2020-12-02
 */
@Data
public class BigPromGradeQuotaDto  implements Serializable {

    private Integer id;

    /**
     * 活动ID
     */
    private String promId;

    /**
     * 星级(1-7)
     */
    private Integer starLevel;

    /**
     * 配额
     */
    private Long quota;

    private String createTime;



}
