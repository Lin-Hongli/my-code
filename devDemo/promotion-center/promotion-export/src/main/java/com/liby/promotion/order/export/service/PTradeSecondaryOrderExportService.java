package com.liby.promotion.order.export.service;

import com.github.pagehelper.PageInfo;
import com.liby.promotion.order.export.dto.QureyOrderRequest;
import com.liby.promotion.order.export.dto.QureyOrderResponse;

import java.util.List;

public interface PTradeSecondaryOrderExportService {

    /**
     * 查询大礼包订单
     * @param qureyOrderRequest
     * @return
     */
    PageInfo<QureyOrderResponse> qureyGiftPacksOrder(QureyOrderRequest qureyOrderRequest);

    /**
     * 导出大礼包订单
     * @param qureyOrderRequest
     * @return
     */
    List<QureyOrderResponse> exportGiftPacksOrder(QureyOrderRequest qureyOrderRequest);

}
