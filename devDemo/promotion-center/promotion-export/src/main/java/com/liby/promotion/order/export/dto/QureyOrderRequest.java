package com.liby.promotion.order.export.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class QureyOrderRequest implements Serializable {

    /**
     * 订单号字符串(格式xxxxxxxxxx,xxxxxxxxxx)
     */
    private String orderStr;

    /**
     * 订单号集合
     */
    private List<String> orderList;

    /**
     * 产品编码字符串
     */
    private String itemIdStr;

    /**
     * 产品编码集合
     */
    private List<String> itemIdList;

    /**
     * 产品名称
     */
    private String itemName;

    /**
     * 售达方编码，格式（xxxxxxxx,xxxxxxxx,xxxxxxxx）
     */
    private String customerCodeStr;

    /**
     * 售达方编码集合
     */
    private List<String> customerCodeList;

    /**
     * 状态集合
     */
    private List<Byte> orderStatusList;

    /**
     * 赠品编码
     */
    private String giftItemIdStr;

    /**
     * 赠品编码集合
     */
    private List<String> giftItemIdList;

    /**
     * 赠品名称
     */
    private String giftItemName;

    /**
     * 订单同步状态
     */
    private List<Byte> orderSyncSucc;

    private Integer pageIndex;

    private Integer pageSize;

}
