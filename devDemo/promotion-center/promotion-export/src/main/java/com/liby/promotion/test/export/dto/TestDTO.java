package com.liby.promotion.test.export.dto;

import com.liby.promotion.common.dto.BaseDTO;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TestDTO extends BaseDTO {

    private String name;
}
