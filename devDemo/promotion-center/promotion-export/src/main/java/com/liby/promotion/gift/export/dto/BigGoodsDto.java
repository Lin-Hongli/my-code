package com.liby.promotion.gift.export.dto;


import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author weiyunhui
 * @since 2020-11-19
 */
@Data
@ToString
public class BigGoodsDto implements Serializable {


    private Long id;

    /**
     * 商品名称
     */
    private String name;



    /**
     * 商品80码
     */
    private String materialCode;

    /**
     * 商品单位(CAR: 件, EA:个)
     */
    private String unit;

    /**
     * 商品价格
     */
    private BigDecimal price;

    /**
     * 现价格
     */
    private BigDecimal newPrice;

    /**
     * 活动id
     */
    private Long promId;

    /**
     * 商品类型（1：商品，2：赠品）
     */
    private int type;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建人id
     */
    private Long createrId;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 更新人id
     */
    private Long updaterId;

    /**
     * 删除标识（1：已删除，0：未删除）
     */
    private Boolean isDelete;
}
