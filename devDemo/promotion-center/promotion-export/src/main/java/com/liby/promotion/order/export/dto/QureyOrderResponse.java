package com.liby.promotion.order.export.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class QureyOrderResponse implements Serializable {

    /**
     * 大礼包订单号
     */
    private String orderCode;

    /**
     * 活动Id
     */
    private String promId;

    /**
     * 活动标题
     */
    private String promTitle;

    /**
     * 活动描述
     */
    private String promDesc;

    /**
     * 产品编码
     */
    private String productCode;

    /**
     * 产品名称
     */
    private String productName;

    /**
     * 购买数量
     */
    private Integer purchaseQuantity;

    /**
     * 单价
     */
    private BigDecimal purchaseUnitPrice;

    /**
     * 订单总金额
     */
    private BigDecimal orderAmount;

    /**
     * 开始时间
     */
    private Date promotionStartTime;

    /**
     * 下单时间
     */
    private Date portalOrderDate;

    /**
     * 支付时间
     */
    private Date payTime;

    /**
     * 订单有效时间
     */
    private Integer orderEffectiveTime;

    /**
     * 下单人编码
     */
    private String createdUserCode;

    /**
     * 售达方编码
     */
    private String soldToPartyCode;

    /**
     * 订单状态(0.未支付；1.待处理；2.审核中；3.已审核；4部分交货；5.已交货；6.已签收；7.已取消；8.待审核)
     */
    private Integer orderStatus;

    /**
     * 订单同步状态
     */
    private Integer sapSyncFlag;

    /**
     * 赠品编码
     */
    private String giftItemId;

    /**
     * 赠品名称
     */
    private String giftItemName;

    /**
     * 赠品数量
     */
    private Integer giftCount;

    /**
     * 赠品图片url
     */
    private String giftImgUrl;


}
