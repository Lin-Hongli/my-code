package com.liby.promotion.gift.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liby.promotion.gift.service.BigCustomerDistributionService;
import com.liby.promotion.gift.service.dao.mybatisPlus.BigCustomerDistributionDao;
import com.liby.promotion.gift.service.entity.BigCustomerDistribution;
import com.liby.promotion.gift.service.entity.BigProvincialDistribution;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 客户分配量 服务实现类
 * </p>
 *
 * @author weiyunhui
 * @since 2020-12-02
 */
@Service
public class BigCustomerDistributionServiceImpl extends ServiceImpl<BigCustomerDistributionDao, BigCustomerDistribution> implements BigCustomerDistributionService {
    @Resource
    BigCustomerDistributionDao bigCustomerDistributionDao;

    @Override
    public int insertCustomerAll(List<BigCustomerDistribution> bigCustomerDistributions) {
        for (BigCustomerDistribution bigProvincialDistribution :
                bigCustomerDistributions) {
            bigCustomerDistributionDao.insert(bigProvincialDistribution);
        }
        return 1;
    }
}
