package com.liby.promotion.order.service.dao.mybatisPlus;

import com.liby.promotion.order.service.entity.BigOrderItem;
import com.liby.promotion.order.service.entity.BigOrderItemExample;
import com.liby.promotion.order.service.entity.BigOrderItemKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BigOrderItemMapper {
    int countByExample(BigOrderItemExample example);

    int deleteByExample(BigOrderItemExample example);

    int deleteByPrimaryKey(BigOrderItemKey key);

    int insert(BigOrderItem record);

    int insertSelective(BigOrderItem record);

    List<BigOrderItem> selectByExample(BigOrderItemExample example);

    BigOrderItem selectByPrimaryKey(BigOrderItemKey key);

    int updateByExampleSelective(@Param("record") BigOrderItem record, @Param("example") BigOrderItemExample example);

    int updateByExample(@Param("record") BigOrderItem record, @Param("example") BigOrderItemExample example);

    int updateByPrimaryKeySelective(BigOrderItem record);

    int updateByPrimaryKey(BigOrderItem record);
}