package com.liby.promotion.gift.service.impl.rpc;

import com.alibaba.dubbo.config.annotation.Service;
import com.liby.promotion.common.redis.key.KeyGenerateor;
import com.liby.promotion.common.redis.service.RedisCacheService;
import com.liby.promotion.common.redis.service.RedisUtil;
import com.liby.promotion.common.util.DateUtils;
import com.liby.promotion.gift.export.dto.*;
import com.liby.promotion.gift.export.service.BigPromotionInfoExportService;
import com.liby.promotion.gift.service.BigCustomerDistributionService;
import com.liby.promotion.gift.service.BigPromGradeQuotaService;
import com.liby.promotion.gift.service.BigPromotionInfoService;
import com.liby.promotion.gift.service.BigProvincialDistributionService;
import com.liby.promotion.gift.service.entity.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service(provider = "dubboProvider")
public class BigPromotionInfoExportServiceImpl extends RedisCacheService<BigPromotionInfoDto> implements BigPromotionInfoExportService {
    @Resource
    private BigPromotionInfoService bigPromotionInfoService;

    @Resource
    private BigCustomerDistributionService bigCustomerDistributionService;

    @Resource
    private BigPromGradeQuotaService bigPromGradeQuotaService;

    @Resource
    private BigProvincialDistributionService bigProvincialDistributionService;

    public BigPromotionInfoExportServiceImpl() {
    }

    @Override
    @Autowired
    @Qualifier("redisUtil2")
    public void setRedisUtil(RedisUtil redisUtil) {
        super.setRedisUtil(redisUtil);
    }

    private String createGiftCode() {
        synchronized (BigPromotionInfoService.class) {
            Long increment = redisUtil.increment(KeyGenerateor.REDIS_SEQUANCE_KEY, 1);
            if (increment > 999) {
                redisUtil.set(KeyGenerateor.REDIS_SEQUANCE_KEY, 0);
                increment = 0L;
            }
            return "DLB"+ DateUtils.format(new Date(), "yyyyMMdd") + String.format("%03d", increment);
    }
    }

    @Override
    public int saveGift(BigPromotionInfoDto bigPromotionInfoDto) {
        try {
            if(bigPromotionInfoDto.getId()==null){
                //生成DLB+当前日期与三位数的随机ID
                bigPromotionInfoDto.setCode(createGiftCode());
            }
            BigPromotionInfo bigPromotionInfo = new BigPromotionInfo();
            //BigPromotionInfoDto转BigPromotionInfo
            BeanUtils.copyProperties(bigPromotionInfoDto, bigPromotionInfo);
            //参数DTO商品对象集合
            List<BigGoodsDto> bigGoodsDtoList = bigPromotionInfoDto.getBigGoodsList();
            //参数DTO客户配额对象集合
            List<BigCustomerDistributionDto> customerDistributionDtos = bigPromotionInfoDto.getCustomerDistributionDtos();
            //参数DTO星级配额对象集合
            List<BigPromGradeQuotaDto> promGradeQuotaDtos = bigPromotionInfoDto.getPromGradeQuotaDtos();
            //参数DTO省区对象集合
            List<BigProvincialDistributionDto> provincialDistributionDtos = bigPromotionInfoDto.getProvincialDistributionDtos();

            //预转的对象集合
            List<BigGoods> bigGoodsList = new ArrayList<>();
            //预转的客户集合
            List<BigCustomerDistribution> bigCustomerDistributions = new ArrayList<>();
            //预转的商品集合
            List<BigPromGradeQuota> bigPromGradeQuotas = new ArrayList<>();
            //预转的省区集合
            List<BigProvincialDistribution> bigProvincialDistributions = new ArrayList<>();
            //循环将商品DTO转型
            for (BigGoodsDto obj :
                    bigGoodsDtoList) {
                BigGoods bigGoods = new BigGoods();
                BeanUtils.copyProperties(obj, bigGoods);
                bigGoodsList.add(bigGoods);
            }
            bigPromotionInfo.setBigGoodsList(bigGoodsList);

            //循环将客户配额DTO转型
            for (BigCustomerDistributionDto obj :
                    customerDistributionDtos) {
                BigCustomerDistribution bigCustomerDistribution = new BigCustomerDistribution();
                BeanUtils.copyProperties(obj, bigCustomerDistribution);
                bigCustomerDistributions.add(bigCustomerDistribution);
            }
            bigPromotionInfo.setCustomerDistributions(bigCustomerDistributions);

            //循环将星级配额DTO转型
            for (BigPromGradeQuotaDto obj :
                    promGradeQuotaDtos) {
                BigPromGradeQuota bigPromGradeQuota = new BigPromGradeQuota();
                BeanUtils.copyProperties(obj, bigPromGradeQuota);
                bigPromGradeQuotas.add(bigPromGradeQuota);
            }
            bigPromotionInfo.setPromGradeQuotas(bigPromGradeQuotas);

            //循环将省区配额DTO转型
            for (BigProvincialDistributionDto obj :
                    provincialDistributionDtos) {
                BigProvincialDistribution bigProvincialDistribution = new BigProvincialDistribution();
                BeanUtils.copyProperties(obj, bigProvincialDistribution);
                bigProvincialDistributions.add(bigProvincialDistribution);
            }
            bigPromotionInfo.setProvincialDistributions(bigProvincialDistributions);

            //执行活动insert，同时新增商品从表数据
            int row = bigPromotionInfoService.saveGift(bigPromotionInfo);
            return row;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public BigPromotionInfoDto queryById(Long id) {
        try {
            BigPromotionInfoDto bigPromotionInfoDto = new BigPromotionInfoDto();
            //根据id查询这当前活动
            BigPromotionInfo bigPromotionInfo = bigPromotionInfoService.queryById(id);
            //将bigPromotionInfo转换成DTO类型
            BeanUtils.copyProperties(bigPromotionInfo, bigPromotionInfoDto);
            //获取bigPromotionInfo的子商品
            List<BigGoods> bigGoodsList = bigPromotionInfo.getBigGoodsList();
            //创建子商品转DTO所需的List
            List<BigGoodsDto> bigGoodsDtoList = new ArrayList<>();
            //循环转换
            for (BigGoods bigGoods :
                    bigGoodsList) {
                BigGoodsDto bigGoodsDto = new BigGoodsDto();
                BeanUtils.copyProperties(bigGoods, bigGoodsDto);
                bigGoodsDtoList.add(bigGoodsDto);
            }
            bigPromotionInfoDto.setBigGoodsList(bigGoodsDtoList);
            return bigPromotionInfoDto;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public int updateStatusById(Long id, int status) {
        try {
            bigPromotionInfoService.updateStatusById(id, status);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public List<BigPromotionInfoDto> selectAll(int pageNp, int pageSize, BigPromotionInfoDto bigPromotionInfoDto) {
        try {
            BigPromotionInfo bigPromotionInfo = new BigPromotionInfo();
            BeanUtils.copyProperties(bigPromotionInfoDto, bigPromotionInfo);
            List<BigPromotionInfo> bigPromotionInfos = bigPromotionInfoService.selectAll(pageNp, pageSize, bigPromotionInfo);
            List<BigPromotionInfoDto> bigPromotionInfoDtos = new ArrayList<>();
            for (BigPromotionInfo bigPromotionInfoFor :
                    bigPromotionInfos) {
                BigPromotionInfoDto bigPromotionInfoDto1 = new BigPromotionInfoDto();
                BeanUtils.copyProperties(bigPromotionInfoFor, bigPromotionInfoDto1);
                bigPromotionInfoDtos.add(bigPromotionInfoDto1);
            }
            return bigPromotionInfoDtos;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
