package com.liby.promotion.gift.service.dao.mybatisPlus;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liby.promotion.gift.service.entity.BigPromotionInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 大礼包活动表 Mapper 接口
 * </p>
 *
 * @author weiyunhui
 * @since 2020-11-19
 */
public interface BigPromotionInfoDao extends BaseMapper<BigPromotionInfo> {
    BigPromotionInfo queryById(@Param("id") Long id);

    int updateStatusById(@Param("id") Long id, @Param("status") int status);

    List<BigPromotionInfo> selectAll(@Param("obj") BigPromotionInfo bigPromotionInfo);
}
