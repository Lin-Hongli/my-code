package com.liby.promotion.material.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liby.promotion.material.service.entity.MaterialConfiguration;

/**
 * @param
 * @author lcd
 * @description
 * @date
 * @return
 **/
public interface MaterialConfigurationService extends IService<MaterialConfiguration> {
}
