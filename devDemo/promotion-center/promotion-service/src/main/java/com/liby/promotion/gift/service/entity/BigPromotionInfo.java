package com.liby.promotion.gift.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 大礼包活动表
 * </p>
 *
 * @author weiyunhui
 * @since 2020-11-19
 */
@Data
@ToString
public class BigPromotionInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 活动编码
     */
    private String code;

    /**
     * 活动类型 (1.大礼包)
     */
    private int type;

    /**
     * 大礼包类型 1.立乐包，2.立乐套包
     */
    private int gifType;

    /**
     * 大礼包名称
     */
    private String name;

    /**
     * 大礼包图片地址
     */
    private String imgUrl;

    /**
     * 大礼包价格
     */
    private BigDecimal totalPrice;

    /**
     * 大礼包库存
     */
    private Integer stock;

    /**
     * 大礼包可用库存
     */
    private Integer residueStock;

    /**
     * 已支付数量(支付库存)
     */
    private Integer dealStock;

    /**
     * 是否买赠
     */
    private Boolean isBuyGif;

    /**
     * 是否执行物料替换
     */
    private Boolean isCanMaterialReplace;

    /**
     *经销商最大购买数量（配额）
     */
    private Integer maxBuyCnt;
    /**
     * 配额方式
     */
    private int quotaMode;

    /**
     * 发布方式 0、手工上下架 1、定时上下架
     */
    private int deployType;

    /**
     * 上架时间
     */
    private Date deployUpTime;

    /**
     * 下架时间
     */
    private Date deployDownTime;

    /**
     * 促销状态
     */
    private int status;
//
//    /**
//     * 描述
//     */
//    private String desc;
//
//    /**
//     * 订单支付有效时长 /秒
//     */
//    private Integer validPayDuration;
//
//    /**
//     * 活动时长 /分钟
//     */
//    private Integer duration;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建人id
     */
    private Long createrId;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 更新人id
     */
    private Long updaterId;

    /**
     * 删除标识（1：已删除，0：未删除）
     */
    private Boolean isDelete;
    /**
     * 关联的商品表
     */
    @TableField(exist = false)
    private List<BigGoods> bigGoodsList = new ArrayList<>();

    /**
     * 客户分配量
     */
    @TableField(exist = false)
    private List<BigCustomerDistribution> customerDistributions = new ArrayList<>();

    /**
     * 星级分配量
     */
    @TableField(exist = false)
    private List<BigPromGradeQuota> promGradeQuotas = new ArrayList<>();

    /**
     * 省区分配量
     */
    @TableField(exist = false)
    private List<BigProvincialDistribution> provincialDistributions = new ArrayList<>();


}
