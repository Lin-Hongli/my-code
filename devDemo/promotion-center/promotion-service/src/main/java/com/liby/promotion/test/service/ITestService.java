package com.liby.promotion.test.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liby.promotion.test.service.entity.Test;

import java.util.List;
import java.util.Map;

public interface ITestService extends IService<Test> {

    List<Map<String, Object>> all();
}
