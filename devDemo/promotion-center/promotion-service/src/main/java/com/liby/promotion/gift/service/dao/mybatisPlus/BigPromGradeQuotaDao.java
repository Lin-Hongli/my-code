package com.liby.promotion.gift.service.dao.mybatisPlus;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liby.promotion.gift.service.entity.BigPromGradeQuota;
import com.liby.promotion.gift.service.entity.BigProvincialDistribution;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 大礼包活动星级配置表 Mapper 接口
 * </p>
 *
 * @author weiyunhui
 * @since 2020-12-02
 */
public interface BigPromGradeQuotaDao extends BaseMapper<BigPromGradeQuota> {
    List<BigPromGradeQuota> selectPromGradeByPromID(@Param("id") int id);

}
