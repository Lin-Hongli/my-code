package com.liby.promotion.material.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liby.promotion.material.service.dao.mybatisPlus.MaterialConfigMapper;
import com.liby.promotion.material.service.MaterialConfigurationService;
import com.liby.promotion.material.service.entity.MaterialConfiguration;
import org.springframework.stereotype.Service;

/**
 * @param
 * @author lcd
 * @description
 * @date
 * @return
 **/
@Service
public class MaterialConfigurationServiceImpl extends ServiceImpl<MaterialConfigMapper, MaterialConfiguration> implements MaterialConfigurationService {
}
