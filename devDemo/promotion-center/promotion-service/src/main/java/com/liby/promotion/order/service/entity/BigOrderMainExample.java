package com.liby.promotion.order.service.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BigOrderMainExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BigOrderMainExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(String value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(String value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(String value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(String value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(String value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(String value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLike(String value) {
            addCriterion("order_id like", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotLike(String value) {
            addCriterion("order_id not like", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<String> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<String> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(String value1, String value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(String value1, String value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderTypeIsNull() {
            addCriterion("order_type is null");
            return (Criteria) this;
        }

        public Criteria andOrderTypeIsNotNull() {
            addCriterion("order_type is not null");
            return (Criteria) this;
        }

        public Criteria andOrderTypeEqualTo(Byte value) {
            addCriterion("order_type =", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeNotEqualTo(Byte value) {
            addCriterion("order_type <>", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeGreaterThan(Byte value) {
            addCriterion("order_type >", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("order_type >=", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeLessThan(Byte value) {
            addCriterion("order_type <", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeLessThanOrEqualTo(Byte value) {
            addCriterion("order_type <=", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeIn(List<Byte> values) {
            addCriterion("order_type in", values, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeNotIn(List<Byte> values) {
            addCriterion("order_type not in", values, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeBetween(Byte value1, Byte value2) {
            addCriterion("order_type between", value1, value2, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("order_type not between", value1, value2, "orderType");
            return (Criteria) this;
        }

        public Criteria andPromIdIsNull() {
            addCriterion("prom_id is null");
            return (Criteria) this;
        }

        public Criteria andPromIdIsNotNull() {
            addCriterion("prom_id is not null");
            return (Criteria) this;
        }

        public Criteria andPromIdEqualTo(String value) {
            addCriterion("prom_id =", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdNotEqualTo(String value) {
            addCriterion("prom_id <>", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdGreaterThan(String value) {
            addCriterion("prom_id >", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdGreaterThanOrEqualTo(String value) {
            addCriterion("prom_id >=", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdLessThan(String value) {
            addCriterion("prom_id <", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdLessThanOrEqualTo(String value) {
            addCriterion("prom_id <=", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdLike(String value) {
            addCriterion("prom_id like", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdNotLike(String value) {
            addCriterion("prom_id not like", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdIn(List<String> values) {
            addCriterion("prom_id in", values, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdNotIn(List<String> values) {
            addCriterion("prom_id not in", values, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdBetween(String value1, String value2) {
            addCriterion("prom_id between", value1, value2, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdNotBetween(String value1, String value2) {
            addCriterion("prom_id not between", value1, value2, "promId");
            return (Criteria) this;
        }

        public Criteria andSessionIdIsNull() {
            addCriterion("session_id is null");
            return (Criteria) this;
        }

        public Criteria andSessionIdIsNotNull() {
            addCriterion("session_id is not null");
            return (Criteria) this;
        }

        public Criteria andSessionIdEqualTo(Long value) {
            addCriterion("session_id =", value, "sessionId");
            return (Criteria) this;
        }

        public Criteria andSessionIdNotEqualTo(Long value) {
            addCriterion("session_id <>", value, "sessionId");
            return (Criteria) this;
        }

        public Criteria andSessionIdGreaterThan(Long value) {
            addCriterion("session_id >", value, "sessionId");
            return (Criteria) this;
        }

        public Criteria andSessionIdGreaterThanOrEqualTo(Long value) {
            addCriterion("session_id >=", value, "sessionId");
            return (Criteria) this;
        }

        public Criteria andSessionIdLessThan(Long value) {
            addCriterion("session_id <", value, "sessionId");
            return (Criteria) this;
        }

        public Criteria andSessionIdLessThanOrEqualTo(Long value) {
            addCriterion("session_id <=", value, "sessionId");
            return (Criteria) this;
        }

        public Criteria andSessionIdIn(List<Long> values) {
            addCriterion("session_id in", values, "sessionId");
            return (Criteria) this;
        }

        public Criteria andSessionIdNotIn(List<Long> values) {
            addCriterion("session_id not in", values, "sessionId");
            return (Criteria) this;
        }

        public Criteria andSessionIdBetween(Long value1, Long value2) {
            addCriterion("session_id between", value1, value2, "sessionId");
            return (Criteria) this;
        }

        public Criteria andSessionIdNotBetween(Long value1, Long value2) {
            addCriterion("session_id not between", value1, value2, "sessionId");
            return (Criteria) this;
        }

        public Criteria andPromTitleIsNull() {
            addCriterion("prom_title is null");
            return (Criteria) this;
        }

        public Criteria andPromTitleIsNotNull() {
            addCriterion("prom_title is not null");
            return (Criteria) this;
        }

        public Criteria andPromTitleEqualTo(String value) {
            addCriterion("prom_title =", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleNotEqualTo(String value) {
            addCriterion("prom_title <>", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleGreaterThan(String value) {
            addCriterion("prom_title >", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleGreaterThanOrEqualTo(String value) {
            addCriterion("prom_title >=", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleLessThan(String value) {
            addCriterion("prom_title <", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleLessThanOrEqualTo(String value) {
            addCriterion("prom_title <=", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleLike(String value) {
            addCriterion("prom_title like", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleNotLike(String value) {
            addCriterion("prom_title not like", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleIn(List<String> values) {
            addCriterion("prom_title in", values, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleNotIn(List<String> values) {
            addCriterion("prom_title not in", values, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleBetween(String value1, String value2) {
            addCriterion("prom_title between", value1, value2, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleNotBetween(String value1, String value2) {
            addCriterion("prom_title not between", value1, value2, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromDescIsNull() {
            addCriterion("prom_desc is null");
            return (Criteria) this;
        }

        public Criteria andPromDescIsNotNull() {
            addCriterion("prom_desc is not null");
            return (Criteria) this;
        }

        public Criteria andPromDescEqualTo(String value) {
            addCriterion("prom_desc =", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescNotEqualTo(String value) {
            addCriterion("prom_desc <>", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescGreaterThan(String value) {
            addCriterion("prom_desc >", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescGreaterThanOrEqualTo(String value) {
            addCriterion("prom_desc >=", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescLessThan(String value) {
            addCriterion("prom_desc <", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescLessThanOrEqualTo(String value) {
            addCriterion("prom_desc <=", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescLike(String value) {
            addCriterion("prom_desc like", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescNotLike(String value) {
            addCriterion("prom_desc not like", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescIn(List<String> values) {
            addCriterion("prom_desc in", values, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescNotIn(List<String> values) {
            addCriterion("prom_desc not in", values, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescBetween(String value1, String value2) {
            addCriterion("prom_desc between", value1, value2, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescNotBetween(String value1, String value2) {
            addCriterion("prom_desc not between", value1, value2, "promDesc");
            return (Criteria) this;
        }

        public Criteria andOrderStatusIsNull() {
            addCriterion("order_status is null");
            return (Criteria) this;
        }

        public Criteria andOrderStatusIsNotNull() {
            addCriterion("order_status is not null");
            return (Criteria) this;
        }

        public Criteria andOrderStatusEqualTo(Byte value) {
            addCriterion("order_status =", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusNotEqualTo(Byte value) {
            addCriterion("order_status <>", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusGreaterThan(Byte value) {
            addCriterion("order_status >", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("order_status >=", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusLessThan(Byte value) {
            addCriterion("order_status <", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusLessThanOrEqualTo(Byte value) {
            addCriterion("order_status <=", value, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusIn(List<Byte> values) {
            addCriterion("order_status in", values, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusNotIn(List<Byte> values) {
            addCriterion("order_status not in", values, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusBetween(Byte value1, Byte value2) {
            addCriterion("order_status between", value1, value2, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andOrderStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("order_status not between", value1, value2, "orderStatus");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationIsNull() {
            addCriterion("valid_pay_duration is null");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationIsNotNull() {
            addCriterion("valid_pay_duration is not null");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationEqualTo(Integer value) {
            addCriterion("valid_pay_duration =", value, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationNotEqualTo(Integer value) {
            addCriterion("valid_pay_duration <>", value, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationGreaterThan(Integer value) {
            addCriterion("valid_pay_duration >", value, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationGreaterThanOrEqualTo(Integer value) {
            addCriterion("valid_pay_duration >=", value, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationLessThan(Integer value) {
            addCriterion("valid_pay_duration <", value, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationLessThanOrEqualTo(Integer value) {
            addCriterion("valid_pay_duration <=", value, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationIn(List<Integer> values) {
            addCriterion("valid_pay_duration in", values, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationNotIn(List<Integer> values) {
            addCriterion("valid_pay_duration not in", values, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationBetween(Integer value1, Integer value2) {
            addCriterion("valid_pay_duration between", value1, value2, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationNotBetween(Integer value1, Integer value2) {
            addCriterion("valid_pay_duration not between", value1, value2, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andTotalAmountIsNull() {
            addCriterion("total_amount is null");
            return (Criteria) this;
        }

        public Criteria andTotalAmountIsNotNull() {
            addCriterion("total_amount is not null");
            return (Criteria) this;
        }

        public Criteria andTotalAmountEqualTo(BigDecimal value) {
            addCriterion("total_amount =", value, "totalAmount");
            return (Criteria) this;
        }

        public Criteria andTotalAmountNotEqualTo(BigDecimal value) {
            addCriterion("total_amount <>", value, "totalAmount");
            return (Criteria) this;
        }

        public Criteria andTotalAmountGreaterThan(BigDecimal value) {
            addCriterion("total_amount >", value, "totalAmount");
            return (Criteria) this;
        }

        public Criteria andTotalAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_amount >=", value, "totalAmount");
            return (Criteria) this;
        }

        public Criteria andTotalAmountLessThan(BigDecimal value) {
            addCriterion("total_amount <", value, "totalAmount");
            return (Criteria) this;
        }

        public Criteria andTotalAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_amount <=", value, "totalAmount");
            return (Criteria) this;
        }

        public Criteria andTotalAmountIn(List<BigDecimal> values) {
            addCriterion("total_amount in", values, "totalAmount");
            return (Criteria) this;
        }

        public Criteria andTotalAmountNotIn(List<BigDecimal> values) {
            addCriterion("total_amount not in", values, "totalAmount");
            return (Criteria) this;
        }

        public Criteria andTotalAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_amount between", value1, value2, "totalAmount");
            return (Criteria) this;
        }

        public Criteria andTotalAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_amount not between", value1, value2, "totalAmount");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeIsNull() {
            addCriterion("customer_code is null");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeIsNotNull() {
            addCriterion("customer_code is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeEqualTo(String value) {
            addCriterion("customer_code =", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeNotEqualTo(String value) {
            addCriterion("customer_code <>", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeGreaterThan(String value) {
            addCriterion("customer_code >", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeGreaterThanOrEqualTo(String value) {
            addCriterion("customer_code >=", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeLessThan(String value) {
            addCriterion("customer_code <", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeLessThanOrEqualTo(String value) {
            addCriterion("customer_code <=", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeLike(String value) {
            addCriterion("customer_code like", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeNotLike(String value) {
            addCriterion("customer_code not like", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeIn(List<String> values) {
            addCriterion("customer_code in", values, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeNotIn(List<String> values) {
            addCriterion("customer_code not in", values, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeBetween(String value1, String value2) {
            addCriterion("customer_code between", value1, value2, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeNotBetween(String value1, String value2) {
            addCriterion("customer_code not between", value1, value2, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerNameIsNull() {
            addCriterion("customer_name is null");
            return (Criteria) this;
        }

        public Criteria andCustomerNameIsNotNull() {
            addCriterion("customer_name is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerNameEqualTo(String value) {
            addCriterion("customer_name =", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameNotEqualTo(String value) {
            addCriterion("customer_name <>", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameGreaterThan(String value) {
            addCriterion("customer_name >", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameGreaterThanOrEqualTo(String value) {
            addCriterion("customer_name >=", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameLessThan(String value) {
            addCriterion("customer_name <", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameLessThanOrEqualTo(String value) {
            addCriterion("customer_name <=", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameLike(String value) {
            addCriterion("customer_name like", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameNotLike(String value) {
            addCriterion("customer_name not like", value, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameIn(List<String> values) {
            addCriterion("customer_name in", values, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameNotIn(List<String> values) {
            addCriterion("customer_name not in", values, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameBetween(String value1, String value2) {
            addCriterion("customer_name between", value1, value2, "customerName");
            return (Criteria) this;
        }

        public Criteria andCustomerNameNotBetween(String value1, String value2) {
            addCriterion("customer_name not between", value1, value2, "customerName");
            return (Criteria) this;
        }

        public Criteria andCancelReasonIsNull() {
            addCriterion("cancel_reason is null");
            return (Criteria) this;
        }

        public Criteria andCancelReasonIsNotNull() {
            addCriterion("cancel_reason is not null");
            return (Criteria) this;
        }

        public Criteria andCancelReasonEqualTo(String value) {
            addCriterion("cancel_reason =", value, "cancelReason");
            return (Criteria) this;
        }

        public Criteria andCancelReasonNotEqualTo(String value) {
            addCriterion("cancel_reason <>", value, "cancelReason");
            return (Criteria) this;
        }

        public Criteria andCancelReasonGreaterThan(String value) {
            addCriterion("cancel_reason >", value, "cancelReason");
            return (Criteria) this;
        }

        public Criteria andCancelReasonGreaterThanOrEqualTo(String value) {
            addCriterion("cancel_reason >=", value, "cancelReason");
            return (Criteria) this;
        }

        public Criteria andCancelReasonLessThan(String value) {
            addCriterion("cancel_reason <", value, "cancelReason");
            return (Criteria) this;
        }

        public Criteria andCancelReasonLessThanOrEqualTo(String value) {
            addCriterion("cancel_reason <=", value, "cancelReason");
            return (Criteria) this;
        }

        public Criteria andCancelReasonLike(String value) {
            addCriterion("cancel_reason like", value, "cancelReason");
            return (Criteria) this;
        }

        public Criteria andCancelReasonNotLike(String value) {
            addCriterion("cancel_reason not like", value, "cancelReason");
            return (Criteria) this;
        }

        public Criteria andCancelReasonIn(List<String> values) {
            addCriterion("cancel_reason in", values, "cancelReason");
            return (Criteria) this;
        }

        public Criteria andCancelReasonNotIn(List<String> values) {
            addCriterion("cancel_reason not in", values, "cancelReason");
            return (Criteria) this;
        }

        public Criteria andCancelReasonBetween(String value1, String value2) {
            addCriterion("cancel_reason between", value1, value2, "cancelReason");
            return (Criteria) this;
        }

        public Criteria andCancelReasonNotBetween(String value1, String value2) {
            addCriterion("cancel_reason not between", value1, value2, "cancelReason");
            return (Criteria) this;
        }

        public Criteria andCancelDescIsNull() {
            addCriterion("cancel_desc is null");
            return (Criteria) this;
        }

        public Criteria andCancelDescIsNotNull() {
            addCriterion("cancel_desc is not null");
            return (Criteria) this;
        }

        public Criteria andCancelDescEqualTo(String value) {
            addCriterion("cancel_desc =", value, "cancelDesc");
            return (Criteria) this;
        }

        public Criteria andCancelDescNotEqualTo(String value) {
            addCriterion("cancel_desc <>", value, "cancelDesc");
            return (Criteria) this;
        }

        public Criteria andCancelDescGreaterThan(String value) {
            addCriterion("cancel_desc >", value, "cancelDesc");
            return (Criteria) this;
        }

        public Criteria andCancelDescGreaterThanOrEqualTo(String value) {
            addCriterion("cancel_desc >=", value, "cancelDesc");
            return (Criteria) this;
        }

        public Criteria andCancelDescLessThan(String value) {
            addCriterion("cancel_desc <", value, "cancelDesc");
            return (Criteria) this;
        }

        public Criteria andCancelDescLessThanOrEqualTo(String value) {
            addCriterion("cancel_desc <=", value, "cancelDesc");
            return (Criteria) this;
        }

        public Criteria andCancelDescLike(String value) {
            addCriterion("cancel_desc like", value, "cancelDesc");
            return (Criteria) this;
        }

        public Criteria andCancelDescNotLike(String value) {
            addCriterion("cancel_desc not like", value, "cancelDesc");
            return (Criteria) this;
        }

        public Criteria andCancelDescIn(List<String> values) {
            addCriterion("cancel_desc in", values, "cancelDesc");
            return (Criteria) this;
        }

        public Criteria andCancelDescNotIn(List<String> values) {
            addCriterion("cancel_desc not in", values, "cancelDesc");
            return (Criteria) this;
        }

        public Criteria andCancelDescBetween(String value1, String value2) {
            addCriterion("cancel_desc between", value1, value2, "cancelDesc");
            return (Criteria) this;
        }

        public Criteria andCancelDescNotBetween(String value1, String value2) {
            addCriterion("cancel_desc not between", value1, value2, "cancelDesc");
            return (Criteria) this;
        }

        public Criteria andCancelTimeIsNull() {
            addCriterion("cancel_time is null");
            return (Criteria) this;
        }

        public Criteria andCancelTimeIsNotNull() {
            addCriterion("cancel_time is not null");
            return (Criteria) this;
        }

        public Criteria andCancelTimeEqualTo(Date value) {
            addCriterion("cancel_time =", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeNotEqualTo(Date value) {
            addCriterion("cancel_time <>", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeGreaterThan(Date value) {
            addCriterion("cancel_time >", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("cancel_time >=", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeLessThan(Date value) {
            addCriterion("cancel_time <", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeLessThanOrEqualTo(Date value) {
            addCriterion("cancel_time <=", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeIn(List<Date> values) {
            addCriterion("cancel_time in", values, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeNotIn(List<Date> values) {
            addCriterion("cancel_time not in", values, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeBetween(Date value1, Date value2) {
            addCriterion("cancel_time between", value1, value2, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeNotBetween(Date value1, Date value2) {
            addCriterion("cancel_time not between", value1, value2, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeIsNull() {
            addCriterion("pay_time is null");
            return (Criteria) this;
        }

        public Criteria andPayTimeIsNotNull() {
            addCriterion("pay_time is not null");
            return (Criteria) this;
        }

        public Criteria andPayTimeEqualTo(Date value) {
            addCriterion("pay_time =", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotEqualTo(Date value) {
            addCriterion("pay_time <>", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThan(Date value) {
            addCriterion("pay_time >", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("pay_time >=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThan(Date value) {
            addCriterion("pay_time <", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThanOrEqualTo(Date value) {
            addCriterion("pay_time <=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeIn(List<Date> values) {
            addCriterion("pay_time in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotIn(List<Date> values) {
            addCriterion("pay_time not in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeBetween(Date value1, Date value2) {
            addCriterion("pay_time between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotBetween(Date value1, Date value2) {
            addCriterion("pay_time not between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andOrderSyncSuccIsNull() {
            addCriterion("order_sync_succ is null");
            return (Criteria) this;
        }

        public Criteria andOrderSyncSuccIsNotNull() {
            addCriterion("order_sync_succ is not null");
            return (Criteria) this;
        }

        public Criteria andOrderSyncSuccEqualTo(Byte value) {
            addCriterion("order_sync_succ =", value, "orderSyncSucc");
            return (Criteria) this;
        }

        public Criteria andOrderSyncSuccNotEqualTo(Byte value) {
            addCriterion("order_sync_succ <>", value, "orderSyncSucc");
            return (Criteria) this;
        }

        public Criteria andOrderSyncSuccGreaterThan(Byte value) {
            addCriterion("order_sync_succ >", value, "orderSyncSucc");
            return (Criteria) this;
        }

        public Criteria andOrderSyncSuccGreaterThanOrEqualTo(Byte value) {
            addCriterion("order_sync_succ >=", value, "orderSyncSucc");
            return (Criteria) this;
        }

        public Criteria andOrderSyncSuccLessThan(Byte value) {
            addCriterion("order_sync_succ <", value, "orderSyncSucc");
            return (Criteria) this;
        }

        public Criteria andOrderSyncSuccLessThanOrEqualTo(Byte value) {
            addCriterion("order_sync_succ <=", value, "orderSyncSucc");
            return (Criteria) this;
        }

        public Criteria andOrderSyncSuccIn(List<Byte> values) {
            addCriterion("order_sync_succ in", values, "orderSyncSucc");
            return (Criteria) this;
        }

        public Criteria andOrderSyncSuccNotIn(List<Byte> values) {
            addCriterion("order_sync_succ not in", values, "orderSyncSucc");
            return (Criteria) this;
        }

        public Criteria andOrderSyncSuccBetween(Byte value1, Byte value2) {
            addCriterion("order_sync_succ between", value1, value2, "orderSyncSucc");
            return (Criteria) this;
        }

        public Criteria andOrderSyncSuccNotBetween(Byte value1, Byte value2) {
            addCriterion("order_sync_succ not between", value1, value2, "orderSyncSucc");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonIsNull() {
            addCriterion("sync_fail_reason is null");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonIsNotNull() {
            addCriterion("sync_fail_reason is not null");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonEqualTo(String value) {
            addCriterion("sync_fail_reason =", value, "syncFailReason");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonNotEqualTo(String value) {
            addCriterion("sync_fail_reason <>", value, "syncFailReason");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonGreaterThan(String value) {
            addCriterion("sync_fail_reason >", value, "syncFailReason");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonGreaterThanOrEqualTo(String value) {
            addCriterion("sync_fail_reason >=", value, "syncFailReason");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonLessThan(String value) {
            addCriterion("sync_fail_reason <", value, "syncFailReason");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonLessThanOrEqualTo(String value) {
            addCriterion("sync_fail_reason <=", value, "syncFailReason");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonLike(String value) {
            addCriterion("sync_fail_reason like", value, "syncFailReason");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonNotLike(String value) {
            addCriterion("sync_fail_reason not like", value, "syncFailReason");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonIn(List<String> values) {
            addCriterion("sync_fail_reason in", values, "syncFailReason");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonNotIn(List<String> values) {
            addCriterion("sync_fail_reason not in", values, "syncFailReason");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonBetween(String value1, String value2) {
            addCriterion("sync_fail_reason between", value1, value2, "syncFailReason");
            return (Criteria) this;
        }

        public Criteria andSyncFailReasonNotBetween(String value1, String value2) {
            addCriterion("sync_fail_reason not between", value1, value2, "syncFailReason");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andCreateUserIsNull() {
            addCriterion("create_user is null");
            return (Criteria) this;
        }

        public Criteria andCreateUserIsNotNull() {
            addCriterion("create_user is not null");
            return (Criteria) this;
        }

        public Criteria andCreateUserEqualTo(String value) {
            addCriterion("create_user =", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotEqualTo(String value) {
            addCriterion("create_user <>", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserGreaterThan(String value) {
            addCriterion("create_user >", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserGreaterThanOrEqualTo(String value) {
            addCriterion("create_user >=", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLessThan(String value) {
            addCriterion("create_user <", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLessThanOrEqualTo(String value) {
            addCriterion("create_user <=", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserLike(String value) {
            addCriterion("create_user like", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotLike(String value) {
            addCriterion("create_user not like", value, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserIn(List<String> values) {
            addCriterion("create_user in", values, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotIn(List<String> values) {
            addCriterion("create_user not in", values, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserBetween(String value1, String value2) {
            addCriterion("create_user between", value1, value2, "createUser");
            return (Criteria) this;
        }

        public Criteria andCreateUserNotBetween(String value1, String value2) {
            addCriterion("create_user not between", value1, value2, "createUser");
            return (Criteria) this;
        }

        public Criteria andSessionNameIsNull() {
            addCriterion("session_name is null");
            return (Criteria) this;
        }

        public Criteria andSessionNameIsNotNull() {
            addCriterion("session_name is not null");
            return (Criteria) this;
        }

        public Criteria andSessionNameEqualTo(String value) {
            addCriterion("session_name =", value, "sessionName");
            return (Criteria) this;
        }

        public Criteria andSessionNameNotEqualTo(String value) {
            addCriterion("session_name <>", value, "sessionName");
            return (Criteria) this;
        }

        public Criteria andSessionNameGreaterThan(String value) {
            addCriterion("session_name >", value, "sessionName");
            return (Criteria) this;
        }

        public Criteria andSessionNameGreaterThanOrEqualTo(String value) {
            addCriterion("session_name >=", value, "sessionName");
            return (Criteria) this;
        }

        public Criteria andSessionNameLessThan(String value) {
            addCriterion("session_name <", value, "sessionName");
            return (Criteria) this;
        }

        public Criteria andSessionNameLessThanOrEqualTo(String value) {
            addCriterion("session_name <=", value, "sessionName");
            return (Criteria) this;
        }

        public Criteria andSessionNameLike(String value) {
            addCriterion("session_name like", value, "sessionName");
            return (Criteria) this;
        }

        public Criteria andSessionNameNotLike(String value) {
            addCriterion("session_name not like", value, "sessionName");
            return (Criteria) this;
        }

        public Criteria andSessionNameIn(List<String> values) {
            addCriterion("session_name in", values, "sessionName");
            return (Criteria) this;
        }

        public Criteria andSessionNameNotIn(List<String> values) {
            addCriterion("session_name not in", values, "sessionName");
            return (Criteria) this;
        }

        public Criteria andSessionNameBetween(String value1, String value2) {
            addCriterion("session_name between", value1, value2, "sessionName");
            return (Criteria) this;
        }

        public Criteria andSessionNameNotBetween(String value1, String value2) {
            addCriterion("session_name not between", value1, value2, "sessionName");
            return (Criteria) this;
        }

        public Criteria andRequestTimeIsNull() {
            addCriterion("request_time is null");
            return (Criteria) this;
        }

        public Criteria andRequestTimeIsNotNull() {
            addCriterion("request_time is not null");
            return (Criteria) this;
        }

        public Criteria andRequestTimeEqualTo(Date value) {
            addCriterion("request_time =", value, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeNotEqualTo(Date value) {
            addCriterion("request_time <>", value, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeGreaterThan(Date value) {
            addCriterion("request_time >", value, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("request_time >=", value, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeLessThan(Date value) {
            addCriterion("request_time <", value, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeLessThanOrEqualTo(Date value) {
            addCriterion("request_time <=", value, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeIn(List<Date> values) {
            addCriterion("request_time in", values, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeNotIn(List<Date> values) {
            addCriterion("request_time not in", values, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeBetween(Date value1, Date value2) {
            addCriterion("request_time between", value1, value2, "requestTime");
            return (Criteria) this;
        }

        public Criteria andRequestTimeNotBetween(Date value1, Date value2) {
            addCriterion("request_time not between", value1, value2, "requestTime");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeIsNull() {
            addCriterion("provincial_code is null");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeIsNotNull() {
            addCriterion("provincial_code is not null");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeEqualTo(String value) {
            addCriterion("provincial_code =", value, "provincialCode");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeNotEqualTo(String value) {
            addCriterion("provincial_code <>", value, "provincialCode");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeGreaterThan(String value) {
            addCriterion("provincial_code >", value, "provincialCode");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeGreaterThanOrEqualTo(String value) {
            addCriterion("provincial_code >=", value, "provincialCode");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeLessThan(String value) {
            addCriterion("provincial_code <", value, "provincialCode");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeLessThanOrEqualTo(String value) {
            addCriterion("provincial_code <=", value, "provincialCode");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeLike(String value) {
            addCriterion("provincial_code like", value, "provincialCode");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeNotLike(String value) {
            addCriterion("provincial_code not like", value, "provincialCode");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeIn(List<String> values) {
            addCriterion("provincial_code in", values, "provincialCode");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeNotIn(List<String> values) {
            addCriterion("provincial_code not in", values, "provincialCode");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeBetween(String value1, String value2) {
            addCriterion("provincial_code between", value1, value2, "provincialCode");
            return (Criteria) this;
        }

        public Criteria andProvincialCodeNotBetween(String value1, String value2) {
            addCriterion("provincial_code not between", value1, value2, "provincialCode");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceIsNull() {
            addCriterion("is_can_material_replace is null");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceIsNotNull() {
            addCriterion("is_can_material_replace is not null");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceEqualTo(Byte value) {
            addCriterion("is_can_material_replace =", value, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceNotEqualTo(Byte value) {
            addCriterion("is_can_material_replace <>", value, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceGreaterThan(Byte value) {
            addCriterion("is_can_material_replace >", value, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceGreaterThanOrEqualTo(Byte value) {
            addCriterion("is_can_material_replace >=", value, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceLessThan(Byte value) {
            addCriterion("is_can_material_replace <", value, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceLessThanOrEqualTo(Byte value) {
            addCriterion("is_can_material_replace <=", value, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceIn(List<Byte> values) {
            addCriterion("is_can_material_replace in", values, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceNotIn(List<Byte> values) {
            addCriterion("is_can_material_replace not in", values, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceBetween(Byte value1, Byte value2) {
            addCriterion("is_can_material_replace between", value1, value2, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceNotBetween(Byte value1, Byte value2) {
            addCriterion("is_can_material_replace not between", value1, value2, "isCanMaterialReplace");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}