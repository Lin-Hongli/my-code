package com.liby.promotion.gift.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liby.promotion.gift.service.entity.BigCustomerDistribution;

import java.util.List;

/**
 * <p>
 * 客户分配量 服务类
 * </p>
 *
 * @author weiyunhui
 * @since 2020-12-02
 */
public interface BigCustomerDistributionService extends IService<BigCustomerDistribution> {
int insertCustomerAll(List<BigCustomerDistribution> bigCustomerDistributions);
}
