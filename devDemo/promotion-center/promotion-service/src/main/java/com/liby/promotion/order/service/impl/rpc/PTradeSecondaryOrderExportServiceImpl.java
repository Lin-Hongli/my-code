package com.liby.promotion.order.service.impl.rpc;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageInfo;
import com.liby.promotion.order.export.dto.QureyOrderRequest;
import com.liby.promotion.order.export.dto.QureyOrderResponse;
import com.liby.promotion.order.export.service.PTradeSecondaryOrderExportService;
import com.liby.promotion.order.service.PTradeSecondaryOrderService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class PTradeSecondaryOrderExportServiceImpl implements PTradeSecondaryOrderExportService {

    @Autowired
    private PTradeSecondaryOrderService pTradeSecondaryOrderService;

    @Override
    public PageInfo<QureyOrderResponse> qureyGiftPacksOrder(QureyOrderRequest qureyOrderRequest) {
        return pTradeSecondaryOrderService.qureyGiftPacksOrder(qureyOrderRequest);
    }

    @Override
    public List<QureyOrderResponse> exportGiftPacksOrder(QureyOrderRequest qureyOrderRequest) {
        return pTradeSecondaryOrderService.exportGiftPacksOrder(qureyOrderRequest);
    }


}
