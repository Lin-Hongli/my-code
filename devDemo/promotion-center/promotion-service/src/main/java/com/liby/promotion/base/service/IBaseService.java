package com.liby.promotion.base.service;

import com.github.pagehelper.Page;

import java.util.List;

/**
 * LibyService 内部基础服务接口
 *
 * @author lgj liugongjun@chinasie.com
 * @version 0.0.1
 * @date 2018年12月02日
 */
public interface IBaseService {
    /**
     * 查询单条数据
     *
     * @param param 条件
     * @return 数据
     */
    <T> T selectOne(Object param);

    /**
     * 查询单条数据
     *
     * @return 数据
     */
    <T> T selectOne();

    /**
     * 查询批量数据 无条件
     *
     * @return 数据
     */
    <T> List<T> selectList();

    /**
     * 查询批量数据 无条件,带翻页
     *
     * @param page 分页
     * @return List<T> 数据
     */
    <T> List<T> selectList(Page page);

    /**
     * 查询批量数据 带条件
     *
     * @param parameter 参数
     * @return 数据
     */
    <T> List<T> selectList(Object parameter);

    /**
     * 翻页带条件查询
     *
     * @param parameter 参数
     * @param page      分页
     * @return 数据
     */
    <T> List<T> selectList(Object parameter, Page page);

    /**
     * 插入数据
     *
     * @param parameter 参数
     * @return 插入数量
     */
    int insert(Object parameter);

    /**
     * 插入数据 无参数
     *
     * @return 插入数量
     */
    int insert();

    /**
     * 插入批量数据
     *
     * @param params 数据集合
     */
    void insert(List<Object> params);

    /**
     * mybatis批量大数据插入
     *
     * @param params   传过来的数据
     * @param batchNum 每批次commit的个数
     */
    void batchInsert(List<Object> params, int batchNum);

    /**
     * 更新批量数据
     *
     * @param params 数据集合
     */
    void update(List<Object> params);

    /**
     * 更新数据
     *
     * @param parameter 参数
     * @return 更新数量
     */
    int update(Object parameter);

    /**
     * 更新数据
     *
     * @return 更新数量
     */
    int update();

    /**
     * 删除数据
     *
     * @param parameter 参数
     * @return 删除数量
     */
    int delete(Object parameter);

    /**
     * 无参数删除
     *
     * @return 删除数量
     */
    int delete();

    /**
     * 删除批量数据
     *
     * @param params 数据集合
     */
    void delete(List<Object> params);

    /**
     * 批量更新
     *
     * @param data     参数列表
     * @param batchNum 每次同步条数
     */
    void batchUpdate(List<Object> data, int batchNum);

    /**
     * 批量删除
     *
     * @param data     参数列表
     * @param batchNum 每次同步条数
     */
    void batchDelete(List<Object> data, int batchNum);
}
