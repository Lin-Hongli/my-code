package com.liby.promotion.order.service.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BigOrderItemExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BigOrderItemExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andOrderItemIdIsNull() {
            addCriterion("order_item_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderItemIdIsNotNull() {
            addCriterion("order_item_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderItemIdEqualTo(Long value) {
            addCriterion("order_item_id =", value, "orderItemId");
            return (Criteria) this;
        }

        public Criteria andOrderItemIdNotEqualTo(Long value) {
            addCriterion("order_item_id <>", value, "orderItemId");
            return (Criteria) this;
        }

        public Criteria andOrderItemIdGreaterThan(Long value) {
            addCriterion("order_item_id >", value, "orderItemId");
            return (Criteria) this;
        }

        public Criteria andOrderItemIdGreaterThanOrEqualTo(Long value) {
            addCriterion("order_item_id >=", value, "orderItemId");
            return (Criteria) this;
        }

        public Criteria andOrderItemIdLessThan(Long value) {
            addCriterion("order_item_id <", value, "orderItemId");
            return (Criteria) this;
        }

        public Criteria andOrderItemIdLessThanOrEqualTo(Long value) {
            addCriterion("order_item_id <=", value, "orderItemId");
            return (Criteria) this;
        }

        public Criteria andOrderItemIdIn(List<Long> values) {
            addCriterion("order_item_id in", values, "orderItemId");
            return (Criteria) this;
        }

        public Criteria andOrderItemIdNotIn(List<Long> values) {
            addCriterion("order_item_id not in", values, "orderItemId");
            return (Criteria) this;
        }

        public Criteria andOrderItemIdBetween(Long value1, Long value2) {
            addCriterion("order_item_id between", value1, value2, "orderItemId");
            return (Criteria) this;
        }

        public Criteria andOrderItemIdNotBetween(Long value1, Long value2) {
            addCriterion("order_item_id not between", value1, value2, "orderItemId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(String value) {
            addCriterion("order_id =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(String value) {
            addCriterion("order_id <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(String value) {
            addCriterion("order_id >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(String value) {
            addCriterion("order_id >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(String value) {
            addCriterion("order_id <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(String value) {
            addCriterion("order_id <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLike(String value) {
            addCriterion("order_id like", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotLike(String value) {
            addCriterion("order_id not like", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<String> values) {
            addCriterion("order_id in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<String> values) {
            addCriterion("order_id not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(String value1, String value2) {
            addCriterion("order_id between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(String value1, String value2) {
            addCriterion("order_id not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andItemIdIsNull() {
            addCriterion("item_id is null");
            return (Criteria) this;
        }

        public Criteria andItemIdIsNotNull() {
            addCriterion("item_id is not null");
            return (Criteria) this;
        }

        public Criteria andItemIdEqualTo(String value) {
            addCriterion("item_id =", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotEqualTo(String value) {
            addCriterion("item_id <>", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdGreaterThan(String value) {
            addCriterion("item_id >", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdGreaterThanOrEqualTo(String value) {
            addCriterion("item_id >=", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdLessThan(String value) {
            addCriterion("item_id <", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdLessThanOrEqualTo(String value) {
            addCriterion("item_id <=", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdLike(String value) {
            addCriterion("item_id like", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotLike(String value) {
            addCriterion("item_id not like", value, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdIn(List<String> values) {
            addCriterion("item_id in", values, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotIn(List<String> values) {
            addCriterion("item_id not in", values, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdBetween(String value1, String value2) {
            addCriterion("item_id between", value1, value2, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemIdNotBetween(String value1, String value2) {
            addCriterion("item_id not between", value1, value2, "itemId");
            return (Criteria) this;
        }

        public Criteria andItemNameIsNull() {
            addCriterion("item_name is null");
            return (Criteria) this;
        }

        public Criteria andItemNameIsNotNull() {
            addCriterion("item_name is not null");
            return (Criteria) this;
        }

        public Criteria andItemNameEqualTo(String value) {
            addCriterion("item_name =", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameNotEqualTo(String value) {
            addCriterion("item_name <>", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameGreaterThan(String value) {
            addCriterion("item_name >", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameGreaterThanOrEqualTo(String value) {
            addCriterion("item_name >=", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameLessThan(String value) {
            addCriterion("item_name <", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameLessThanOrEqualTo(String value) {
            addCriterion("item_name <=", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameLike(String value) {
            addCriterion("item_name like", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameNotLike(String value) {
            addCriterion("item_name not like", value, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameIn(List<String> values) {
            addCriterion("item_name in", values, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameNotIn(List<String> values) {
            addCriterion("item_name not in", values, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameBetween(String value1, String value2) {
            addCriterion("item_name between", value1, value2, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemNameNotBetween(String value1, String value2) {
            addCriterion("item_name not between", value1, value2, "itemName");
            return (Criteria) this;
        }

        public Criteria andItemPriceIsNull() {
            addCriterion("item_price is null");
            return (Criteria) this;
        }

        public Criteria andItemPriceIsNotNull() {
            addCriterion("item_price is not null");
            return (Criteria) this;
        }

        public Criteria andItemPriceEqualTo(BigDecimal value) {
            addCriterion("item_price =", value, "itemPrice");
            return (Criteria) this;
        }

        public Criteria andItemPriceNotEqualTo(BigDecimal value) {
            addCriterion("item_price <>", value, "itemPrice");
            return (Criteria) this;
        }

        public Criteria andItemPriceGreaterThan(BigDecimal value) {
            addCriterion("item_price >", value, "itemPrice");
            return (Criteria) this;
        }

        public Criteria andItemPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("item_price >=", value, "itemPrice");
            return (Criteria) this;
        }

        public Criteria andItemPriceLessThan(BigDecimal value) {
            addCriterion("item_price <", value, "itemPrice");
            return (Criteria) this;
        }

        public Criteria andItemPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("item_price <=", value, "itemPrice");
            return (Criteria) this;
        }

        public Criteria andItemPriceIn(List<BigDecimal> values) {
            addCriterion("item_price in", values, "itemPrice");
            return (Criteria) this;
        }

        public Criteria andItemPriceNotIn(List<BigDecimal> values) {
            addCriterion("item_price not in", values, "itemPrice");
            return (Criteria) this;
        }

        public Criteria andItemPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("item_price between", value1, value2, "itemPrice");
            return (Criteria) this;
        }

        public Criteria andItemPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("item_price not between", value1, value2, "itemPrice");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlIsNull() {
            addCriterion("item_img_url is null");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlIsNotNull() {
            addCriterion("item_img_url is not null");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlEqualTo(String value) {
            addCriterion("item_img_url =", value, "itemImgUrl");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlNotEqualTo(String value) {
            addCriterion("item_img_url <>", value, "itemImgUrl");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlGreaterThan(String value) {
            addCriterion("item_img_url >", value, "itemImgUrl");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlGreaterThanOrEqualTo(String value) {
            addCriterion("item_img_url >=", value, "itemImgUrl");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlLessThan(String value) {
            addCriterion("item_img_url <", value, "itemImgUrl");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlLessThanOrEqualTo(String value) {
            addCriterion("item_img_url <=", value, "itemImgUrl");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlLike(String value) {
            addCriterion("item_img_url like", value, "itemImgUrl");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlNotLike(String value) {
            addCriterion("item_img_url not like", value, "itemImgUrl");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlIn(List<String> values) {
            addCriterion("item_img_url in", values, "itemImgUrl");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlNotIn(List<String> values) {
            addCriterion("item_img_url not in", values, "itemImgUrl");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlBetween(String value1, String value2) {
            addCriterion("item_img_url between", value1, value2, "itemImgUrl");
            return (Criteria) this;
        }

        public Criteria andItemImgUrlNotBetween(String value1, String value2) {
            addCriterion("item_img_url not between", value1, value2, "itemImgUrl");
            return (Criteria) this;
        }

        public Criteria andPromPriceIsNull() {
            addCriterion("prom_price is null");
            return (Criteria) this;
        }

        public Criteria andPromPriceIsNotNull() {
            addCriterion("prom_price is not null");
            return (Criteria) this;
        }

        public Criteria andPromPriceEqualTo(BigDecimal value) {
            addCriterion("prom_price =", value, "promPrice");
            return (Criteria) this;
        }

        public Criteria andPromPriceNotEqualTo(BigDecimal value) {
            addCriterion("prom_price <>", value, "promPrice");
            return (Criteria) this;
        }

        public Criteria andPromPriceGreaterThan(BigDecimal value) {
            addCriterion("prom_price >", value, "promPrice");
            return (Criteria) this;
        }

        public Criteria andPromPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("prom_price >=", value, "promPrice");
            return (Criteria) this;
        }

        public Criteria andPromPriceLessThan(BigDecimal value) {
            addCriterion("prom_price <", value, "promPrice");
            return (Criteria) this;
        }

        public Criteria andPromPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("prom_price <=", value, "promPrice");
            return (Criteria) this;
        }

        public Criteria andPromPriceIn(List<BigDecimal> values) {
            addCriterion("prom_price in", values, "promPrice");
            return (Criteria) this;
        }

        public Criteria andPromPriceNotIn(List<BigDecimal> values) {
            addCriterion("prom_price not in", values, "promPrice");
            return (Criteria) this;
        }

        public Criteria andPromPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("prom_price between", value1, value2, "promPrice");
            return (Criteria) this;
        }

        public Criteria andPromPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("prom_price not between", value1, value2, "promPrice");
            return (Criteria) this;
        }

        public Criteria andItemCntIsNull() {
            addCriterion("item_cnt is null");
            return (Criteria) this;
        }

        public Criteria andItemCntIsNotNull() {
            addCriterion("item_cnt is not null");
            return (Criteria) this;
        }

        public Criteria andItemCntEqualTo(Integer value) {
            addCriterion("item_cnt =", value, "itemCnt");
            return (Criteria) this;
        }

        public Criteria andItemCntNotEqualTo(Integer value) {
            addCriterion("item_cnt <>", value, "itemCnt");
            return (Criteria) this;
        }

        public Criteria andItemCntGreaterThan(Integer value) {
            addCriterion("item_cnt >", value, "itemCnt");
            return (Criteria) this;
        }

        public Criteria andItemCntGreaterThanOrEqualTo(Integer value) {
            addCriterion("item_cnt >=", value, "itemCnt");
            return (Criteria) this;
        }

        public Criteria andItemCntLessThan(Integer value) {
            addCriterion("item_cnt <", value, "itemCnt");
            return (Criteria) this;
        }

        public Criteria andItemCntLessThanOrEqualTo(Integer value) {
            addCriterion("item_cnt <=", value, "itemCnt");
            return (Criteria) this;
        }

        public Criteria andItemCntIn(List<Integer> values) {
            addCriterion("item_cnt in", values, "itemCnt");
            return (Criteria) this;
        }

        public Criteria andItemCntNotIn(List<Integer> values) {
            addCriterion("item_cnt not in", values, "itemCnt");
            return (Criteria) this;
        }

        public Criteria andItemCntBetween(Integer value1, Integer value2) {
            addCriterion("item_cnt between", value1, value2, "itemCnt");
            return (Criteria) this;
        }

        public Criteria andItemCntNotBetween(Integer value1, Integer value2) {
            addCriterion("item_cnt not between", value1, value2, "itemCnt");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andPromIdIsNull() {
            addCriterion("prom_id is null");
            return (Criteria) this;
        }

        public Criteria andPromIdIsNotNull() {
            addCriterion("prom_id is not null");
            return (Criteria) this;
        }

        public Criteria andPromIdEqualTo(String value) {
            addCriterion("prom_id =", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdNotEqualTo(String value) {
            addCriterion("prom_id <>", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdGreaterThan(String value) {
            addCriterion("prom_id >", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdGreaterThanOrEqualTo(String value) {
            addCriterion("prom_id >=", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdLessThan(String value) {
            addCriterion("prom_id <", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdLessThanOrEqualTo(String value) {
            addCriterion("prom_id <=", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdLike(String value) {
            addCriterion("prom_id like", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdNotLike(String value) {
            addCriterion("prom_id not like", value, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdIn(List<String> values) {
            addCriterion("prom_id in", values, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdNotIn(List<String> values) {
            addCriterion("prom_id not in", values, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdBetween(String value1, String value2) {
            addCriterion("prom_id between", value1, value2, "promId");
            return (Criteria) this;
        }

        public Criteria andPromIdNotBetween(String value1, String value2) {
            addCriterion("prom_id not between", value1, value2, "promId");
            return (Criteria) this;
        }

        public Criteria andPromTitleIsNull() {
            addCriterion("prom_title is null");
            return (Criteria) this;
        }

        public Criteria andPromTitleIsNotNull() {
            addCriterion("prom_title is not null");
            return (Criteria) this;
        }

        public Criteria andPromTitleEqualTo(String value) {
            addCriterion("prom_title =", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleNotEqualTo(String value) {
            addCriterion("prom_title <>", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleGreaterThan(String value) {
            addCriterion("prom_title >", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleGreaterThanOrEqualTo(String value) {
            addCriterion("prom_title >=", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleLessThan(String value) {
            addCriterion("prom_title <", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleLessThanOrEqualTo(String value) {
            addCriterion("prom_title <=", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleLike(String value) {
            addCriterion("prom_title like", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleNotLike(String value) {
            addCriterion("prom_title not like", value, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleIn(List<String> values) {
            addCriterion("prom_title in", values, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleNotIn(List<String> values) {
            addCriterion("prom_title not in", values, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleBetween(String value1, String value2) {
            addCriterion("prom_title between", value1, value2, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromTitleNotBetween(String value1, String value2) {
            addCriterion("prom_title not between", value1, value2, "promTitle");
            return (Criteria) this;
        }

        public Criteria andPromDescIsNull() {
            addCriterion("prom_desc is null");
            return (Criteria) this;
        }

        public Criteria andPromDescIsNotNull() {
            addCriterion("prom_desc is not null");
            return (Criteria) this;
        }

        public Criteria andPromDescEqualTo(String value) {
            addCriterion("prom_desc =", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescNotEqualTo(String value) {
            addCriterion("prom_desc <>", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescGreaterThan(String value) {
            addCriterion("prom_desc >", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescGreaterThanOrEqualTo(String value) {
            addCriterion("prom_desc >=", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescLessThan(String value) {
            addCriterion("prom_desc <", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescLessThanOrEqualTo(String value) {
            addCriterion("prom_desc <=", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescLike(String value) {
            addCriterion("prom_desc like", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescNotLike(String value) {
            addCriterion("prom_desc not like", value, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescIn(List<String> values) {
            addCriterion("prom_desc in", values, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescNotIn(List<String> values) {
            addCriterion("prom_desc not in", values, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescBetween(String value1, String value2) {
            addCriterion("prom_desc between", value1, value2, "promDesc");
            return (Criteria) this;
        }

        public Criteria andPromDescNotBetween(String value1, String value2) {
            addCriterion("prom_desc not between", value1, value2, "promDesc");
            return (Criteria) this;
        }

        public Criteria andBrandTypeIsNull() {
            addCriterion("brand_type is null");
            return (Criteria) this;
        }

        public Criteria andBrandTypeIsNotNull() {
            addCriterion("brand_type is not null");
            return (Criteria) this;
        }

        public Criteria andBrandTypeEqualTo(Byte value) {
            addCriterion("brand_type =", value, "brandType");
            return (Criteria) this;
        }

        public Criteria andBrandTypeNotEqualTo(Byte value) {
            addCriterion("brand_type <>", value, "brandType");
            return (Criteria) this;
        }

        public Criteria andBrandTypeGreaterThan(Byte value) {
            addCriterion("brand_type >", value, "brandType");
            return (Criteria) this;
        }

        public Criteria andBrandTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("brand_type >=", value, "brandType");
            return (Criteria) this;
        }

        public Criteria andBrandTypeLessThan(Byte value) {
            addCriterion("brand_type <", value, "brandType");
            return (Criteria) this;
        }

        public Criteria andBrandTypeLessThanOrEqualTo(Byte value) {
            addCriterion("brand_type <=", value, "brandType");
            return (Criteria) this;
        }

        public Criteria andBrandTypeIn(List<Byte> values) {
            addCriterion("brand_type in", values, "brandType");
            return (Criteria) this;
        }

        public Criteria andBrandTypeNotIn(List<Byte> values) {
            addCriterion("brand_type not in", values, "brandType");
            return (Criteria) this;
        }

        public Criteria andBrandTypeBetween(Byte value1, Byte value2) {
            addCriterion("brand_type between", value1, value2, "brandType");
            return (Criteria) this;
        }

        public Criteria andBrandTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("brand_type not between", value1, value2, "brandType");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdIsNull() {
            addCriterion("gift_item_id is null");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdIsNotNull() {
            addCriterion("gift_item_id is not null");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdEqualTo(String value) {
            addCriterion("gift_item_id =", value, "giftItemId");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdNotEqualTo(String value) {
            addCriterion("gift_item_id <>", value, "giftItemId");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdGreaterThan(String value) {
            addCriterion("gift_item_id >", value, "giftItemId");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdGreaterThanOrEqualTo(String value) {
            addCriterion("gift_item_id >=", value, "giftItemId");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdLessThan(String value) {
            addCriterion("gift_item_id <", value, "giftItemId");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdLessThanOrEqualTo(String value) {
            addCriterion("gift_item_id <=", value, "giftItemId");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdLike(String value) {
            addCriterion("gift_item_id like", value, "giftItemId");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdNotLike(String value) {
            addCriterion("gift_item_id not like", value, "giftItemId");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdIn(List<String> values) {
            addCriterion("gift_item_id in", values, "giftItemId");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdNotIn(List<String> values) {
            addCriterion("gift_item_id not in", values, "giftItemId");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdBetween(String value1, String value2) {
            addCriterion("gift_item_id between", value1, value2, "giftItemId");
            return (Criteria) this;
        }

        public Criteria andGiftItemIdNotBetween(String value1, String value2) {
            addCriterion("gift_item_id not between", value1, value2, "giftItemId");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameIsNull() {
            addCriterion("gift_item_name is null");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameIsNotNull() {
            addCriterion("gift_item_name is not null");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameEqualTo(String value) {
            addCriterion("gift_item_name =", value, "giftItemName");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameNotEqualTo(String value) {
            addCriterion("gift_item_name <>", value, "giftItemName");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameGreaterThan(String value) {
            addCriterion("gift_item_name >", value, "giftItemName");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameGreaterThanOrEqualTo(String value) {
            addCriterion("gift_item_name >=", value, "giftItemName");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameLessThan(String value) {
            addCriterion("gift_item_name <", value, "giftItemName");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameLessThanOrEqualTo(String value) {
            addCriterion("gift_item_name <=", value, "giftItemName");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameLike(String value) {
            addCriterion("gift_item_name like", value, "giftItemName");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameNotLike(String value) {
            addCriterion("gift_item_name not like", value, "giftItemName");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameIn(List<String> values) {
            addCriterion("gift_item_name in", values, "giftItemName");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameNotIn(List<String> values) {
            addCriterion("gift_item_name not in", values, "giftItemName");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameBetween(String value1, String value2) {
            addCriterion("gift_item_name between", value1, value2, "giftItemName");
            return (Criteria) this;
        }

        public Criteria andGiftItemNameNotBetween(String value1, String value2) {
            addCriterion("gift_item_name not between", value1, value2, "giftItemName");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlIsNull() {
            addCriterion("gift_img_url is null");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlIsNotNull() {
            addCriterion("gift_img_url is not null");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlEqualTo(String value) {
            addCriterion("gift_img_url =", value, "giftImgUrl");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlNotEqualTo(String value) {
            addCriterion("gift_img_url <>", value, "giftImgUrl");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlGreaterThan(String value) {
            addCriterion("gift_img_url >", value, "giftImgUrl");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlGreaterThanOrEqualTo(String value) {
            addCriterion("gift_img_url >=", value, "giftImgUrl");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlLessThan(String value) {
            addCriterion("gift_img_url <", value, "giftImgUrl");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlLessThanOrEqualTo(String value) {
            addCriterion("gift_img_url <=", value, "giftImgUrl");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlLike(String value) {
            addCriterion("gift_img_url like", value, "giftImgUrl");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlNotLike(String value) {
            addCriterion("gift_img_url not like", value, "giftImgUrl");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlIn(List<String> values) {
            addCriterion("gift_img_url in", values, "giftImgUrl");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlNotIn(List<String> values) {
            addCriterion("gift_img_url not in", values, "giftImgUrl");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlBetween(String value1, String value2) {
            addCriterion("gift_img_url between", value1, value2, "giftImgUrl");
            return (Criteria) this;
        }

        public Criteria andGiftImgUrlNotBetween(String value1, String value2) {
            addCriterion("gift_img_url not between", value1, value2, "giftImgUrl");
            return (Criteria) this;
        }

        public Criteria andGiftCountIsNull() {
            addCriterion("gift_count is null");
            return (Criteria) this;
        }

        public Criteria andGiftCountIsNotNull() {
            addCriterion("gift_count is not null");
            return (Criteria) this;
        }

        public Criteria andGiftCountEqualTo(Integer value) {
            addCriterion("gift_count =", value, "giftCount");
            return (Criteria) this;
        }

        public Criteria andGiftCountNotEqualTo(Integer value) {
            addCriterion("gift_count <>", value, "giftCount");
            return (Criteria) this;
        }

        public Criteria andGiftCountGreaterThan(Integer value) {
            addCriterion("gift_count >", value, "giftCount");
            return (Criteria) this;
        }

        public Criteria andGiftCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("gift_count >=", value, "giftCount");
            return (Criteria) this;
        }

        public Criteria andGiftCountLessThan(Integer value) {
            addCriterion("gift_count <", value, "giftCount");
            return (Criteria) this;
        }

        public Criteria andGiftCountLessThanOrEqualTo(Integer value) {
            addCriterion("gift_count <=", value, "giftCount");
            return (Criteria) this;
        }

        public Criteria andGiftCountIn(List<Integer> values) {
            addCriterion("gift_count in", values, "giftCount");
            return (Criteria) this;
        }

        public Criteria andGiftCountNotIn(List<Integer> values) {
            addCriterion("gift_count not in", values, "giftCount");
            return (Criteria) this;
        }

        public Criteria andGiftCountBetween(Integer value1, Integer value2) {
            addCriterion("gift_count between", value1, value2, "giftCount");
            return (Criteria) this;
        }

        public Criteria andGiftCountNotBetween(Integer value1, Integer value2) {
            addCriterion("gift_count not between", value1, value2, "giftCount");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeIsNull() {
            addCriterion("customer_code is null");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeIsNotNull() {
            addCriterion("customer_code is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeEqualTo(String value) {
            addCriterion("customer_code =", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeNotEqualTo(String value) {
            addCriterion("customer_code <>", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeGreaterThan(String value) {
            addCriterion("customer_code >", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeGreaterThanOrEqualTo(String value) {
            addCriterion("customer_code >=", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeLessThan(String value) {
            addCriterion("customer_code <", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeLessThanOrEqualTo(String value) {
            addCriterion("customer_code <=", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeLike(String value) {
            addCriterion("customer_code like", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeNotLike(String value) {
            addCriterion("customer_code not like", value, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeIn(List<String> values) {
            addCriterion("customer_code in", values, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeNotIn(List<String> values) {
            addCriterion("customer_code not in", values, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeBetween(String value1, String value2) {
            addCriterion("customer_code between", value1, value2, "customerCode");
            return (Criteria) this;
        }

        public Criteria andCustomerCodeNotBetween(String value1, String value2) {
            addCriterion("customer_code not between", value1, value2, "customerCode");
            return (Criteria) this;
        }

        public Criteria andRequestDeliveryDateIsNull() {
            addCriterion("request_delivery_date is null");
            return (Criteria) this;
        }

        public Criteria andRequestDeliveryDateIsNotNull() {
            addCriterion("request_delivery_date is not null");
            return (Criteria) this;
        }

        public Criteria andRequestDeliveryDateEqualTo(Date value) {
            addCriterion("request_delivery_date =", value, "requestDeliveryDate");
            return (Criteria) this;
        }

        public Criteria andRequestDeliveryDateNotEqualTo(Date value) {
            addCriterion("request_delivery_date <>", value, "requestDeliveryDate");
            return (Criteria) this;
        }

        public Criteria andRequestDeliveryDateGreaterThan(Date value) {
            addCriterion("request_delivery_date >", value, "requestDeliveryDate");
            return (Criteria) this;
        }

        public Criteria andRequestDeliveryDateGreaterThanOrEqualTo(Date value) {
            addCriterion("request_delivery_date >=", value, "requestDeliveryDate");
            return (Criteria) this;
        }

        public Criteria andRequestDeliveryDateLessThan(Date value) {
            addCriterion("request_delivery_date <", value, "requestDeliveryDate");
            return (Criteria) this;
        }

        public Criteria andRequestDeliveryDateLessThanOrEqualTo(Date value) {
            addCriterion("request_delivery_date <=", value, "requestDeliveryDate");
            return (Criteria) this;
        }

        public Criteria andRequestDeliveryDateIn(List<Date> values) {
            addCriterion("request_delivery_date in", values, "requestDeliveryDate");
            return (Criteria) this;
        }

        public Criteria andRequestDeliveryDateNotIn(List<Date> values) {
            addCriterion("request_delivery_date not in", values, "requestDeliveryDate");
            return (Criteria) this;
        }

        public Criteria andRequestDeliveryDateBetween(Date value1, Date value2) {
            addCriterion("request_delivery_date between", value1, value2, "requestDeliveryDate");
            return (Criteria) this;
        }

        public Criteria andRequestDeliveryDateNotBetween(Date value1, Date value2) {
            addCriterion("request_delivery_date not between", value1, value2, "requestDeliveryDate");
            return (Criteria) this;
        }

        public Criteria andItemUnitIsNull() {
            addCriterion("item_unit is null");
            return (Criteria) this;
        }

        public Criteria andItemUnitIsNotNull() {
            addCriterion("item_unit is not null");
            return (Criteria) this;
        }

        public Criteria andItemUnitEqualTo(String value) {
            addCriterion("item_unit =", value, "itemUnit");
            return (Criteria) this;
        }

        public Criteria andItemUnitNotEqualTo(String value) {
            addCriterion("item_unit <>", value, "itemUnit");
            return (Criteria) this;
        }

        public Criteria andItemUnitGreaterThan(String value) {
            addCriterion("item_unit >", value, "itemUnit");
            return (Criteria) this;
        }

        public Criteria andItemUnitGreaterThanOrEqualTo(String value) {
            addCriterion("item_unit >=", value, "itemUnit");
            return (Criteria) this;
        }

        public Criteria andItemUnitLessThan(String value) {
            addCriterion("item_unit <", value, "itemUnit");
            return (Criteria) this;
        }

        public Criteria andItemUnitLessThanOrEqualTo(String value) {
            addCriterion("item_unit <=", value, "itemUnit");
            return (Criteria) this;
        }

        public Criteria andItemUnitLike(String value) {
            addCriterion("item_unit like", value, "itemUnit");
            return (Criteria) this;
        }

        public Criteria andItemUnitNotLike(String value) {
            addCriterion("item_unit not like", value, "itemUnit");
            return (Criteria) this;
        }

        public Criteria andItemUnitIn(List<String> values) {
            addCriterion("item_unit in", values, "itemUnit");
            return (Criteria) this;
        }

        public Criteria andItemUnitNotIn(List<String> values) {
            addCriterion("item_unit not in", values, "itemUnit");
            return (Criteria) this;
        }

        public Criteria andItemUnitBetween(String value1, String value2) {
            addCriterion("item_unit between", value1, value2, "itemUnit");
            return (Criteria) this;
        }

        public Criteria andItemUnitNotBetween(String value1, String value2) {
            addCriterion("item_unit not between", value1, value2, "itemUnit");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitIsNull() {
            addCriterion("gift_item_unit is null");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitIsNotNull() {
            addCriterion("gift_item_unit is not null");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitEqualTo(String value) {
            addCriterion("gift_item_unit =", value, "giftItemUnit");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitNotEqualTo(String value) {
            addCriterion("gift_item_unit <>", value, "giftItemUnit");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitGreaterThan(String value) {
            addCriterion("gift_item_unit >", value, "giftItemUnit");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitGreaterThanOrEqualTo(String value) {
            addCriterion("gift_item_unit >=", value, "giftItemUnit");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitLessThan(String value) {
            addCriterion("gift_item_unit <", value, "giftItemUnit");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitLessThanOrEqualTo(String value) {
            addCriterion("gift_item_unit <=", value, "giftItemUnit");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitLike(String value) {
            addCriterion("gift_item_unit like", value, "giftItemUnit");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitNotLike(String value) {
            addCriterion("gift_item_unit not like", value, "giftItemUnit");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitIn(List<String> values) {
            addCriterion("gift_item_unit in", values, "giftItemUnit");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitNotIn(List<String> values) {
            addCriterion("gift_item_unit not in", values, "giftItemUnit");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitBetween(String value1, String value2) {
            addCriterion("gift_item_unit between", value1, value2, "giftItemUnit");
            return (Criteria) this;
        }

        public Criteria andGiftItemUnitNotBetween(String value1, String value2) {
            addCriterion("gift_item_unit not between", value1, value2, "giftItemUnit");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}