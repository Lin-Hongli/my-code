package com.liby.promotion.base.filter;

import com.alibaba.dubbo.rpc.*;
import com.liby.common.data.ContextHelper;
import com.liby.common.data.LogData;
import com.liby.common.logging.Log;

import java.util.Date;

/**
 * DubboConsumerFilter 消费者通用拦截器
 *
 * @author lgj liugongjun@chinasie.com
 * @version 0.0.1
 * @date 2019年01月09日
 */
public class DubboConsumerFilter implements Filter {
    private static final Log log = Log.get();

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        RpcContext rpcContext = RpcContext.getContext();
        LogData logData = ContextHelper.initLogData();

        rpcContext.setAttachment("logId", logData.getLogId());
        rpcContext.setAttachment("traceId", logData.getTraceId());

        logData.setReqType(LogData.ReqType.DUBBO);
        logData.setLogMethodParams(rpcContext.getArguments());
        logData.setStartDate(new Date());
        log.info("start request dubbo:", logData);
        Result result;
        try {
            result = invoker.invoke(invocation);
            logData.setLogMethodParams(result.getValue());
        } finally {
            logData.setEndDate(new Date());
            logData.setLogMethodParamsType(LogData.MethodParamsType.OUT);
            log.info("end request dubbo:", logData);
        }
        return result;
    }
}
