package com.liby.promotion.gift.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liby.promotion.gift.service.dao.mybatisPlus.BigGoodsDao;
import com.liby.promotion.gift.service.entity.BigGoods;
import com.liby.promotion.gift.service.BigGoodsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author weiyunhui
 * @since 2020-11-19
 */
@Service
public class BigGoodsServiceImpl extends ServiceImpl<BigGoodsDao, BigGoods> implements BigGoodsService {

}
