package com.liby.promotion.base.rpcservice.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.liby.promotion.common.constants.SysConstants;
import com.liby.promotion.common.dto.BaseDTO;
import com.liby.promotion.common.dto.CurrentUser;
import com.liby.promotion.common.entity.BaseEntity;
import com.liby.promotion.common.redis.service.RedisCacheService;
import com.liby.promotion.common.result.Page;
import com.liby.promotion.common.result.Result;
import com.liby.promotion.common.service.rpc.IBaseExportService;
import com.liby.promotion.common.util.FilterParser;
import com.liby.promotion.common.util.ScanSqlUtil;
import com.liby.promotion.common.util.ValidateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.lang.reflect.ParameterizedType;
import java.util.*;

public abstract class BaseExportServiceImpl<DTO extends BaseDTO, ENTITY extends BaseEntity, SERVICE extends IService<ENTITY>> extends RedisCacheService<DTO> implements IBaseExportService<DTO> {

    private Class<ENTITY> entityClass;
    private Class<DTO> dtoClass;

    @Autowired
    protected SERVICE service;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public BaseExportServiceImpl() {
        ParameterizedType type = (ParameterizedType)this.getClass().getGenericSuperclass();
        this.dtoClass = (Class)type.getActualTypeArguments()[0];
        this.entityClass = (Class)type.getActualTypeArguments()[1];
    }

    @Override
    public Result pageList(DTO dto) {
        return cachePage(dto, (params) ->{
            String filter = dto.getFilter();
            String orderBy = dto.getOrderBy();
            Integer currentPage = dto.getCurrentPage();
            Integer pageSize = dto.getPageSize();
            String sql = ScanSqlUtil.getSqlText(this, "sql.xml", "pageList");
            String queryStr = FilterParser.getCondition(filter);
            String where = ValidateUtil.isAvailable(queryStr) ? queryStr : "";
            String order = " order by " + orderBy;
            String countSql = " select count(*) from (" + sql + ") x where 1 = 1 " + where;
            String dataSql = "select * from (" + sql + ") x where 1 = 1 " + where + order + " limit " + (currentPage - 1) + ", " + pageSize;
            Integer total = jdbcTemplate.queryForObject(countSql, Number.class).intValue();
            List<DTO> dataList = jdbcTemplate.query(dataSql, new BeanPropertyRowMapper(dtoClass));
            Page<DTO> page = new Page();
            page.setCurrentPage(currentPage);
            page.setPageSize(pageSize);
            page.setData(dataList);
            page.setTotal(total);
            return Result.succ(page);
        });
    }

    @Override
    public Result selectById(DTO dto) {
        Long id = dto.getId();
        return cacheOne(id, (params) -> {
            ENTITY entity = service.getById(id);
            if(entity == null) {
                return Result.succ();
            }
            DTO newDto = dtoClass.newInstance();
            BeanUtils.copyProperties(entity, newDto);
            return Result.succ(newDto);
        });
    }

    @Transactional
    @Override
    public Result save(DTO dto, CurrentUser currentUser, String... groups) {
        return cacheSave((params) -> {
            ENTITY entity = entityClass.newInstance();
            BeanUtils.copyProperties(dto, entity);
            entity.setCreateTime(new Date());
            entity.setCreaterId(currentUser.getId());
            entity.setCreaterName(currentUser.getName());
            service.save(entity);
            return Result.succ(entity);
        }, groups);
    }

    @Transactional
    @Override
    public Result updateById(DTO dto, CurrentUser currentUser, String... groups) {
        Result result = cacheUpdate(dto.getId(), (params) -> {
            ENTITY entity = entityClass.newInstance();
            BeanUtils.copyProperties(dto, entity);
            entity.setModifierId(currentUser.getId());
            entity.setModifyTime(new Date());
            entity.setModifierName(currentUser.getName());
            service.updateById(entity);
            return Result.succ(entity);
        }, groups);
        return result;
    }

    @Transactional
    @Override
    public Result deleteById(DTO dto, CurrentUser currentUser, String... groups) {
        Long id = dto.getId();
        Result result = cacheUpdate(id, (params) -> {
            ENTITY entity = service.getById(id);
            if(entity != null) {
                entity.setModifierId(currentUser.getId());
                entity.setModifierName(currentUser.getName());
                entity.setModifyTime(new Date());
                service.updateById(entity);
                service.removeById(id);
                return Result.succ();
            } else {
                return Result.fail("数据【id = " + id + "】不存在");
            }
        }, groups);
        return result;
    }

    @Override
    @Transactional
    public Result batchDeleteById(DTO dto, CurrentUser currentUser, String... groups) {
        String[] ids = dto.getIds().split(",");
        for (int i = 0; i < ids.length; i++) {
            dto.setId(Long.valueOf(ids[i]));
            Result result = deleteById(dto, currentUser, groups);
            if(!result.isSuccess()) {
                throw new RuntimeException(result.getError());
            }
        }
        return Result.succ();
    }

    @Override
    public Result getExportTableData(DTO dto) {
        try {
            JSONArray headerJsonArray = dto.headerJsonArray();
            Assert.notEmpty(headerJsonArray, "表头信息为空");
            String[] titles = new String[headerJsonArray.size()];
            String[] fields = new String[headerJsonArray.size()];
            for (int i = 0; i < headerJsonArray.size(); i++) {
                JSONObject headerJson = headerJsonArray.getJSONObject(i);
                String title = headerJson.getString("title");
                Assert.hasLength(title, "表头信息信息不全");
                titles[i] = title;
                String key = headerJson.getString("key");
                Assert.hasLength(key, "表头信息信息不全");
                fields[i] = key;
            }
            List<Map<String, Object>> data = null;
            String exportType = dto.getExportType();
            if(SysConstants.EXPORT_TYPE_SOME.equals(exportType)) {
                JSONArray selectDataJsonArray = dto.selectDataJsonArray();
                Assert.notEmpty(selectDataJsonArray, "没有选中任何数据");
                data = new ArrayList<>();
                for (int i = 0; i < selectDataJsonArray.size(); i++) {
                    data.add(selectDataJsonArray.getJSONObject(i));
                }
            } else if(SysConstants.EXPORT_TYPE_ALL.equals(exportType)) {
                Result result = cacheExport(dto, (p) -> {
                    String filter = dto.getFilter();
                    String queryStr = FilterParser.getCondition(filter);
                    String where = ValidateUtil.isAvailable(queryStr) ? queryStr : "";
                    String orderBy = dto.getOrderBy();
                    String order = " order by " + orderBy;
                    String heartSql = StringUtils.join(fields, ",");
                    String sql = ScanSqlUtil.getSqlText(this, "sql.xml", "pageList");
                    sql = "select " + heartSql + " from (" + sql + ") x where 1 = 1 " + where + order;
                    List<Map<String, Object>> dataList = jdbcTemplate.queryForList(sql);
                    return Result.succ(dataList);
                });
                if(!result.isSuccess()) {
                    return result;
                }
                data = (List<Map<String, Object>>) result.getData();
            } else {
                throw new RuntimeException("导出类型不正确");
            }
            List<Object[]> dataList = new ArrayList<>();
            for (int i = 0; i < data.size(); i++) {
                Map<String, Object> map = data.get(i);
                Object[] d = new Object[fields.length];
                for (int j = 0; j < fields.length; j++) {
                    String field = fields[j];
                    d[j] = map.get(field);
                }
                dataList.add(d);
            }
            Map<String, Object> map = new HashMap<>();
            map.put("dataList", dataList);
            map.put("titles", titles);
            return Result.succ(map);
        } catch (Exception e) {
            return Result.fail(e);
        }
    }
}
