package com.liby.promotion.order.service.dao.mybatisPlus;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liby.promotion.order.export.dto.QureyOrderRequest;
import com.liby.promotion.order.export.dto.QureyOrderResponse;
import com.liby.promotion.order.service.entity.BigOrderMain;
import com.liby.promotion.order.service.entity.BigOrderMainExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BigOrderMainMapper extends BaseMapper<BigOrderMain> {
    int countByExample(BigOrderMainExample example);

    int deleteByExample(BigOrderMainExample example);

    int deleteByPrimaryKey(String orderId);

    int insert(BigOrderMain record);

    int insertSelective(BigOrderMain record);

    List<BigOrderMain> selectByExampleWithBLOBs(BigOrderMainExample example);

    List<BigOrderMain> selectByExample(BigOrderMainExample example);

    BigOrderMain selectByPrimaryKey(String orderId);

    List<QureyOrderResponse> selectGiftPacks(@Param("qureyOrderRequest") QureyOrderRequest qureyOrderRequest);

    int updateByExampleSelective(@Param("record") BigOrderMain record, @Param("example") BigOrderMainExample example);

    int updateByExampleWithBLOBs(@Param("record") BigOrderMain record, @Param("example") BigOrderMainExample example);

    int updateByExample(@Param("record") BigOrderMain record, @Param("example") BigOrderMainExample example);

    int updateByPrimaryKeySelective(BigOrderMain record);

    int updateByPrimaryKeyWithBLOBs(BigOrderMain record);

    int updateByPrimaryKey(BigOrderMain record);

    List<QureyOrderResponse> exportGiftPacksOrder(@Param("qureyOrderRequest") QureyOrderRequest qureyOrderRequest);

}