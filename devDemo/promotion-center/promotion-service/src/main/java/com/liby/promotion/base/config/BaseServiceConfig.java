package com.liby.promotion.base.config;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;


/**
 * BaseServiceConfig 服务配置初始化
 *
 * @author lgj liugongjun@chinasie.com
 * @version 0.0.1
 * @date 2018年11月29日
 */
@Configuration
public class BaseServiceConfig implements InitializingBean {

    @Override
    public void afterPropertiesSet() {

    }
}
