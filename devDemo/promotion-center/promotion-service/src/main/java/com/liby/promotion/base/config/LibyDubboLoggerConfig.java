package com.liby.promotion.base.config;

import com.alibaba.dubbo.common.logger.LoggerFactory;
import com.alibaba.dubbo.common.logger.slf4j.Slf4jLoggerAdapter;
import org.springframework.context.annotation.Configuration;

/**
 * LibyDubboLoggerConfig dubbo 协议配置
 *
 * @author lgj liugongjun@chinasie.com
 * @version 0.0.1
 * @date 2018年11月29日
 */
@Configuration
public class LibyDubboLoggerConfig {
    static {
        LoggerFactory.setLoggerAdapter(new Slf4jLoggerAdapter());
    }
}
