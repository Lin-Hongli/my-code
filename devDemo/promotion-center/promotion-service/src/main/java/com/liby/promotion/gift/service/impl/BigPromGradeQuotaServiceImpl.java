package com.liby.promotion.gift.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liby.promotion.gift.service.BigPromGradeQuotaService;
import com.liby.promotion.gift.service.dao.mybatisPlus.BigPromGradeQuotaDao;
import com.liby.promotion.gift.service.entity.BigPromGradeQuota;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 大礼包活动星级配置表 服务实现类
 * </p>
 *
 * @author weiyunhui
 * @since 2020-12-02
 */
@Service
public class BigPromGradeQuotaServiceImpl extends ServiceImpl<BigPromGradeQuotaDao, BigPromGradeQuota> implements BigPromGradeQuotaService {
    @Resource
    BigPromGradeQuotaDao bigPromGradeQuotaDao;

    @Override
    public int insertPromGradeQuotaAll(List<BigPromGradeQuota> bigPromGradeQuotas) {
        for (BigPromGradeQuota bigProvincialDistribution :
                bigPromGradeQuotas) {
            bigPromGradeQuotaDao.insert(bigProvincialDistribution);
        }
        return 1;
    }
}
