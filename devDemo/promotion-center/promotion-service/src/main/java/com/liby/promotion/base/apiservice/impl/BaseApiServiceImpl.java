package com.liby.promotion.base.apiservice.impl;


import com.liby.promotion.common.redis.service.RedisUtil;
import com.liby.promotion.common.result.Result;
import com.liby.promotion.common.service.api.IBaseApiService;
import com.liby.promotion.common.util.*;
import com.liby.promotion.common.vo.FileVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class BaseApiServiceImpl implements IBaseApiService {

    @Autowired(required = false)
    protected RedisUtil redisUtil;
    @Autowired(required = false)
    protected RedisUtil<Object, String> stringRedisUtil;

    protected Result mkdirs(String tempPath) {
        if(!ValidateUtil.isAvailable(tempPath)) {
            return Result.fail("临时文件目录为空，请联系管理员指定");
        }
        File file = new File(tempPath);
        if(!file.exists()) {
            if(!file.mkdirs()) {
                return Result.fail("创建临时文件目录失败");
            }
        }
        return Result.succ();
    }

    @Override
    public Result exportTableData(String tempPath, CallBack<Result> callBack) {
        try {
            Result result = mkdirs(tempPath);
            if(!result.isSuccess()) {
                return result;
            }
            result = callBack.doCallBack();
            if(!result.isSuccess()) {
                log.info(result.getError());
                return result;
            }
            Map<String, Object> map = (Map<String, Object>) result.getData();
            String[] titles = (String[]) map.get("titles");
            List<Object[]> dataList = (List<Object[]>) map.get("dataList");
            Workbook wb = ExcelUtil.createXSSFWorkbook(titles, dataList);
            String originFileName = StringUtil.getUuid() + ".xlsx";
            ExcelUtil.autoSizeColumn(wb, 0, 0, titles.length);
            result = ExcelUtil.writeFile(tempPath + originFileName, wb);
            if(!result.isSuccess()) {
                log.info(result.getError());
                return result;
            }
            String fileName = "标准导出【" + DateUtils.format(new Date(), "yyyyMMddHHmmss") + "】" + ".xlsx";
            map = new HashMap<>();
            map.put("originFileName", originFileName);
            map.put("fileName", fileName);
            return Result.succ(map);
        } catch (Exception e) {
            return Result.fail(e);
        }
    }

    @Override
    public Result download(String tempPath, FileVO fileVO) {
        File file = null;
        try {
            String originFileName = URLDecoder.decode(fileVO.getOriginFileName(), "utf-8");
            file = new File(tempPath.concat(originFileName));
            if(!file.exists()) {
                return Result.fail("目标文件【" + originFileName + "】不存在");
            }
            byte[] bytes = FileUtil.readFile(file);
            if(bytes == null) {
                return Result.fail("读取目标文件【" + originFileName + "】失败");
            }
            return Result.succ(bytes);
        } catch (Exception e) {
            return Result.fail(e);
        } finally {
            if(!fileVO.getKeepFile() && file != null && file.exists()) {
                file.delete();
            }
        }
    }
}
