package com.liby.promotion.order.service;

import com.github.pagehelper.PageInfo;
import com.liby.promotion.order.export.dto.QureyOrderRequest;
import com.liby.promotion.order.export.dto.QureyOrderResponse;

import java.util.List;

public interface PTradeSecondaryOrderService {

    PageInfo<QureyOrderResponse> qureyGiftPacksOrder(QureyOrderRequest qureyOrderRequest);

    List<QureyOrderResponse> exportGiftPacksOrder(QureyOrderRequest qureyOrderRequest);

}
