package com.liby.promotion.gift.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liby.promotion.gift.service.BigProvincialDistributionService;
import com.liby.promotion.gift.service.dao.mybatisPlus.BigProvincialDistributionDao;
import com.liby.promotion.gift.service.entity.BigProvincialDistribution;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 省区分配量 服务实现类
 * </p>
 *
 * @author weiyunhui
 * @since 2020-12-02
 */
@Transactional
@Service
public class BigProvincialDistributionServiceImpl extends ServiceImpl<BigProvincialDistributionDao, BigProvincialDistribution> implements BigProvincialDistributionService {
    @Resource
    BigProvincialDistributionDao bigProvincialDistributionDao;

    @Override
    public int insertProvincialAll(List<BigProvincialDistribution> bigProvincialDistributions) {
        for (BigProvincialDistribution bigProvincialDistribution :
                bigProvincialDistributions) {
            bigProvincialDistributionDao.insert(bigProvincialDistribution);
        }
        return 1;
    }
}
