package com.liby.promotion.base.filter;

import com.alibaba.dubbo.rpc.*;
import com.liby.common.data.ContextHelper;
import com.liby.common.logging.Log;
import com.liby.promotion.common.constants.SysConstants;
import org.apache.catalina.connector.RequestFacade;
import org.apache.commons.lang3.LocaleUtils;
import org.springframework.util.StringUtils;

import java.util.Locale;

/**
 * DubboLocaleFilter 国际化处理
 *
 * @author lgj liugongjun@chinasie.com
 * @version 0.0.1
 * @date 2018年11月29日
 */
public class DubboLocaleFilter implements Filter {
    private static final Log log = Log.get();

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        //
        RequestFacade request = (RequestFacade) RpcContext.getContext().getRequest();
        String locale;
        Locale tmpLocale = null;
        if (request == null) {
            locale = invocation.getAttachment(SysConstants.LOCALE);
        } else {
            locale = request.getParameter(SysConstants.LOCALE);
            if (StringUtils.isEmpty(locale)) {
                tmpLocale = request.getLocale();
            }
        }

        if (tmpLocale == null) {
            if (!StringUtils.isEmpty(locale)) {
                tmpLocale = LocaleUtils.toLocale(locale);
                ContextHelper.setLocale(tmpLocale);
            }
        } else {
            ContextHelper.setLocale(tmpLocale);
        }


        Result result = invoker.invoke(invocation);

        ContextHelper.clear();
        return result;
    }
}
