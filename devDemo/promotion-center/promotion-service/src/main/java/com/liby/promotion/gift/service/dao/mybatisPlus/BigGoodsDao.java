package com.liby.promotion.gift.service.dao.mybatisPlus;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liby.promotion.gift.service.entity.BigGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author weiyunhui
 * @since 2020-11-19
 */
public interface BigGoodsDao extends BaseMapper<BigGoods> {
   int insertList(@Param("BigGoods")List<BigGoods> bigGoodsList);
}
