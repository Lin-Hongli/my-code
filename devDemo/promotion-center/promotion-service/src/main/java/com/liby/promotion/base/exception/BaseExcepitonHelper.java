package com.liby.promotion.base.exception;

import com.liby.common.type.BaseType;
import com.liby.common.type.LibyType;

/**
 * BaseExcepitonHelper base异常代码管理
 *
 * @author lgj liugongjun@chinasie.com
 * @version 0.0.1
 * @date 2018年12月02日
 */
public class BaseExcepitonHelper extends BaseType {
    private static final LibyType<BaseExcepitonHelper> TYPES = new LibyType<>();

    //service无继承
    public static final BaseExcepitonHelper BASE_SERVICE_001 = TYPES.create(2000, "exp-base-service-001");
    //token缺失
    public static final BaseExcepitonHelper BASE_SERVICE_002 = TYPES.create(2000, "exp-base-service-002");

}
