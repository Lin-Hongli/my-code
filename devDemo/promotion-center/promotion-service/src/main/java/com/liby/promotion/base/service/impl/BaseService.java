package com.liby.promotion.base.service.impl;

import com.github.pagehelper.Page;
import com.liby.common.bo.CommonBo;
import com.liby.common.exception.LibyException;
import com.liby.promotion.base.dao.ILibyBaseDao;
import com.liby.promotion.base.exception.BaseExcepitonHelper;
import com.liby.promotion.base.service.IBaseService;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * BaseService 基础服务
 *
 * @author lgj liugongjun@chinasie.com
 * @version 0.0.1
 * @date 2018年12月02日
 */
public class BaseService<T extends ILibyBaseDao> implements IBaseService {
    private static final String DOT = ".";
    //服务提供类
    private Class<T> clazz;
    //服务接口类
    private Class<T> tClass;

    public BaseService() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String clsName = stackTrace[2].getClassName();
        try {
            this.clazz = (Class<T>) Class.forName(clsName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        this.tClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public CommonBo getCommonBo() {
        return commonBo;
    }

    public void setCommonBo(CommonBo commonBo) {
        this.commonBo = commonBo;
    }

    @Autowired
    protected CommonBo commonBo;

    protected String getClassName() {
        if (!ILibyBaseDao.class.isAssignableFrom(tClass)) {
            throw new LibyException(BaseExcepitonHelper.BASE_SERVICE_001, tClass.getName(), ILibyBaseDao.class.getName());
        }
        if (!BaseService.class.isAssignableFrom(clazz)) {
            throw new LibyException(BaseExcepitonHelper.BASE_SERVICE_001, clazz.getName(), BaseService.class.getName());
        }
        return tClass.getName();
    }

    protected String getMethod() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String clsName = stackTrace[3].getMethodName();
        return clsName;
    }

    @Override
    public <T> T selectOne(Object param) {
        String st = getClassName() + DOT + getMethod();

        return this.commonBo.selectOne(st, param);
    }

    @Override
    public <T> T selectOne() {
        String st = getClassName() + DOT + getMethod();
        return this.commonBo.selectOne(st);
    }

    @Override
    public <T> List<T> selectList() {
        String st = getClassName() + DOT + getMethod();
        return this.commonBo.selectList(st);
    }

    @Override
    public <T> List<T> selectList(Page page) {
        String st = getClassName() + DOT + getMethod();
        return this.commonBo.selectList(st, page);
    }

    @Override
    public <T> List<T> selectList(Object parameter) {
        String st = getClassName() + DOT + getMethod();
        return this.commonBo.selectList(st, parameter);
    }

    @Override
    public <T> List<T> selectList(Object parameter, Page page) {
        String st = getClassName() + DOT + getMethod();
        return this.commonBo.selectList(st, parameter, page);
    }

    @Override
    public int insert(Object parameter) {
        String st = getClassName() + DOT + getMethod();
        return this.commonBo.insert(st, parameter);
    }

    @Override
    public int insert() {
        String st = getClassName() + DOT + getMethod();
        return this.commonBo.insert(st);
    }

    @Override
    public void insert(List<Object> params) {
        String st = getClassName() + DOT + getMethod();
        this.commonBo.insert(st, params);
    }

    @Override
    public void batchInsert(List<Object> params, int batchNum) {
        String st = getClassName() + DOT + getMethod();
        this.commonBo.batchInsert(st, params, batchNum);
    }

    @Override
    public void update(List<Object> params) {
        String st = getClassName() + DOT + getMethod();
        this.commonBo.update(st, params);
    }

    @Override
    public int update(Object parameter) {
        String st = getClassName() + DOT + getMethod();
        return this.commonBo.update(st, parameter);
    }

    @Override
    public int update() {
        String st = getClassName() + DOT + getMethod();
        return this.commonBo.update(st);
    }

    @Override
    public int delete(Object parameter) {
        String st = getClassName() + DOT + getMethod();
        return this.commonBo.delete(st, parameter);
    }

    @Override
    public int delete() {
        String st = getClassName() + DOT + getMethod();
        return this.commonBo.delete(st);
    }

    @Override
    public void delete(List<Object> params) {
        String st = getClassName() + DOT + getMethod();
        this.commonBo.delete(st, params);
    }

    @Override
    public void batchUpdate(List<Object> data, int batchNum) {
        String st = getClassName() + DOT + getMethod();
        this.commonBo.batchUpdate(st, data, batchNum);
    }

    @Override
    public void batchDelete(List<Object> data, int batchNum) {
        String st = getClassName() + DOT + getMethod();
        this.commonBo.batchDelete(st, data, batchNum);
    }
}
