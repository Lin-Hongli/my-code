package com.liby.promotion.order.service.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BigPromotionInfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BigPromotionInfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCodeIsNull() {
            addCriterion("code is null");
            return (Criteria) this;
        }

        public Criteria andCodeIsNotNull() {
            addCriterion("code is not null");
            return (Criteria) this;
        }

        public Criteria andCodeEqualTo(String value) {
            addCriterion("code =", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotEqualTo(String value) {
            addCriterion("code <>", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThan(String value) {
            addCriterion("code >", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThanOrEqualTo(String value) {
            addCriterion("code >=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThan(String value) {
            addCriterion("code <", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThanOrEqualTo(String value) {
            addCriterion("code <=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLike(String value) {
            addCriterion("code like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotLike(String value) {
            addCriterion("code not like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeIn(List<String> values) {
            addCriterion("code in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotIn(List<String> values) {
            addCriterion("code not in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeBetween(String value1, String value2) {
            addCriterion("code between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotBetween(String value1, String value2) {
            addCriterion("code not between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Boolean value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Boolean value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Boolean value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Boolean value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Boolean value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Boolean value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Boolean> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Boolean> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Boolean value1, Boolean value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Boolean value1, Boolean value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andGifTypeIsNull() {
            addCriterion("gif_type is null");
            return (Criteria) this;
        }

        public Criteria andGifTypeIsNotNull() {
            addCriterion("gif_type is not null");
            return (Criteria) this;
        }

        public Criteria andGifTypeEqualTo(Boolean value) {
            addCriterion("gif_type =", value, "gifType");
            return (Criteria) this;
        }

        public Criteria andGifTypeNotEqualTo(Boolean value) {
            addCriterion("gif_type <>", value, "gifType");
            return (Criteria) this;
        }

        public Criteria andGifTypeGreaterThan(Boolean value) {
            addCriterion("gif_type >", value, "gifType");
            return (Criteria) this;
        }

        public Criteria andGifTypeGreaterThanOrEqualTo(Boolean value) {
            addCriterion("gif_type >=", value, "gifType");
            return (Criteria) this;
        }

        public Criteria andGifTypeLessThan(Boolean value) {
            addCriterion("gif_type <", value, "gifType");
            return (Criteria) this;
        }

        public Criteria andGifTypeLessThanOrEqualTo(Boolean value) {
            addCriterion("gif_type <=", value, "gifType");
            return (Criteria) this;
        }

        public Criteria andGifTypeIn(List<Boolean> values) {
            addCriterion("gif_type in", values, "gifType");
            return (Criteria) this;
        }

        public Criteria andGifTypeNotIn(List<Boolean> values) {
            addCriterion("gif_type not in", values, "gifType");
            return (Criteria) this;
        }

        public Criteria andGifTypeBetween(Boolean value1, Boolean value2) {
            addCriterion("gif_type between", value1, value2, "gifType");
            return (Criteria) this;
        }

        public Criteria andGifTypeNotBetween(Boolean value1, Boolean value2) {
            addCriterion("gif_type not between", value1, value2, "gifType");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andImgUrlIsNull() {
            addCriterion("img_url is null");
            return (Criteria) this;
        }

        public Criteria andImgUrlIsNotNull() {
            addCriterion("img_url is not null");
            return (Criteria) this;
        }

        public Criteria andImgUrlEqualTo(String value) {
            addCriterion("img_url =", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlNotEqualTo(String value) {
            addCriterion("img_url <>", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlGreaterThan(String value) {
            addCriterion("img_url >", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlGreaterThanOrEqualTo(String value) {
            addCriterion("img_url >=", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlLessThan(String value) {
            addCriterion("img_url <", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlLessThanOrEqualTo(String value) {
            addCriterion("img_url <=", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlLike(String value) {
            addCriterion("img_url like", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlNotLike(String value) {
            addCriterion("img_url not like", value, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlIn(List<String> values) {
            addCriterion("img_url in", values, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlNotIn(List<String> values) {
            addCriterion("img_url not in", values, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlBetween(String value1, String value2) {
            addCriterion("img_url between", value1, value2, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andImgUrlNotBetween(String value1, String value2) {
            addCriterion("img_url not between", value1, value2, "imgUrl");
            return (Criteria) this;
        }

        public Criteria andTotalPriceIsNull() {
            addCriterion("total_price is null");
            return (Criteria) this;
        }

        public Criteria andTotalPriceIsNotNull() {
            addCriterion("total_price is not null");
            return (Criteria) this;
        }

        public Criteria andTotalPriceEqualTo(BigDecimal value) {
            addCriterion("total_price =", value, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceNotEqualTo(BigDecimal value) {
            addCriterion("total_price <>", value, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceGreaterThan(BigDecimal value) {
            addCriterion("total_price >", value, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total_price >=", value, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceLessThan(BigDecimal value) {
            addCriterion("total_price <", value, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total_price <=", value, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceIn(List<BigDecimal> values) {
            addCriterion("total_price in", values, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceNotIn(List<BigDecimal> values) {
            addCriterion("total_price not in", values, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_price between", value1, value2, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andTotalPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total_price not between", value1, value2, "totalPrice");
            return (Criteria) this;
        }

        public Criteria andStockIsNull() {
            addCriterion("stock is null");
            return (Criteria) this;
        }

        public Criteria andStockIsNotNull() {
            addCriterion("stock is not null");
            return (Criteria) this;
        }

        public Criteria andStockEqualTo(Integer value) {
            addCriterion("stock =", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotEqualTo(Integer value) {
            addCriterion("stock <>", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThan(Integer value) {
            addCriterion("stock >", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockGreaterThanOrEqualTo(Integer value) {
            addCriterion("stock >=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThan(Integer value) {
            addCriterion("stock <", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockLessThanOrEqualTo(Integer value) {
            addCriterion("stock <=", value, "stock");
            return (Criteria) this;
        }

        public Criteria andStockIn(List<Integer> values) {
            addCriterion("stock in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotIn(List<Integer> values) {
            addCriterion("stock not in", values, "stock");
            return (Criteria) this;
        }

        public Criteria andStockBetween(Integer value1, Integer value2) {
            addCriterion("stock between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andStockNotBetween(Integer value1, Integer value2) {
            addCriterion("stock not between", value1, value2, "stock");
            return (Criteria) this;
        }

        public Criteria andIsBuyGifIsNull() {
            addCriterion("is_buy_gif is null");
            return (Criteria) this;
        }

        public Criteria andIsBuyGifIsNotNull() {
            addCriterion("is_buy_gif is not null");
            return (Criteria) this;
        }

        public Criteria andIsBuyGifEqualTo(Boolean value) {
            addCriterion("is_buy_gif =", value, "isBuyGif");
            return (Criteria) this;
        }

        public Criteria andIsBuyGifNotEqualTo(Boolean value) {
            addCriterion("is_buy_gif <>", value, "isBuyGif");
            return (Criteria) this;
        }

        public Criteria andIsBuyGifGreaterThan(Boolean value) {
            addCriterion("is_buy_gif >", value, "isBuyGif");
            return (Criteria) this;
        }

        public Criteria andIsBuyGifGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_buy_gif >=", value, "isBuyGif");
            return (Criteria) this;
        }

        public Criteria andIsBuyGifLessThan(Boolean value) {
            addCriterion("is_buy_gif <", value, "isBuyGif");
            return (Criteria) this;
        }

        public Criteria andIsBuyGifLessThanOrEqualTo(Boolean value) {
            addCriterion("is_buy_gif <=", value, "isBuyGif");
            return (Criteria) this;
        }

        public Criteria andIsBuyGifIn(List<Boolean> values) {
            addCriterion("is_buy_gif in", values, "isBuyGif");
            return (Criteria) this;
        }

        public Criteria andIsBuyGifNotIn(List<Boolean> values) {
            addCriterion("is_buy_gif not in", values, "isBuyGif");
            return (Criteria) this;
        }

        public Criteria andIsBuyGifBetween(Boolean value1, Boolean value2) {
            addCriterion("is_buy_gif between", value1, value2, "isBuyGif");
            return (Criteria) this;
        }

        public Criteria andIsBuyGifNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_buy_gif not between", value1, value2, "isBuyGif");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceIsNull() {
            addCriterion("is_can_material_replace is null");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceIsNotNull() {
            addCriterion("is_can_material_replace is not null");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceEqualTo(Boolean value) {
            addCriterion("is_can_material_replace =", value, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceNotEqualTo(Boolean value) {
            addCriterion("is_can_material_replace <>", value, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceGreaterThan(Boolean value) {
            addCriterion("is_can_material_replace >", value, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_can_material_replace >=", value, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceLessThan(Boolean value) {
            addCriterion("is_can_material_replace <", value, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceLessThanOrEqualTo(Boolean value) {
            addCriterion("is_can_material_replace <=", value, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceIn(List<Boolean> values) {
            addCriterion("is_can_material_replace in", values, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceNotIn(List<Boolean> values) {
            addCriterion("is_can_material_replace not in", values, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceBetween(Boolean value1, Boolean value2) {
            addCriterion("is_can_material_replace between", value1, value2, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andIsCanMaterialReplaceNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_can_material_replace not between", value1, value2, "isCanMaterialReplace");
            return (Criteria) this;
        }

        public Criteria andMaxBuyCntIsNull() {
            addCriterion("max_buy_cnt is null");
            return (Criteria) this;
        }

        public Criteria andMaxBuyCntIsNotNull() {
            addCriterion("max_buy_cnt is not null");
            return (Criteria) this;
        }

        public Criteria andMaxBuyCntEqualTo(Integer value) {
            addCriterion("max_buy_cnt =", value, "maxBuyCnt");
            return (Criteria) this;
        }

        public Criteria andMaxBuyCntNotEqualTo(Integer value) {
            addCriterion("max_buy_cnt <>", value, "maxBuyCnt");
            return (Criteria) this;
        }

        public Criteria andMaxBuyCntGreaterThan(Integer value) {
            addCriterion("max_buy_cnt >", value, "maxBuyCnt");
            return (Criteria) this;
        }

        public Criteria andMaxBuyCntGreaterThanOrEqualTo(Integer value) {
            addCriterion("max_buy_cnt >=", value, "maxBuyCnt");
            return (Criteria) this;
        }

        public Criteria andMaxBuyCntLessThan(Integer value) {
            addCriterion("max_buy_cnt <", value, "maxBuyCnt");
            return (Criteria) this;
        }

        public Criteria andMaxBuyCntLessThanOrEqualTo(Integer value) {
            addCriterion("max_buy_cnt <=", value, "maxBuyCnt");
            return (Criteria) this;
        }

        public Criteria andMaxBuyCntIn(List<Integer> values) {
            addCriterion("max_buy_cnt in", values, "maxBuyCnt");
            return (Criteria) this;
        }

        public Criteria andMaxBuyCntNotIn(List<Integer> values) {
            addCriterion("max_buy_cnt not in", values, "maxBuyCnt");
            return (Criteria) this;
        }

        public Criteria andMaxBuyCntBetween(Integer value1, Integer value2) {
            addCriterion("max_buy_cnt between", value1, value2, "maxBuyCnt");
            return (Criteria) this;
        }

        public Criteria andMaxBuyCntNotBetween(Integer value1, Integer value2) {
            addCriterion("max_buy_cnt not between", value1, value2, "maxBuyCnt");
            return (Criteria) this;
        }

        public Criteria andQuotaModeIsNull() {
            addCriterion("quota_mode is null");
            return (Criteria) this;
        }

        public Criteria andQuotaModeIsNotNull() {
            addCriterion("quota_mode is not null");
            return (Criteria) this;
        }

        public Criteria andQuotaModeEqualTo(Byte value) {
            addCriterion("quota_mode =", value, "quotaMode");
            return (Criteria) this;
        }

        public Criteria andQuotaModeNotEqualTo(Byte value) {
            addCriterion("quota_mode <>", value, "quotaMode");
            return (Criteria) this;
        }

        public Criteria andQuotaModeGreaterThan(Byte value) {
            addCriterion("quota_mode >", value, "quotaMode");
            return (Criteria) this;
        }

        public Criteria andQuotaModeGreaterThanOrEqualTo(Byte value) {
            addCriterion("quota_mode >=", value, "quotaMode");
            return (Criteria) this;
        }

        public Criteria andQuotaModeLessThan(Byte value) {
            addCriterion("quota_mode <", value, "quotaMode");
            return (Criteria) this;
        }

        public Criteria andQuotaModeLessThanOrEqualTo(Byte value) {
            addCriterion("quota_mode <=", value, "quotaMode");
            return (Criteria) this;
        }

        public Criteria andQuotaModeIn(List<Byte> values) {
            addCriterion("quota_mode in", values, "quotaMode");
            return (Criteria) this;
        }

        public Criteria andQuotaModeNotIn(List<Byte> values) {
            addCriterion("quota_mode not in", values, "quotaMode");
            return (Criteria) this;
        }

        public Criteria andQuotaModeBetween(Byte value1, Byte value2) {
            addCriterion("quota_mode between", value1, value2, "quotaMode");
            return (Criteria) this;
        }

        public Criteria andQuotaModeNotBetween(Byte value1, Byte value2) {
            addCriterion("quota_mode not between", value1, value2, "quotaMode");
            return (Criteria) this;
        }

        public Criteria andDeployTypeIsNull() {
            addCriterion("deploy_type is null");
            return (Criteria) this;
        }

        public Criteria andDeployTypeIsNotNull() {
            addCriterion("deploy_type is not null");
            return (Criteria) this;
        }

        public Criteria andDeployTypeEqualTo(Boolean value) {
            addCriterion("deploy_type =", value, "deployType");
            return (Criteria) this;
        }

        public Criteria andDeployTypeNotEqualTo(Boolean value) {
            addCriterion("deploy_type <>", value, "deployType");
            return (Criteria) this;
        }

        public Criteria andDeployTypeGreaterThan(Boolean value) {
            addCriterion("deploy_type >", value, "deployType");
            return (Criteria) this;
        }

        public Criteria andDeployTypeGreaterThanOrEqualTo(Boolean value) {
            addCriterion("deploy_type >=", value, "deployType");
            return (Criteria) this;
        }

        public Criteria andDeployTypeLessThan(Boolean value) {
            addCriterion("deploy_type <", value, "deployType");
            return (Criteria) this;
        }

        public Criteria andDeployTypeLessThanOrEqualTo(Boolean value) {
            addCriterion("deploy_type <=", value, "deployType");
            return (Criteria) this;
        }

        public Criteria andDeployTypeIn(List<Boolean> values) {
            addCriterion("deploy_type in", values, "deployType");
            return (Criteria) this;
        }

        public Criteria andDeployTypeNotIn(List<Boolean> values) {
            addCriterion("deploy_type not in", values, "deployType");
            return (Criteria) this;
        }

        public Criteria andDeployTypeBetween(Boolean value1, Boolean value2) {
            addCriterion("deploy_type between", value1, value2, "deployType");
            return (Criteria) this;
        }

        public Criteria andDeployTypeNotBetween(Boolean value1, Boolean value2) {
            addCriterion("deploy_type not between", value1, value2, "deployType");
            return (Criteria) this;
        }

        public Criteria andDeployUpTimeIsNull() {
            addCriterion("deploy_up_time is null");
            return (Criteria) this;
        }

        public Criteria andDeployUpTimeIsNotNull() {
            addCriterion("deploy_up_time is not null");
            return (Criteria) this;
        }

        public Criteria andDeployUpTimeEqualTo(Date value) {
            addCriterion("deploy_up_time =", value, "deployUpTime");
            return (Criteria) this;
        }

        public Criteria andDeployUpTimeNotEqualTo(Date value) {
            addCriterion("deploy_up_time <>", value, "deployUpTime");
            return (Criteria) this;
        }

        public Criteria andDeployUpTimeGreaterThan(Date value) {
            addCriterion("deploy_up_time >", value, "deployUpTime");
            return (Criteria) this;
        }

        public Criteria andDeployUpTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("deploy_up_time >=", value, "deployUpTime");
            return (Criteria) this;
        }

        public Criteria andDeployUpTimeLessThan(Date value) {
            addCriterion("deploy_up_time <", value, "deployUpTime");
            return (Criteria) this;
        }

        public Criteria andDeployUpTimeLessThanOrEqualTo(Date value) {
            addCriterion("deploy_up_time <=", value, "deployUpTime");
            return (Criteria) this;
        }

        public Criteria andDeployUpTimeIn(List<Date> values) {
            addCriterion("deploy_up_time in", values, "deployUpTime");
            return (Criteria) this;
        }

        public Criteria andDeployUpTimeNotIn(List<Date> values) {
            addCriterion("deploy_up_time not in", values, "deployUpTime");
            return (Criteria) this;
        }

        public Criteria andDeployUpTimeBetween(Date value1, Date value2) {
            addCriterion("deploy_up_time between", value1, value2, "deployUpTime");
            return (Criteria) this;
        }

        public Criteria andDeployUpTimeNotBetween(Date value1, Date value2) {
            addCriterion("deploy_up_time not between", value1, value2, "deployUpTime");
            return (Criteria) this;
        }

        public Criteria andDeployDownTimeIsNull() {
            addCriterion("deploy_down_time is null");
            return (Criteria) this;
        }

        public Criteria andDeployDownTimeIsNotNull() {
            addCriterion("deploy_down_time is not null");
            return (Criteria) this;
        }

        public Criteria andDeployDownTimeEqualTo(Date value) {
            addCriterion("deploy_down_time =", value, "deployDownTime");
            return (Criteria) this;
        }

        public Criteria andDeployDownTimeNotEqualTo(Date value) {
            addCriterion("deploy_down_time <>", value, "deployDownTime");
            return (Criteria) this;
        }

        public Criteria andDeployDownTimeGreaterThan(Date value) {
            addCriterion("deploy_down_time >", value, "deployDownTime");
            return (Criteria) this;
        }

        public Criteria andDeployDownTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("deploy_down_time >=", value, "deployDownTime");
            return (Criteria) this;
        }

        public Criteria andDeployDownTimeLessThan(Date value) {
            addCriterion("deploy_down_time <", value, "deployDownTime");
            return (Criteria) this;
        }

        public Criteria andDeployDownTimeLessThanOrEqualTo(Date value) {
            addCriterion("deploy_down_time <=", value, "deployDownTime");
            return (Criteria) this;
        }

        public Criteria andDeployDownTimeIn(List<Date> values) {
            addCriterion("deploy_down_time in", values, "deployDownTime");
            return (Criteria) this;
        }

        public Criteria andDeployDownTimeNotIn(List<Date> values) {
            addCriterion("deploy_down_time not in", values, "deployDownTime");
            return (Criteria) this;
        }

        public Criteria andDeployDownTimeBetween(Date value1, Date value2) {
            addCriterion("deploy_down_time between", value1, value2, "deployDownTime");
            return (Criteria) this;
        }

        public Criteria andDeployDownTimeNotBetween(Date value1, Date value2) {
            addCriterion("deploy_down_time not between", value1, value2, "deployDownTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Boolean value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Boolean value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Boolean value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Boolean value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Boolean value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Boolean value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Boolean> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Boolean> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Boolean value1, Boolean value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Boolean value1, Boolean value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationIsNull() {
            addCriterion("valid_pay_duration is null");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationIsNotNull() {
            addCriterion("valid_pay_duration is not null");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationEqualTo(Integer value) {
            addCriterion("valid_pay_duration =", value, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationNotEqualTo(Integer value) {
            addCriterion("valid_pay_duration <>", value, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationGreaterThan(Integer value) {
            addCriterion("valid_pay_duration >", value, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationGreaterThanOrEqualTo(Integer value) {
            addCriterion("valid_pay_duration >=", value, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationLessThan(Integer value) {
            addCriterion("valid_pay_duration <", value, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationLessThanOrEqualTo(Integer value) {
            addCriterion("valid_pay_duration <=", value, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationIn(List<Integer> values) {
            addCriterion("valid_pay_duration in", values, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationNotIn(List<Integer> values) {
            addCriterion("valid_pay_duration not in", values, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationBetween(Integer value1, Integer value2) {
            addCriterion("valid_pay_duration between", value1, value2, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andValidPayDurationNotBetween(Integer value1, Integer value2) {
            addCriterion("valid_pay_duration not between", value1, value2, "validPayDuration");
            return (Criteria) this;
        }

        public Criteria andDurationIsNull() {
            addCriterion("duration is null");
            return (Criteria) this;
        }

        public Criteria andDurationIsNotNull() {
            addCriterion("duration is not null");
            return (Criteria) this;
        }

        public Criteria andDurationEqualTo(Integer value) {
            addCriterion("duration =", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotEqualTo(Integer value) {
            addCriterion("duration <>", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationGreaterThan(Integer value) {
            addCriterion("duration >", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationGreaterThanOrEqualTo(Integer value) {
            addCriterion("duration >=", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationLessThan(Integer value) {
            addCriterion("duration <", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationLessThanOrEqualTo(Integer value) {
            addCriterion("duration <=", value, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationIn(List<Integer> values) {
            addCriterion("duration in", values, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotIn(List<Integer> values) {
            addCriterion("duration not in", values, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationBetween(Integer value1, Integer value2) {
            addCriterion("duration between", value1, value2, "duration");
            return (Criteria) this;
        }

        public Criteria andDurationNotBetween(Integer value1, Integer value2) {
            addCriterion("duration not between", value1, value2, "duration");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNull() {
            addCriterion("creater_id is null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNotNull() {
            addCriterion("creater_id is not null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdEqualTo(Long value) {
            addCriterion("creater_id =", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotEqualTo(Long value) {
            addCriterion("creater_id <>", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThan(Long value) {
            addCriterion("creater_id >", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThanOrEqualTo(Long value) {
            addCriterion("creater_id >=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThan(Long value) {
            addCriterion("creater_id <", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThanOrEqualTo(Long value) {
            addCriterion("creater_id <=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIn(List<Long> values) {
            addCriterion("creater_id in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotIn(List<Long> values) {
            addCriterion("creater_id not in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdBetween(Long value1, Long value2) {
            addCriterion("creater_id between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotBetween(Long value1, Long value2) {
            addCriterion("creater_id not between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdaterIdIsNull() {
            addCriterion("updater_id is null");
            return (Criteria) this;
        }

        public Criteria andUpdaterIdIsNotNull() {
            addCriterion("updater_id is not null");
            return (Criteria) this;
        }

        public Criteria andUpdaterIdEqualTo(Long value) {
            addCriterion("updater_id =", value, "updaterId");
            return (Criteria) this;
        }

        public Criteria andUpdaterIdNotEqualTo(Long value) {
            addCriterion("updater_id <>", value, "updaterId");
            return (Criteria) this;
        }

        public Criteria andUpdaterIdGreaterThan(Long value) {
            addCriterion("updater_id >", value, "updaterId");
            return (Criteria) this;
        }

        public Criteria andUpdaterIdGreaterThanOrEqualTo(Long value) {
            addCriterion("updater_id >=", value, "updaterId");
            return (Criteria) this;
        }

        public Criteria andUpdaterIdLessThan(Long value) {
            addCriterion("updater_id <", value, "updaterId");
            return (Criteria) this;
        }

        public Criteria andUpdaterIdLessThanOrEqualTo(Long value) {
            addCriterion("updater_id <=", value, "updaterId");
            return (Criteria) this;
        }

        public Criteria andUpdaterIdIn(List<Long> values) {
            addCriterion("updater_id in", values, "updaterId");
            return (Criteria) this;
        }

        public Criteria andUpdaterIdNotIn(List<Long> values) {
            addCriterion("updater_id not in", values, "updaterId");
            return (Criteria) this;
        }

        public Criteria andUpdaterIdBetween(Long value1, Long value2) {
            addCriterion("updater_id between", value1, value2, "updaterId");
            return (Criteria) this;
        }

        public Criteria andUpdaterIdNotBetween(Long value1, Long value2) {
            addCriterion("updater_id not between", value1, value2, "updaterId");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNull() {
            addCriterion("is_delete is null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIsNotNull() {
            addCriterion("is_delete is not null");
            return (Criteria) this;
        }

        public Criteria andIsDeleteEqualTo(Boolean value) {
            addCriterion("is_delete =", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotEqualTo(Boolean value) {
            addCriterion("is_delete <>", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThan(Boolean value) {
            addCriterion("is_delete >", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_delete >=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThan(Boolean value) {
            addCriterion("is_delete <", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteLessThanOrEqualTo(Boolean value) {
            addCriterion("is_delete <=", value, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteIn(List<Boolean> values) {
            addCriterion("is_delete in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotIn(List<Boolean> values) {
            addCriterion("is_delete not in", values, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteBetween(Boolean value1, Boolean value2) {
            addCriterion("is_delete between", value1, value2, "isDelete");
            return (Criteria) this;
        }

        public Criteria andIsDeleteNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_delete not between", value1, value2, "isDelete");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}