package com.liby.promotion.material.service.dao.mybatisPlus;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liby.promotion.material.service.entity.MaterialConfiguration;

/**
 * @param
 * @author lcd
 * @description
 * @date
 * @return
 **/
public interface MaterialConfigMapper extends BaseMapper<MaterialConfiguration> {
}
