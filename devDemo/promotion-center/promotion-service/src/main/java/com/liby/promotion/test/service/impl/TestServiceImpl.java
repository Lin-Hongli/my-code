package com.liby.promotion.test.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.liby.promotion.test.service.dao.mybatisPlus.TestMapper;
import com.liby.promotion.test.service.entity.Test;
import com.liby.promotion.test.service.ITestService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 此层针对单表所有的逻辑处理
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements ITestService {

    @Override
    public List<Map<String, Object>> all() {
        return baseMapper.all();
    }
}
