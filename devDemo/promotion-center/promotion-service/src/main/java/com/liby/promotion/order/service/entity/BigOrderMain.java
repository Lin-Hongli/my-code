package com.liby.promotion.order.service.entity;

import java.math.BigDecimal;
import java.util.Date;

public class BigOrderMain {
    private String orderId;

    private Byte orderType;

    private String promId;

    private Long sessionId;

    private String promTitle;

    private String promDesc;

    private Byte orderStatus;

    private Integer validPayDuration;

    private BigDecimal totalAmount;

    private String customerCode;

    private String customerName;

    private String cancelReason;

    private String cancelDesc;

    private Date cancelTime;

    private Date payTime;

    private Byte orderSyncSucc;

    private String syncFailReason;

    private Date createTime;

    private Date updateTime;

    private String createUser;

    private String sessionName;

    private Date requestTime;

    private String provincialCode;

    private Byte isCanMaterialReplace;

    private byte[] isSmsSend;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public Byte getOrderType() {
        return orderType;
    }

    public void setOrderType(Byte orderType) {
        this.orderType = orderType;
    }

    public String getPromId() {
        return promId;
    }

    public void setPromId(String promId) {
        this.promId = promId == null ? null : promId.trim();
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public String getPromTitle() {
        return promTitle;
    }

    public void setPromTitle(String promTitle) {
        this.promTitle = promTitle == null ? null : promTitle.trim();
    }

    public String getPromDesc() {
        return promDesc;
    }

    public void setPromDesc(String promDesc) {
        this.promDesc = promDesc == null ? null : promDesc.trim();
    }

    public Byte getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Byte orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getValidPayDuration() {
        return validPayDuration;
    }

    public void setValidPayDuration(Integer validPayDuration) {
        this.validPayDuration = validPayDuration;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode == null ? null : customerCode.trim();
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName == null ? null : customerName.trim();
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason == null ? null : cancelReason.trim();
    }

    public String getCancelDesc() {
        return cancelDesc;
    }

    public void setCancelDesc(String cancelDesc) {
        this.cancelDesc = cancelDesc == null ? null : cancelDesc.trim();
    }

    public Date getCancelTime() {
        return cancelTime;
    }

    public void setCancelTime(Date cancelTime) {
        this.cancelTime = cancelTime;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Byte getOrderSyncSucc() {
        return orderSyncSucc;
    }

    public void setOrderSyncSucc(Byte orderSyncSucc) {
        this.orderSyncSucc = orderSyncSucc;
    }

    public String getSyncFailReason() {
        return syncFailReason;
    }

    public void setSyncFailReason(String syncFailReason) {
        this.syncFailReason = syncFailReason == null ? null : syncFailReason.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName == null ? null : sessionName.trim();
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public String getProvincialCode() {
        return provincialCode;
    }

    public void setProvincialCode(String provincialCode) {
        this.provincialCode = provincialCode == null ? null : provincialCode.trim();
    }

    public Byte getIsCanMaterialReplace() {
        return isCanMaterialReplace;
    }

    public void setIsCanMaterialReplace(Byte isCanMaterialReplace) {
        this.isCanMaterialReplace = isCanMaterialReplace;
    }

    public byte[] getIsSmsSend() {
        return isSmsSend;
    }

    public void setIsSmsSend(byte[] isSmsSend) {
        this.isSmsSend = isSmsSend;
    }
}