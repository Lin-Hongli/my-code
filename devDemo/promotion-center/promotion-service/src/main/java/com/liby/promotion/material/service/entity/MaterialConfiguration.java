package com.liby.promotion.material.service.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("big_material_config")
public class MaterialConfiguration {

    @TableField("id")
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 头图url
     */
    @TableField("head_img_url")
    private String headImgUrl;

    /**
     * 中部图url
     */
    @TableField("middle_img_url")
    private String middleImgUrl;

    /**
     * 背景图url
     */
    @TableField("back_img_url")
    private String backImgUrl;

    /**
     * 脚图url
     */
    @TableField("foot_img_url")
    private String footImgUrl;

    /**
     * 类型 1秒杀 2众筹 3大礼包
     */
    @TableField("type")
    private Integer type;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;


}