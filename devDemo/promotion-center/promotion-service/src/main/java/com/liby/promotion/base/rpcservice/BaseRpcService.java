package com.liby.promotion.base.rpcservice;

import com.liby.promotion.base.service.IService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * BaseRpcService 基础服务 dubbo
 *
 * @author lgj liugongjun@chinasie.com
 * @version 0.0.1
 * @date 2018年11月29日
 */
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class BaseRpcService<M extends IService> {
    public M getService() {
        return service;
    }

    public void setService(M service) {
        this.service = service;
    }

    @Autowired
    protected M service;
}
