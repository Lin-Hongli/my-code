package com.liby.promotion.gift.service.dao.mybatisPlus;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liby.promotion.gift.service.entity.BigCustomerDistribution;
import com.liby.promotion.gift.service.entity.BigPromGradeQuota;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 客户分配量 Mapper 接口
 * </p>
 *
 * @author weiyunhui
 * @since 2020-12-02
 */
public interface BigCustomerDistributionDao extends BaseMapper<BigCustomerDistribution> {
    List<BigCustomerDistribution> selectCustomerByPromID(@Param("id") int id);

}
