package com.liby.promotion.gift.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.liby.promotion.gift.service.BigPromotionInfoService;
import com.liby.promotion.gift.service.dao.mybatisPlus.*;
import com.liby.promotion.gift.service.entity.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 大礼包活动表 服务实现类
 * </p>
 *
 * @author weiyunhui
 * @since 2020-11-19
 */
@Transactional
@Service
public class BigPromotionInfoServiceImpl extends ServiceImpl<BigPromotionInfoDao, BigPromotionInfo> implements BigPromotionInfoService {
    @Resource
    private BigPromotionInfoDao bigPromotionInfoDao;
    @Resource
    private BigGoodsDao bigGoodsDao;


    @Resource
    private BigCustomerDistributionDao customerDistributionDao;

    @Resource
    private BigPromGradeQuotaDao promGradeQuotaDao;

    @Resource
    private BigProvincialDistributionDao provincialDistributionDao;

    @Override
    public int saveGift(BigPromotionInfo bigPromotionInfo) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //获取赠品列表
        List<BigGoods> bigGoodsList = bigPromotionInfo.getBigGoodsList();
        //获取客户分配量列表
        List<BigCustomerDistribution> customerDistributions = bigPromotionInfo.getCustomerDistributions();
        //获取星级分配量列表
        List<BigPromGradeQuota> promGradeQuotas = bigPromotionInfo.getPromGradeQuotas();
        //获取省区分配量列表
        List<BigProvincialDistribution> provincialDistributions = bigPromotionInfo.getProvincialDistributions();
        int quotaMode = bigPromotionInfo.getQuotaMode();
        if (bigPromotionInfo.getId() != null) {//修改
            Long promId = bigPromotionInfo.getId();
            //旧的大礼包数据
            BigPromotionInfo bigPromotionInfos = bigPromotionInfoDao.queryById(bigPromotionInfo.getId());
            //如果旧的数据为定时上下价，并且更改了价格且是新增的，并且在有效时间段类，并且类型为依据售完，则更改活动状态为秒杀中
            if (bigPromotionInfos.getDeployType() == 1 && bigPromotionInfos.getStatus() == 2) {
                Long thisDate = new Date().getTime();
                Long beginTime = bigPromotionInfos.getDeployUpTime().getTime();
                Long endTime = bigPromotionInfos.getDeployDownTime().getTime();

                if (bigPromotionInfo.getStock() > bigPromotionInfos.getStock() &&
                        thisDate > beginTime && endTime > thisDate) {
                    bigPromotionInfo.setStatus(1);
                }
            }


            //获取当前id下未修改之前的商品数据列表
            List<BigGoods> bigGoods = bigPromotionInfos.getBigGoodsList();

            //删除旧配额数据begin
            if (bigPromotionInfos.getQuotaMode() == 2) {//如果是星级配额,则删除全部旧数据
                List<BigPromGradeQuota> bigPromGradeQuotas = promGradeQuotaDao.selectPromGradeByPromID(bigPromotionInfos.getId().intValue());
                for (BigPromGradeQuota obj :
                        bigPromGradeQuotas) {
                    promGradeQuotaDao.deleteById(obj.getId());
                }
            } else if (bigPromotionInfos.getQuotaMode() == 3) {//如果旧的是客户配额,则删除全部旧数据
                List<BigCustomerDistribution> bigCustomerDistributions = customerDistributionDao.selectCustomerByPromID(bigPromotionInfos.getId().intValue());
                if (bigCustomerDistributions != null && !bigCustomerDistributions.isEmpty()) {
                    for (BigCustomerDistribution obj :
                            bigCustomerDistributions) {
                        obj.setDeleteStatus(1);
                        customerDistributionDao.updateById(obj);
                    }
                }
            }

            //删除省区配额
            List<BigProvincialDistribution> bigCustomerDistributions = provincialDistributionDao.selectProvincialByPromID(bigPromotionInfos.getId().intValue());
            if (bigCustomerDistributions != null && !bigCustomerDistributions.isEmpty()) {
                for (BigProvincialDistribution obj :
                        bigCustomerDistributions) {
                    obj.setDeleteStatus(1);
                    provincialDistributionDao.updateById(obj);
                }
            }
            //删除旧配额数据end

            bigPromotionInfo.setUpdateTime(simpleDateFormat.format(new Date()));
            bigPromotionInfoDao.updateById(bigPromotionInfo);
            //循环旧的商品列表进行逻辑删除
            for (BigGoods bigGoodsFor :
                    bigGoods) {
                bigGoodsFor.setIsDelete(true);
                bigGoodsFor.setUpdateTime(simpleDateFormat.format(new Date()));

                bigGoodsDao.updateById(bigGoodsFor);
            }
            //删除完再进行新增
            for (BigGoods bigGoodsFor :
                    bigGoodsList) {
                //将活动实体的创建人id插入商品实体内
                bigGoodsFor.setCreaterId(bigPromotionInfo.getUpdaterId());
                bigGoodsFor.setUpdaterId(bigPromotionInfo.getUpdaterId());
                bigGoodsFor.setCreateTime(simpleDateFormat.format(new Date()));
                bigGoodsFor.setPromId(promId);

                bigGoodsDao.insert(bigGoodsFor);
            }


            //新增配额数据begin
            if (quotaMode == 2) {//如果是星级
                //新增星级分配量
                if (promGradeQuotas != null && !promGradeQuotas.isEmpty()) {
                    for (BigPromGradeQuota obj :
                            promGradeQuotas) {
                        obj.setCreateTime(simpleDateFormat.format(new Date()));
                        obj.setPromId(promId.toString());
                        promGradeQuotaDao.insert(obj);
                    }
                }
            } else if (quotaMode == 3) {//如果是省区
                //新增客户分配量
                if (customerDistributions != null && !customerDistributions.isEmpty()) {
                    for (BigCustomerDistribution obj :
                            customerDistributions) {
                        obj.setAddTime(simpleDateFormat.format(new Date()));
                        obj.setPromId(promId.toString());
                        customerDistributionDao.insert(obj);
                    }
                }
            }

            //新增省区分配量
            if (provincialDistributions != null && !provincialDistributions.isEmpty()) {
                for (BigProvincialDistribution obj :
                        provincialDistributions) {
                    obj.setAddTime(simpleDateFormat.format(new Date()));
                    obj.setPromId(promId.toString());
                    provincialDistributionDao.insert(obj);
                }
            }
            //新增配额数据end
        } else {//新增
            //状态默认为未开始
            bigPromotionInfo.setStatus(0);
            bigPromotionInfo.setCreateTime(simpleDateFormat.format(new Date()));
            //邢增商品
            bigPromotionInfoDao.insert(bigPromotionInfo);
            //获取自增识的ID
            Long promId = bigPromotionInfo.getId();
            //新增配额数据begin
            if (quotaMode == 2) {//如果是星级
                //新增星级分配量
                if (promGradeQuotas != null && !promGradeQuotas.isEmpty()) {
                    for (BigPromGradeQuota obj :
                            promGradeQuotas) {
                        obj.setCreateTime(simpleDateFormat.format(new Date()));
                        obj.setPromId(promId.toString());
                        promGradeQuotaDao.insert(obj);
                    }
                }
            } else if (quotaMode == 3) {//如果是省区
                //新增客户分配量
                if (customerDistributions != null && !customerDistributions.isEmpty()) {
                    for (BigCustomerDistribution obj :
                            customerDistributions) {
                        obj.setAddTime(simpleDateFormat.format(new Date()));
                        obj.setPromId(promId.toString());
                        customerDistributionDao.insert(obj);
                    }
                }
            }

            //新增省区分配量
            if (provincialDistributions != null && !provincialDistributions.isEmpty()) {
                for (BigProvincialDistribution obj :
                        provincialDistributions) {
                    obj.setAddTime(simpleDateFormat.format(new Date()));
                    obj.setPromId(promId.toString());
                    provincialDistributionDao.insert(obj);
                }
            }

            //新增商品赠品
            for (BigGoods bigGoodsFor :
                    bigGoodsList) {
                //将活动实体的创建人id插入商品实体内
                bigGoodsFor.setCreaterId(bigPromotionInfo.getUpdaterId());
                bigGoodsFor.setCreateTime(simpleDateFormat.format(new Date()));
                bigGoodsFor.setPromId(promId);
                System.out.println(bigGoodsFor);
                bigGoodsDao.insert(bigGoodsFor);
            }
        }
        return 1;
    }

    @Override
    public BigPromotionInfo queryById(Long id) {
        try {
            BigPromotionInfo bigPromotionInfo = bigPromotionInfoDao.queryById(id);
            return bigPromotionInfo;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int updateStatusById(Long id, int status) {
        bigPromotionInfoDao.updateStatusById(id, status);

        return 1;
    }

    @Override
    public List<BigPromotionInfo> selectAll(int pageNp, int pageSize, BigPromotionInfo bigPromotionInfo) {
        try {
            PageHelper.startPage(pageNp, pageSize);
            List<BigPromotionInfo> bigPromotionInfos = bigPromotionInfoDao.selectAll(bigPromotionInfo);
            PageInfo<BigPromotionInfo> pageInfo = new PageInfo(bigPromotionInfos);
            return pageInfo.getList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }


}
