package com.liby.promotion.gift.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liby.promotion.gift.service.entity.BigProvincialDistribution;

import java.util.List;

/**
 * <p>
 * 省区分配量 服务类
 * </p>
 *
 * @author weiyunhui
 * @since 2020-12-02
 */
public interface BigProvincialDistributionService extends IService<BigProvincialDistribution> {
    int insertProvincialAll(List<BigProvincialDistribution> bigProvincialDistributions);
}
