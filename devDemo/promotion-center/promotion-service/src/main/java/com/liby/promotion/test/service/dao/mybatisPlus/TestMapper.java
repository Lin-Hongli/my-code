package com.liby.promotion.test.service.dao.mybatisPlus;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liby.promotion.test.service.entity.Test;

import java.util.List;
import java.util.Map;

public interface TestMapper extends BaseMapper<Test> {

    List<Map<String, Object>> all();
}
