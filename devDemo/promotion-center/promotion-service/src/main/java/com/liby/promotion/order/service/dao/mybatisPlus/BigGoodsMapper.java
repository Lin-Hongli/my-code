package com.liby.promotion.order.service.dao.mybatisPlus;

import com.liby.promotion.order.service.entity.BigGoods;
import com.liby.promotion.order.service.entity.BigGoodsExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BigGoodsMapper {
    int countByExample(BigGoodsExample example);

    int deleteByExample(BigGoodsExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BigGoods record);

    int insertSelective(BigGoods record);

    List<BigGoods> selectByExample(BigGoodsExample example);

    BigGoods selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BigGoods record, @Param("example") BigGoodsExample example);

    int updateByExample(@Param("record") BigGoods record, @Param("example") BigGoodsExample example);

    int updateByPrimaryKeySelective(BigGoods record);

    int updateByPrimaryKey(BigGoods record);
}