package com.liby.promotion.base.config;

import com.alibaba.dubbo.config.*;
import com.liby.common.logging.Log;
import com.liby.promotion.base.rpcservice.AuthBaseRpcService;
import com.liby.promotion.base.rpcservice.BaseRpcService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * DubboProtocolConfig dubbo 协议配置
 *
 * @author lgj liugongjun@chinasie.com
 * @version 0.0.1
 * @date 2018年11月29日
 */
@Configuration
@ConditionalOnProperty(
        prefix = "dubbo",
        name = {"enabled"},
        matchIfMissing = true,
        havingValue = "true"
)
@AutoConfigureAfter(ApplicationConfig.class)
public class DubboProtocolConfig implements InitializingBean {
    private static final Log log = Log.get();
    //rest 端口
    @Value("${liby.dubbo.rest.port:20880}")
    private int restPort;
    //dubbo端口
    @Value("${liby.dubbo.dubbo.port:20881}")
    private int dubboPort;
    @Value("${liby.dubbo.rest.context.path:api}")
    private String restContextpath;
    @Value("${provider.service.version:1.0.0}")
    private String serviceVersion;
    
    @Value("${dubbo.timeout:1000}")
    private int timeOut;

    //发布应用配置
    @Autowired
    private ApplicationConfig application;

    //发布注册配置
    @Autowired
    private RegistryConfig registry;

    private ObjectProvider<List<BaseRpcService>> services;

    public DubboProtocolConfig(ObjectProvider<List<BaseRpcService>> services) {
        this.services = services;
    }

    @Bean(name = "dubboProvider")
    public ProviderConfig providerConfig() {
        ProviderConfig providerConfig = new ProviderConfig();
        providerConfig.setTimeout(timeOut);
        providerConfig.setRetries(3);
        providerConfig.setDelay(-1);
        providerConfig.setApplication(application);
        providerConfig.setRegistry(registry);
        providerConfig.setProtocol(getDubbo());
        providerConfig.setVersion(serviceVersion);
        providerConfig.setFilter("localeFilter,traceFilter");
        return providerConfig;
    }

    @Bean(name = "rest")
    public ProtocolConfig getRest() {
        ProtocolConfig config = new ProtocolConfig("rest", restPort);
        config.setServer("tomcat");
        config.setContextpath(restContextpath);
        return config;
    }

    public ProtocolConfig getDubbo() {
        ProtocolConfig config = new ProtocolConfig("dubbo", dubboPort);
        return config;
    }

    protected void initService(List<BaseRpcService> lists, List<ProtocolConfig> protocols) {
        if (lists != null && lists.size() > 0) {
            for (BaseRpcService service : lists) {
                Class cls = service.getClass();
                ServiceConfig<BaseRpcService> sc = new ServiceConfig<>(); // 此实例很重，封装了与注册中心的连接，请自行缓存，否则可能造成内存和连接泄漏
                String name = service.toString();
                if (name.contains("@")) {
                    name = name.split("@")[0];
                    if (!cls.getName().equals(name)) {
                        try {
                            cls = Class.forName(name);
                        } catch (ClassNotFoundException e) {
                            log.error(e);
                        }
                    }
                }
                sc.setApplication(application);
                sc.setTimeout(timeOut);
                sc.setRegistry(registry); // 多个注册中心可以用setRegistries()
                sc.setProtocols(protocols); // 多个协议可以用setProtocols()
                sc.setInterface(cls.getInterfaces()[0]);
                sc.setRef(service);
                sc.setVersion(serviceVersion);
                if (service instanceof AuthBaseRpcService) {
                    sc.setFilter("localeFilter,traceFilter,tokenFilter");
                } else {
                    sc.setFilter("localeFilter,traceFilter");
                }

                // 暴露及注册服务
                sc.export();
            }
        }
    }

    @Bean(name = "consumerConfig")
    public ConsumerConfig consumerConfig() {
        ConsumerConfig consumerConfig = new ConsumerConfig();
        consumerConfig.setVersion(serviceVersion);
        consumerConfig.setApplication(application);
        consumerConfig.setRegistry(registry);
        consumerConfig.setFilter("cousumerFilter");
        return consumerConfig;
    }

    @Override
    public void afterPropertiesSet() {
        List<BaseRpcService> lists = services.getIfAvailable(Collections::emptyList);
        List<ProtocolConfig> protocols = new ArrayList<>(2);
        protocols.add(getDubbo());
        //protocols.add(getRest());

        initService(lists, protocols);
    }
}
