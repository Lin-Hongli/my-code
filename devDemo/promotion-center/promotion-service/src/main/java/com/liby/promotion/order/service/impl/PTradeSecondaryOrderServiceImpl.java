package com.liby.promotion.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.liby.promotion.order.export.dto.QureyOrderRequest;
import com.liby.promotion.order.export.dto.QureyOrderResponse;
import com.liby.promotion.order.service.PTradeSecondaryOrderService;
import com.liby.promotion.order.service.dao.mybatisPlus.BigOrderMainMapper;
import com.liby.promotion.order.service.entity.BigOrderMain;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PTradeSecondaryOrderServiceImpl extends ServiceImpl<BigOrderMainMapper, BigOrderMain> implements PTradeSecondaryOrderService {

    private String perCent = "%";

    @Override
    public PageInfo<QureyOrderResponse> qureyGiftPacksOrder(QureyOrderRequest qureyOrderRequest) {
        operationString(qureyOrderRequest);
        /*qureyOrderRequest.setPageIndex(qureyOrderRequest.getPageIndex()-1<0?0:qureyOrderRequest.getPageIndex()-1);
        PageHelper.startPage(qureyOrderRequest.getPageIndex(),qureyOrderRequest.getPageSize());*/
        PageHelper.startPage(0,10);
        PageInfo<QureyOrderResponse> pageInfo = new PageInfo<>(baseMapper.selectGiftPacks(qureyOrderRequest));
        return pageInfo;
    }

    @Override
    public List<QureyOrderResponse> exportGiftPacksOrder(QureyOrderRequest qureyOrderRequest) {
        operationString(qureyOrderRequest);
        List<QureyOrderResponse> qureyOrderResponses = baseMapper.exportGiftPacksOrder(qureyOrderRequest);
        return qureyOrderResponses;
    }

    /**
     * 操作字符串
     * @param qureyOrderRequest
     * @return
     */
    private void operationString(QureyOrderRequest qureyOrderRequest){
        if (verifyStatus(qureyOrderRequest.getOrderStatusList())){
            qureyOrderRequest.setOrderStatusList(null);
        }
        if (verifyStatus(qureyOrderRequest.getOrderSyncSucc())){
            qureyOrderRequest.setOrderSyncSucc(null);
        }
        if (qureyOrderRequest.getOrderStr()!=null && !qureyOrderRequest.equals("")){
            qureyOrderRequest.setOrderList(splitString(qureyOrderRequest.getOrderStr()));
        }
        if (qureyOrderRequest.getItemIdStr()!=null && !qureyOrderRequest.getItemIdStr().equals("")){
            qureyOrderRequest.setItemIdList(splitString(qureyOrderRequest.getItemIdStr()));
        }
        if (qureyOrderRequest.getCustomerCodeStr()!=null && !qureyOrderRequest.getCustomerCodeStr().equals("")){
            qureyOrderRequest.setCustomerCodeList(splitString(qureyOrderRequest.getCustomerCodeStr()));
        }
        if (qureyOrderRequest.getGiftItemIdStr()!=null && !qureyOrderRequest.getGiftItemIdStr().equals("")){
            qureyOrderRequest.setGiftItemIdList(splitString(qureyOrderRequest.getGiftItemIdStr()));
        }
    }

    /**
     * 切割字符串
     * @param str
     * @return
     */
    private List<String> splitString(String str){
        List<String> stringList = new ArrayList<>();
        if (str!=null && str.length()>0) {
            String[] split = str.split(",");
            for (String s : split) {
//                stringList.add((perCent + s + perCent));
                stringList.add(s);
            }
        }
        return stringList;
    }

    /**
     * 校验状态
     * @param bytes
     */
    private boolean verifyStatus(List<Byte> bytes){
        if (bytes!=null && bytes.size()>0) {
            for (Byte aByte : bytes) {
                if (aByte == -1) {
                    return true;
                }
            }
        }
        return false;
    }

}
