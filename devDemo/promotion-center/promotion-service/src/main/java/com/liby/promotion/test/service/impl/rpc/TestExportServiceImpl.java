package com.liby.promotion.test.service.impl.rpc;

import com.alibaba.dubbo.config.annotation.Service;
import com.liby.promotion.base.rpcservice.impl.BaseExportServiceImpl;
import com.liby.promotion.common.redis.service.RedisUtil;
import com.liby.promotion.test.export.dto.TestDTO;
import com.liby.promotion.test.export.service.ITestExportService;
import com.liby.promotion.test.service.ITestService;
import com.liby.promotion.test.service.entity.Test;

import javax.annotation.Resource;

/**
 * 此层一般只负责调用对应的底层表service
 * 只有多表操作时，才做调用的逻辑处理
 */
@Service(provider = "dubboProvider")
public class TestExportServiceImpl extends BaseExportServiceImpl<TestDTO, Test, ITestService> implements ITestExportService {

    @Override
    @Resource(name="redisUtil2")
    public void setRedisUtil(RedisUtil redisUtil) {
        super.setRedisUtil(redisUtil);
    }
}
