package com.liby.promotion.base.filter;

import com.alibaba.dubbo.rpc.*;
import com.liby.common.data.ContextHelper;
import com.liby.common.data.LogData;
import com.liby.common.logging.Log;
import org.apache.catalina.connector.RequestFacade;
import org.slf4j.MDC;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * DubboTraceFilter 日志处理
 *
 * @author lgj liugongjun@chinasie.com
 * @version 0.0.1
 * @date 2018年11月29日
 */
public class DubboTraceFilter implements Filter {
    private static final Log log = Log.get();

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        RpcContext rpcContext = RpcContext.getContext();
        RequestFacade request = (RequestFacade) rpcContext.getRequest();
        //1 记录参数
        LogData logData = ContextHelper.initLogData();
        logData.setReqType(LogData.ReqType.DUBBO);
        String traceId, preLogId;
        if (request == null) {
            traceId = invocation.getAttachment("traceId");
            preLogId = invocation.getAttachment("logId");
        } else {
            traceId = request.getParameter("traceId");
            preLogId = request.getParameter("logId");
        }
        if (StringUtils.isEmpty(traceId)) {
            traceId = logData.getTraceId();
        }
        logData.setTraceId(traceId);
        logData.setParentLogId(preLogId);
        logData.setClientIp(rpcContext.getRemoteAddressString());
        logData.setLogMethod(invocation.getMethodName());
        logData.setLogMethodParams(invocation.getArguments());
        logData.setReqUrl(rpcContext.getUrl().toServiceString());
        MDC.put("logId", logData.getLogId());
        MDC.put("traceId", traceId);
        Result result;
        logData.setStartDate(new Date());

        try {
            log.info("dubbo start:", logData);

            result = invoker.invoke(invocation);
            logData.setLogMethodParams(result.getValue());
        } finally {
            logData.setLogMethodParamsType(LogData.MethodParamsType.OUT);
            logData.setEndDate(new Date());
            log.info("dubbo end:", logData);
        }

        return result;
    }
}
