package com.liby.promotion.material.service.impl.rpc;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.liby.promotion.material.export.dto.MaterialConfigurationDto;
import com.liby.promotion.material.export.service.MaterialConfigurationRpcService;
import com.liby.promotion.material.service.MaterialConfigurationService;
import com.liby.promotion.material.service.entity.MaterialConfiguration;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @param
 * @author lcd
 * @description
 * @date
 * @return
 **/
@Service(provider = "dubboProvider")
public class MaterialConfigurationRpcServiceImpl implements MaterialConfigurationRpcService {

    @Autowired
    private MaterialConfigurationService materialConfigurationService;

    @Override
    public MaterialConfigurationDto get(Integer type) {
        MaterialConfigurationDto dto = new MaterialConfigurationDto();
        try {
            QueryWrapper<MaterialConfiguration> queryWrapper = new QueryWrapper();
            if (type != null) {
                queryWrapper.eq("type", type);
            }
            MaterialConfiguration configuration = materialConfigurationService.getOne(queryWrapper);
            BeanUtils.copyProperties(configuration,dto);
        }catch (Exception e){
            e.printStackTrace();
        }
        return dto;
    }

    @Override
    public void saveOrUpdateConfig(MaterialConfigurationDto dto) {
        MaterialConfiguration materialConfiguration = new MaterialConfiguration();
        BeanUtils.copyProperties(dto,materialConfiguration);
        if (dto.getId()==null){
            materialConfiguration.setCreateTime(new Date());
        }
        materialConfiguration.setUpdateTime(new Date());
        materialConfigurationService.saveOrUpdate(materialConfiguration);
    }
}
