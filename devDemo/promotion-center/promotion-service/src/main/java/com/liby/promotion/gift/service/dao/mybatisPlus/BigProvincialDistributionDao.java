package com.liby.promotion.gift.service.dao.mybatisPlus;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.liby.promotion.gift.service.entity.BigProvincialDistribution;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 省区分配量 Mapper 接口
 * </p>
 *
 * @author weiyunhui
 * @since 2020-12-02
 */
public interface BigProvincialDistributionDao extends BaseMapper<BigProvincialDistribution> {
    List<BigProvincialDistribution> selectProvincialByPromID(@Param("id") int id);
}
