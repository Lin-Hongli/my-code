package com.liby.promotion.order.service.dao.mybatisPlus;

import com.liby.promotion.order.service.entity.BigPromotionInfo;
import com.liby.promotion.order.service.entity.BigPromotionInfoExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BigPromotionInfoMapper {
    int countByExample(BigPromotionInfoExample example);

    int deleteByExample(BigPromotionInfoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(BigPromotionInfo record);

    int insertSelective(BigPromotionInfo record);

    List<BigPromotionInfo> selectByExampleWithBLOBs(BigPromotionInfoExample example);

    List<BigPromotionInfo> selectByExample(BigPromotionInfoExample example);

    BigPromotionInfo selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") BigPromotionInfo record,
                                 @Param("example") BigPromotionInfoExample example);

    int updateByExampleWithBLOBs(@Param("record") BigPromotionInfo record, @Param("example") BigPromotionInfoExample example);

    int updateByExample(@Param("record") BigPromotionInfo record, @Param("example") BigPromotionInfoExample example);

    int updateByPrimaryKeySelective(BigPromotionInfo record);

    int updateByPrimaryKeyWithBLOBs(BigPromotionInfo record);

    int updateByPrimaryKey(BigPromotionInfo record);
}