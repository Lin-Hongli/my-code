package com.liby.promotion.order.service.entity;

import java.math.BigDecimal;
import java.util.Date;

public class BigPromotionInfo {
    private Long id;

    private String code;

    private Boolean type;

    private Boolean gifType;

    private String name;

    private String imgUrl;

    private BigDecimal totalPrice;

    private Integer stock;

    private Boolean isBuyGif;

    private Boolean isCanMaterialReplace;

    private Integer maxBuyCnt;

    private Byte quotaMode;

    private Boolean deployType;

    private Date deployUpTime;

    private Date deployDownTime;

    private Boolean status;

    private Integer validPayDuration;

    private Integer duration;

    private Date createTime;

    private Long createrId;

    private Date updateTime;

    private Long updaterId;

    private Boolean isDelete;

    private String desc;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public Boolean getType() {
        return type;
    }

    public void setType(Boolean type) {
        this.type = type;
    }

    public Boolean getGifType() {
        return gifType;
    }

    public void setGifType(Boolean gifType) {
        this.gifType = gifType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl == null ? null : imgUrl.trim();
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Boolean getIsBuyGif() {
        return isBuyGif;
    }

    public void setIsBuyGif(Boolean isBuyGif) {
        this.isBuyGif = isBuyGif;
    }

    public Boolean getIsCanMaterialReplace() {
        return isCanMaterialReplace;
    }

    public void setIsCanMaterialReplace(Boolean isCanMaterialReplace) {
        this.isCanMaterialReplace = isCanMaterialReplace;
    }

    public Integer getMaxBuyCnt() {
        return maxBuyCnt;
    }

    public void setMaxBuyCnt(Integer maxBuyCnt) {
        this.maxBuyCnt = maxBuyCnt;
    }

    public Byte getQuotaMode() {
        return quotaMode;
    }

    public void setQuotaMode(Byte quotaMode) {
        this.quotaMode = quotaMode;
    }

    public Boolean getDeployType() {
        return deployType;
    }

    public void setDeployType(Boolean deployType) {
        this.deployType = deployType;
    }

    public Date getDeployUpTime() {
        return deployUpTime;
    }

    public void setDeployUpTime(Date deployUpTime) {
        this.deployUpTime = deployUpTime;
    }

    public Date getDeployDownTime() {
        return deployDownTime;
    }

    public void setDeployDownTime(Date deployDownTime) {
        this.deployDownTime = deployDownTime;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getValidPayDuration() {
        return validPayDuration;
    }

    public void setValidPayDuration(Integer validPayDuration) {
        this.validPayDuration = validPayDuration;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreaterId() {
        return createrId;
    }

    public void setCreaterId(Long createrId) {
        this.createrId = createrId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUpdaterId() {
        return updaterId;
    }

    public void setUpdaterId(Long updaterId) {
        this.updaterId = updaterId;
    }

    public Boolean getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc == null ? null : desc.trim();
    }
}