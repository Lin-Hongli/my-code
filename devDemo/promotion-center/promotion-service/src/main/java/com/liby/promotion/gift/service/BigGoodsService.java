package com.liby.promotion.gift.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liby.promotion.gift.service.entity.BigGoods;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author weiyunhui
 * @since 2020-11-19
 */
public interface BigGoodsService extends IService<BigGoods> {

}
