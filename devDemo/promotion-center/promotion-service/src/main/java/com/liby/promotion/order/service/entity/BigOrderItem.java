package com.liby.promotion.order.service.entity;

import java.math.BigDecimal;
import java.util.Date;

public class BigOrderItem extends BigOrderItemKey {
    private String itemId;

    private String itemName;

    private BigDecimal itemPrice;

    private String itemImgUrl;

    private BigDecimal promPrice;

    private Integer itemCnt;

    private Date createTime;

    private Date updateTime;

    private String promId;

    private String promTitle;

    private String promDesc;

    private Byte brandType;

    private String giftItemId;

    private String giftItemName;

    private String giftImgUrl;

    private Integer giftCount;

    private String customerCode;

    private Date requestDeliveryDate;

    private String itemUnit;

    private String giftItemUnit;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId == null ? null : itemId.trim();
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName == null ? null : itemName.trim();
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public String getItemImgUrl() {
        return itemImgUrl;
    }

    public void setItemImgUrl(String itemImgUrl) {
        this.itemImgUrl = itemImgUrl == null ? null : itemImgUrl.trim();
    }

    public BigDecimal getPromPrice() {
        return promPrice;
    }

    public void setPromPrice(BigDecimal promPrice) {
        this.promPrice = promPrice;
    }

    public Integer getItemCnt() {
        return itemCnt;
    }

    public void setItemCnt(Integer itemCnt) {
        this.itemCnt = itemCnt;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getPromId() {
        return promId;
    }

    public void setPromId(String promId) {
        this.promId = promId == null ? null : promId.trim();
    }

    public String getPromTitle() {
        return promTitle;
    }

    public void setPromTitle(String promTitle) {
        this.promTitle = promTitle == null ? null : promTitle.trim();
    }

    public String getPromDesc() {
        return promDesc;
    }

    public void setPromDesc(String promDesc) {
        this.promDesc = promDesc == null ? null : promDesc.trim();
    }

    public Byte getBrandType() {
        return brandType;
    }

    public void setBrandType(Byte brandType) {
        this.brandType = brandType;
    }

    public String getGiftItemId() {
        return giftItemId;
    }

    public void setGiftItemId(String giftItemId) {
        this.giftItemId = giftItemId == null ? null : giftItemId.trim();
    }

    public String getGiftItemName() {
        return giftItemName;
    }

    public void setGiftItemName(String giftItemName) {
        this.giftItemName = giftItemName == null ? null : giftItemName.trim();
    }

    public String getGiftImgUrl() {
        return giftImgUrl;
    }

    public void setGiftImgUrl(String giftImgUrl) {
        this.giftImgUrl = giftImgUrl == null ? null : giftImgUrl.trim();
    }

    public Integer getGiftCount() {
        return giftCount;
    }

    public void setGiftCount(Integer giftCount) {
        this.giftCount = giftCount;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode == null ? null : customerCode.trim();
    }

    public Date getRequestDeliveryDate() {
        return requestDeliveryDate;
    }

    public void setRequestDeliveryDate(Date requestDeliveryDate) {
        this.requestDeliveryDate = requestDeliveryDate;
    }

    public String getItemUnit() {
        return itemUnit;
    }

    public void setItemUnit(String itemUnit) {
        this.itemUnit = itemUnit == null ? null : itemUnit.trim();
    }

    public String getGiftItemUnit() {
        return giftItemUnit;
    }

    public void setGiftItemUnit(String giftItemUnit) {
        this.giftItemUnit = giftItemUnit == null ? null : giftItemUnit.trim();
    }
}