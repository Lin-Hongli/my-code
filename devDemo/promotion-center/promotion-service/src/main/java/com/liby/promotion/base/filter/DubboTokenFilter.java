package com.liby.promotion.base.filter;

import com.alibaba.dubbo.rpc.*;
import com.liby.common.data.ContextHelper;
import com.liby.common.exception.LibyException;
import com.liby.common.logging.Log;
import com.liby.promotion.base.exception.BaseExcepitonHelper;
import com.liby.promotion.common.constants.SysConstants;
import org.apache.catalina.connector.RequestFacade;
import org.springframework.util.StringUtils;

/**
 * DubboTokenFilter token认证处理
 *
 * @author lgj liugongjun@chinasie.com
 * @version 0.0.1
 * @date 2018年11月29日
 */
public class DubboTokenFilter implements Filter {
    private static final Log log = Log.get();

    private void init() {

    }

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        RpcContext rpcContext = RpcContext.getContext();
        RequestFacade request = (RequestFacade) rpcContext.getRequest();
        String token = null;
        if (request == null) {
            token = invocation.getAttachment(SysConstants.AUTH_TOKEN);
        } else {
            token = request.getHeader(SysConstants.AUTH_TOKEN);
            if (StringUtils.isEmpty(token)) {
                token = request.getParameter(SysConstants.AUTH_TOKEN);
            }
        }
        if (StringUtils.isEmpty(token)) {
            throw new LibyException(BaseExcepitonHelper.BASE_SERVICE_002, RpcContext.getContext().getRemoteAddressString());
        }
        init();
        //token验证


        ContextHelper.initLogData();

        Result result = invoker.invoke(invocation);

        return result;
    }
}
