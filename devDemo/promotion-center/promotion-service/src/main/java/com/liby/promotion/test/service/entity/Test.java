package com.liby.promotion.test.service.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.liby.promotion.common.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * 不要直接@Data，用什么写什么
 */
@TableName("test")
@Getter
@Setter
public class Test extends BaseEntity {

    @TableField("name")
    private String name;
}
