package com.liby.promotion.gift.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.liby.promotion.gift.service.entity.BigPromGradeQuota;

import java.util.List;

/**
 * <p>
 * 大礼包活动星级配置表 服务类
 * </p>
 *
 * @author weiyunhui
 * @since 2020-12-02
 */
public interface BigPromGradeQuotaService extends IService<BigPromGradeQuota> {
    int insertPromGradeQuotaAll(List<BigPromGradeQuota> bigPromGradeQuotas);
}
