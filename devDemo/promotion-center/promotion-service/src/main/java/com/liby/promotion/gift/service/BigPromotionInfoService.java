package com.liby.promotion.gift.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.liby.promotion.gift.service.entity.BigPromotionInfo;

import java.text.ParseException;
import java.util.List;

/**
 * <p>
 * 大礼包活动表 服务类
 * </p>
 *
 * @author weiyunhui
 * @since 2020-11-19
 */
public interface BigPromotionInfoService extends IService<BigPromotionInfo> {
    /**
     * 大礼包新增
     *
     * @param bigPromotionInfo
     * @return
     */
    int saveGift(BigPromotionInfo bigPromotionInfo) throws ParseException;

    /**
     * 根据id查询大礼包
     * @param id
     * @return
     */
    BigPromotionInfo queryById(Long id);

    /**
     * 修改大礼包上下架状态
     * @param id
     * @param status
     * @return
     */
    int updateStatusById(Long id, int status);

    /**
     * 查询所有大礼包数据
     * @param pageNp
     * @param pageSize
     * @param bigPromotionInfo
     * @return
     */
    List<BigPromotionInfo> selectAll(int pageNp,int pageSize,BigPromotionInfo bigPromotionInfo);
}
