package com.liby.promotion.common.service.api;

import com.liby.promotion.common.result.Result;
import com.liby.promotion.common.util.CallBack;
import com.liby.promotion.common.vo.FileVO;

public interface IBaseApiService {

    Result exportTableData(String tempPath, CallBack<Result> callBack);

    Result download(String tempPath, FileVO fileVO);
}
