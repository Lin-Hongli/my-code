package com.liby.promotion.common.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class FileUtil {

	public static byte[] readFile(File file) {
		InputStream fis = null;
		ByteArrayOutputStream bos = null;
		try {
			fis = new FileInputStream(file);
			bos = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fis.read(buffer)) != -1) {
				bos.write(buffer, 0, n);
			}
			return bos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();			
			return null;
		} finally {
			IOUtil.close(bos);
			IOUtil.close(fis);
		}
	}
}
