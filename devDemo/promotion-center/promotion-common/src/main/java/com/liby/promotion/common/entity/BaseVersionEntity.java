package com.liby.promotion.common.entity;

import com.baomidou.mybatisplus.annotation.Version;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class BaseVersionEntity extends BaseEntity {
    @Version
    private Integer version;
}
