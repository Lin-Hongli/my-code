package com.liby.promotion.common.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.UUID;

public class StringUtil {
	
	public static String getUuid() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	public static String toJSONString(Object o, SerializerFeature... features) {
		if(o instanceof String) {
			return (String) o;
		} else if(o instanceof Object){
			return JSON.toJSONStringWithDateFormat(o, "yyyy-MM-dd HH:mm:ss", features);
		}
		return null;
	}

	public static String upperFirstWord(String str) {
		if(!ValidateUtil.isAvailable(str)) {return null;}
		char firstWord = str.charAt(0);
		if(firstWord == "_".charAt(0)) {return str;}
		return Character.isUpperCase(firstWord) ?
				str :
				new StringBuffer().append(Character.toUpperCase(firstWord)).append(str.substring(1)).toString();
	}
}
