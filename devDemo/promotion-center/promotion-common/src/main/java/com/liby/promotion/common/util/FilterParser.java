package com.liby.promotion.common.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class FilterParser {

	public static String getCondition(String filter) throws Exception{
		if (!ValidateUtil.isAvailable(filter)) {
			return "";
		}
		filter = URLDecoder.decode(filter, "UTF-8");
		JSONArray jsonArray = JSONArray.parseArray(filter);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < jsonArray.size(); i++) {
			String condition = parse(jsonArray.getJSONObject(i), sb.toString());
			sb.append(condition);
		}
		
		return sb.toString();
	}

	private static String parse(JSONObject json, String sb) throws Exception{
		
		String logic = json.containsKey("logic") ? json.getString("logic") : "and";
		String field = json.getString("field").replaceAll("\\s", "");
		
		if (field.matches("\\(")) {
			 return String.format(" %s %s", logic, field);
		}else if (field.matches("\\)")) {
			return field;
		}
		if (sb.endsWith("(")) {
			logic = "";
		}
		
		String operator = json.getString("operator");
		String value1 = json.getString("value");
		String value2 = json.containsKey("value2") ? json.getString("value2") : null;
		Boolean isNumber = json.containsKey("isNumber") ? json.getBoolean("isNumber") : false;

		//不考虑类型的情况 全部作为字符串处理
		if ("包含".equals(operator)) {
			value1="'%" + value1 + "%'";
			return String.format(" %s %s like %s ", logic, field, value1);
		} else if ("不包含".equals(operator)) {
			value1="'%" + value1 + "%'";
			return String.format(" %s %s not like %s ", logic, field, value1);
		} else if ("左包含".equals(operator)) {
			value1="'" + value1 + "%'";
			return String.format(" %s %s like %s ", logic, field, value1);
		} else if ("不左包含".equals(operator)) {
			value1="'" + value1 + "%'";
			return String.format(" %s %s not like %s ", logic, field, value1);
		} else if ("右包含".equals(operator)) {
			value1="'%" + value1 + "'";
			return String.format(" %s %s like %s ", logic, field, value1);
		} else if ("不右包含".equals(operator)) {
			value1="'%" + value1 + "'";
			return String.format(" %s %s not like %s ", logic, field, value1);
		} else if ("集合".equals(operator)) {
			String strs[] = value1.split(",");
			String temp = "";
			for(String str : strs){
				if (!ValidateUtil.isAvailable(temp)) {
					temp = isNumber ? str : "'" + str + "'";
				} else {
					temp += "," + (isNumber ? str : "'" + str + "'");
				}
			}
			return String.format(" %s %s in (" + temp + ") ", logic, field, value1, temp);
		} else if (null != value2 && "介于".equals(operator)) {
			String format = isNumber ? " %s %s between %s and %s " : " %s %s between '%s' and '%s' ";
			return String.format(format, logic, field, value1, value2);
		} else if (null!=value2 && "不介于".equals(operator)) {
			String format = isNumber ? " %s %s not between %s and %s " : " %s %s not between '%s' and '%s' ";
			return String.format(format, logic, field, value1, value2);
		} else if("为空".equals(operator)) {
			return String.format(" %s (%s is null or %s = '') ", logic, field, field);
		} else if("非空".equals(operator)) {
			return String.format(" %s (%s is not null and %s != '') ", logic, field, field);
		} else{
			operator = operaction.get(operator);
			String format = isNumber ? " %s %s %s %s " : " %s %s %s '%s' ";
			return String.format(format, logic,field,operator,value1);
		}
	}
	// 1等于 2不等于 3大于 4小于 5大于等于 6小于等于 7不等于
	private static Map<String, String> operaction = new HashMap<String, String>() {
		{
			put("等于", "=");
			put("不等于", "<>");
			put("大于", ">");
			put("小于", "<");
			put("大于等于", ">=");
			put("小于等于", "<=");
		}
	};
}
