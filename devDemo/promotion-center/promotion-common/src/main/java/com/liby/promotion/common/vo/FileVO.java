package com.liby.promotion.common.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class FileVO extends BaseVO {

    @NotEmpty(groups = FileVO.ExportExcel.class, message = "exportType为空")
    private String exportType;
    @NotEmpty(groups = FileVO.ExportExcel.class, message = "header为空")
    private String header;
    private String selectData;

    @NotBlank(groups = {FileVO.DownloadFile.class}, message = "originFileName为空")
    private String originFileName;
    @NotBlank(groups = {FileVO.DownloadFile.class}, message = "fileName为空")
    private String fileName;
    @JSONField(serialize = false)
    private Boolean keepFile = false;

    public interface DownloadFile {}
    public interface ExportExcel {}
}
