package com.liby.promotion.common.result;

import com.liby.common.data.ResponseData;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Slf4j
public class Result implements Serializable {

    private boolean success;
    private String error;
    private Object data;
    private Throwable throwable;
    private Map<String, Object> map = new HashMap<String, Object>();

    public static Result succ(){
        Result result = new Result();
        result.setSuccess(true);
        return result;
    }

    public static Result succ(Object data){
        Result result = Result.succ();
        result.setData(data);
        return result;
    }

    private static Result fail() {
        Result result = new Result();
        result.setSuccess(false);
        return result;
    }

    public static Result fail(String error){
        Result result = Result.fail();
        result.setError(error);
        return result;
    }

    public static Result fail(Throwable throwable){
        Result result = Result.fail();
        result.setFailMessage(throwable);
        result.setThrowable(throwable);
        log.error("\n" + result.getStackTrace());
        return result;
    }

    public String getStackTrace() {
        if(throwable == null) {return null;}
        StringWriter sw = new StringWriter();
        throwable.printStackTrace(new PrintWriter(sw, true));
        return sw.toString();
    }

    public ResponseData toResponseData() {
        if(this.isSuccess()) {
            return new ResponseData(200, this.getData());
        } else {
            return new ResponseData(500, this.getError());
        }
    }

    protected void setFailMessage(Throwable throwable) {
        this.setSuccess(false);
        while(throwable.getCause() != null) {
            throwable = throwable.getCause();
        }
        String message = throwable.getMessage();
        this.setError(message);
    }

    public Result set(String key, Object obj) {
        this.map.put(key, obj);
        return this;
    }

    public Object get(String key) {
        return map.get(key);
    }

    public Result remove(String key) {
        this.map.remove(key);
        return this;
    }
}
