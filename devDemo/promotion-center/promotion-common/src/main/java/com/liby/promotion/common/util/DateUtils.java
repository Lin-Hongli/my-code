package com.liby.promotion.common.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {
	
	private static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static final String DATE_FORMAT = "yyyy-MM-dd";

	public static String format(Date date, String pattern) {
		return new SimpleDateFormat(pattern).format(date);
	}
	
	public static String formatDate(Date date) {
		return format(date, DATE_FORMAT);
	}

	public static String formatNowDate() {
		return format(new Date(), DATE_FORMAT);
	}

	public static String formatTime(Date date) {
		return format(date, TIME_FORMAT);
	}

	public static String formatNowTime() {
		return format(new Date(), TIME_FORMAT);
	}
	
	public static Date parse(String dateStr, String pattern) throws Exception{
		return new SimpleDateFormat(pattern).parse(dateStr);
	}
	
	public static Date parseTime(String dateStr) throws Exception {
		return parse(dateStr, TIME_FORMAT);
	}

	public static Date parseDate(String dateStr) throws Exception {
		return parse(dateStr, DATE_FORMAT);
	}

	public static Date offsetDate(Date date, int offset, TimeUnit timeUnit) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(timeUnit.getValue(), offset);
		return calendar.getTime();
	}

	public static Date offsetDate(int offset, TimeUnit timeUnit) {
		return offsetDate(new Date(), offset, timeUnit);
	}

	public static String offsetDateStr(Date date, int offset, TimeUnit timeUnit, String pattern) {
		return format(offsetDate(date, offset, timeUnit), pattern);
	}

	public static String offsetDateStr(int offset, TimeUnit timeUnit, String pattern) {
		return offsetDateStr(new Date(), offset, timeUnit, pattern);
	}
	
	public static Date getDateBeginTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	} 
	
	public static Date getDateMiddleTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 12);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();		
	}
	
	public static Date getDateEndTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	} 	
	
	public static String getDateBeginTimeStr(String dateStr, String informat, String outFormat) throws Exception {
		return format(getDateBeginTime(parse(dateStr, informat)),outFormat);
	} 
	
	public static String getDateMiddleTimeStr(String dateStr, String informat, String outFormat) throws Exception {
		return format(getDateMiddleTime(parse(dateStr, informat)),outFormat);
	} 
	
	public static String getDateEndTimeStr(String dateStr, String informat, String outFormat) throws Exception {
		return format(getDateEndTime(parse(dateStr, informat)),outFormat);
	} 	
	
	public static Date getFirstDateOfCurrentMonth(Date currentDate, boolean isDateBeginTime) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.MONTH, 0);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return isDateBeginTime ?  getDateBeginTime(calendar.getTime()) : calendar.getTime();
	}
	
	public static Date getLastDateOfCurrentMonth(Date currentDate, boolean isShiErDian) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, 0);	
		return isShiErDian ? getDateEndTime(calendar.getTime()) : calendar.getTime();
	}
	
	public static Date getFirstDateOfCurrentYear(Date currentDate, boolean isDateBeginTime) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.YEAR, 0);
		calendar.set(Calendar.DAY_OF_YEAR, 1);
		return isDateBeginTime ?  getDateBeginTime(calendar.getTime()) : calendar.getTime();
	}
	
	public static Date getLastDateOfCurrentYear(Date currentDate, boolean isShiErDian) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		calendar.add(Calendar.YEAR, 1);
		calendar.set(Calendar.DAY_OF_YEAR, 0);	
		return isShiErDian ? getDateEndTime(calendar.getTime()) : calendar.getTime();
	}
	
	public static Date getDateOfWeek(Date currentDate, int dateOfWeek, int offsetWeek) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(currentDate);
		int dayWeek = calendar.get(Calendar.DAY_OF_WEEK);
		if(dayWeek == Calendar.SUNDAY){
			dayWeek += 7;
		}
		if(dateOfWeek == Calendar.SUNDAY){
			dateOfWeek += 7;
		}
		calendar.add(Calendar.DATE, dateOfWeek - dayWeek + 7 * offsetWeek);
		return calendar.getTime();
	}

	public static String getTimeDifference(String dateStr1, String dateStr2) throws Exception {
		Date date1 = parse(dateStr1, TIME_FORMAT);
		Date date2 = parse(dateStr2, TIME_FORMAT);
		return String.valueOf(Math.abs(date2.getTime() - date1.getTime()));
	}
	
	public enum TimeUnit {  
		
		MILLISECOND(Calendar.MILLISECOND), 
		SECOND(Calendar.MILLISECOND), 
		MINUTE(Calendar.MINUTE), 
		HOUR(Calendar.HOUR), 
		DAY(Calendar.DAY_OF_MONTH), 
		MONTH(Calendar.MONDAY), 
		YEAR(Calendar.YEAR);
		
		private int value;
		
		private TimeUnit(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return this.value;
		}
	}
}
