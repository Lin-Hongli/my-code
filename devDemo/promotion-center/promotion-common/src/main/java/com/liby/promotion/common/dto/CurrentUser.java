package com.liby.promotion.common.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class CurrentUser implements Serializable {

    private String id;
    private String name;
}
