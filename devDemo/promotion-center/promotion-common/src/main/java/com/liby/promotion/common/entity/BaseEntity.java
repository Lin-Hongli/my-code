package com.liby.promotion.common.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.liby.promotion.common.id.RandomIdGenerator;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public abstract class BaseEntity extends RandomIdGenerator implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private String createrId;
    private String createrName;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date modifyTime;
    private String modifierId;
    private String modifierName;
    @TableLogic
    @JSONField(serialize = false)
    private boolean deleted;
}
