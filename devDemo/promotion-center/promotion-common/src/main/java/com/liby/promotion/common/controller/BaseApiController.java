package com.liby.promotion.common.controller;

import com.liby.common.data.ResponseData;
import com.liby.promotion.common.result.Result;
import com.liby.promotion.common.util.CallBack;
import com.liby.promotion.common.vo.BaseVO;
import com.liby.promotion.common.vo.PageVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import javax.validation.Validation;
import javax.validation.Validator;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Slf4j
public class BaseApiController<VO extends PageVO> {

    private Class<VO> voClass;

    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public BaseApiController() {

        Type genericSuperclass = this.getClass().getGenericSuperclass();
        if(genericSuperclass instanceof ParameterizedType) {
            ParameterizedType type = (ParameterizedType)this.getClass().getGenericSuperclass();
            this.voClass = (Class)type.getActualTypeArguments()[0];
        }
    }

    public ResponseData getError(BaseVO vo, Errors errors, CallBack<? extends Result> callBack) {
        try {
            String error = getError(errors);
            if(error != null) {
                return Result.fail(error).toResponseData();
            }
            return callBack.doCallBack().toResponseData();
        } catch (Exception e) {
            return Result.fail(e).toResponseData();
        }
    }

    private String getError(Errors errors) {
        if(errors.hasErrors()) {
            List<String> errorMessageList = new ArrayList<>();
            Iterator<ObjectError> iterator = errors.getAllErrors().iterator();
            while(iterator.hasNext()) {
                ObjectError error = iterator.next();
                errorMessageList.add(error.getDefaultMessage());
            }
            return StringUtils.join(errorMessageList, ",");
        }
        return null;
    }
}
