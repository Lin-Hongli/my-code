package com.liby.promotion.common.util;


import com.liby.promotion.common.result.Result;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

public class ExcelUtil {

    public static Result writeFile(String disPath, Workbook wb) {
        OutputStream os = null;
        try {
            if (wb != null) {
                os = new BufferedOutputStream(new FileOutputStream(disPath));
                wb.write(os);
                os.flush();
            }
            return Result.succ();
        } catch (Exception e) {
            return Result.fail(e);
        } finally {
            IOUtil.close(os);
        }
    }
    public static Workbook createXSSFWorkbook(String[] titles, List<Object[]> dataList) {
        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet("sheet1");
        CellStyle headerStyle = createBorderCellStyle(wb);
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        // 表头
        Row row = sheet.createRow(0);
        for (int i = 0; i < titles.length; i++) {
            String title = titles[i];
            Cell cell = row.createCell(i);
            cell.setCellValue(title);
            cell.setCellStyle(headerStyle);
        }
        CellStyle dataStyle = createBorderCellStyle(wb);
        // 数据
        for (int i = 0; i < dataList.size(); i++) {
            Object[] data = dataList.get(i);
            row = sheet.createRow(i + 1);
            for (int j = 0; j < data.length; j++) {
                Cell cell = row.createCell(j);
                Object value = data[j];
                if (ValidateUtil.isAvailable(value)) {
                    if (value instanceof Number) {
                        DataFormat df = wb.createDataFormat();
                        if(value.toString().matches("^(-?\\d+)(\\.\\d+)?$")) {
                            dataStyle.setDataFormat(df.getFormat("#,#0"));
                        } else if(value.toString().matches("^[-\\+]?[\\d]*$")) {
                            dataStyle.setDataFormat(df.getFormat("#,##0.00"));
                        }
                        cell.setCellValue(Double.valueOf(String.valueOf(value)));
                    } else if(value instanceof Date) {
                        cell.setCellValue(DateUtils.format((Date)value, "yyyy-MM-dd HH:mm:ss"));
                    } else {
                        cell.setCellValue(String.valueOf(value));
                    }
                }
                cell.setCellStyle(dataStyle);
            }
        }
        return wb;
    }

    private static CellStyle createBorderCellStyle(Workbook wb) {
        CellStyle style = wb.createCellStyle();
        style.setBorderBottom(CellStyle.BORDER_THIN);  //下边框
        style.setBorderLeft(CellStyle.BORDER_THIN);    //左边框
        style.setBorderTop(CellStyle.BORDER_THIN);     //上边框
        style.setBorderRight(CellStyle.BORDER_THIN);   //右边框
        return style;
    }

    public static void autoSizeColumn(Workbook wb, int sheetIndex, int beginColumnIndex, int endColumnIndex) {
        Sheet sheet = wb.getSheetAt(sheetIndex);
        if (sheet != null) {
            for (int i = beginColumnIndex; i <= endColumnIndex; i++) {
                sheet.autoSizeColumn(i);
            }
            /* 实际宽度 < 默认宽度的时候、设置为默认宽度 */
            for (int i = beginColumnIndex; i < endColumnIndex; i++) {
                int curColWidth = sheet.getColumnWidth(i);
                if (curColWidth < 2500) {
                    sheet.setColumnWidth(i, 2500);
                }
            }
        }
    }
}
