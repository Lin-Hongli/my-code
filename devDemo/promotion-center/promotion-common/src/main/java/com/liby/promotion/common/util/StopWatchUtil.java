package com.liby.promotion.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StopWatch;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class StopWatchUtil {

    private static ThreadLocal<Map<String, StopWatch>> threadLocal = new ThreadLocal<>();

    public static void watch(String id, String watchName) {
        Map<String, StopWatch> stopWatchMap = threadLocal.get();
        if(stopWatchMap == null) {
            stopWatchMap = new HashMap<>();
        }
        StopWatch stopWatch = stopWatchMap.get(id);
        if(stopWatch == null) {
            StackTraceElement stackTraceElement = new Exception().getStackTrace()[1];
            String className = stackTraceElement.getClassName();
            String methodName = stackTraceElement.getMethodName();
            String watchId = className + "." + methodName;
            stopWatch = new StopWatch(watchId);
            stopWatch.start(watchName);
            stopWatchMap.put(id, stopWatch);
            threadLocal.set(stopWatchMap);
        } else {
            stopWatch.stop();
            stopWatch.start(watchName);
        }
    }

    public static void prettyPrint(String id) {
        Map<String, StopWatch> stopWatchMap = threadLocal.get();
        if(stopWatchMap != null) {
            StopWatch stopWatch = stopWatchMap.get(id);
            if(stopWatch != null) {
                stopWatch.stop();
                log.info("\n" + stopWatch.prettyPrint());
                stopWatchMap.remove(id);
                if(stopWatchMap.isEmpty()) {
                    threadLocal.remove();
                }
            }
        }
    }
}
