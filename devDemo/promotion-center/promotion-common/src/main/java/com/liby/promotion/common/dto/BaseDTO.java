package com.liby.promotion.common.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BaseDTO extends PageDTO {

    private Integer version;
    private String ids;
}
