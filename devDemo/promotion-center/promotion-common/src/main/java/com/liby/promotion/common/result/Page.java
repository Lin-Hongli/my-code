package com.liby.promotion.common.result;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class Page<T> implements Serializable {

    private Integer pageSize;
    private Integer currentPage;
    private int total;
    private List<T>  data = new ArrayList<>();
}
