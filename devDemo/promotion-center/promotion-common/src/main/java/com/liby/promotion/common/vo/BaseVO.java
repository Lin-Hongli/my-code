package com.liby.promotion.common.vo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BaseVO {

    @NotNull(groups = {
            BaseVO.UpdateById.class,
            BaseVO.SelectById.class,
            BaseVO.DeleteById.class
    }, message = "id不能为空")
    private Long id;
    @NotEmpty(groups = BaseVO.BatchDelete.class, message = "ids不能为空")
    private String ids;

    public interface BatchDelete {}
    public interface UpdateById {}
    public interface SelectById {}
    public interface DeleteById {}
}
