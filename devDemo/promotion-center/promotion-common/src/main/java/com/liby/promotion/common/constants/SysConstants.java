package com.liby.promotion.common.constants;

/**
 * SysConstants 常量
 *
 * @author lgj liugongjun@chinasie.com
 * @version 0.0.1
 * @sine 2018年12月19日
 */
public class SysConstants {
    //安全验证token
    public static final String AUTH_TOKEN = "token";
    public static final String LOCALE = "locale";

    public static final String EXPORT_TYPE_ALL = "all";
    public static final String EXPORT_TYPE_SOME = "some";
}
