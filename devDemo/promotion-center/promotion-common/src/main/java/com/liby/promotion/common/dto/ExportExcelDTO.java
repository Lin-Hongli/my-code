package com.liby.promotion.common.dto;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.liby.promotion.common.entity.BaseEntity;
import com.liby.promotion.common.util.ValidateUtil;
import lombok.Getter;
import lombok.Setter;

import java.net.URLDecoder;

@Getter
@Setter
public abstract class ExportExcelDTO extends BaseEntity {
    @JsonIgnore
    @JSONField(serialize = false)
    private String exportType;
    @JsonIgnore
    @JSONField(serialize = false)
    private String header;
    @JsonIgnore
    @JSONField(serialize = false)
    private String selectData;

    public JSONArray headerJsonArray() throws Exception {
        if(ValidateUtil.isAvailable(header)) {
            String jsonStr = URLDecoder.decode(header,"utf-8");
            return JSONArray.parseArray(jsonStr);
        }
        return new JSONArray();
    }
    public JSONArray selectDataJsonArray() throws Exception {
        if(ValidateUtil.isAvailable(selectData)) {
            String jsonStr = URLDecoder.decode(selectData,"utf-8");
            return JSONArray.parseArray(jsonStr);
        }
        return new JSONArray();
    }
}
