package com.liby.promotion.common.util;

public interface CallBack<R> {

    R doCallBack(Object... t) throws Exception;

}
