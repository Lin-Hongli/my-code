package com.liby.promotion.common.redis.service;

import com.liby.promotion.common.dto.PageDTO;
import com.liby.promotion.common.result.Page;
import com.liby.promotion.common.result.Result;
import com.liby.promotion.common.util.CallBack;
import com.liby.promotion.common.util.StopWatchUtil;
import com.liby.promotion.common.util.StringUtil;
import com.liby.promotion.common.util.ValidateUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public abstract class RedisCacheService<T> {

    protected RedisUtil redisUtil;

    public void setRedisUtil(RedisUtil redisUtil) {
        this.redisUtil = redisUtil;
    }

    protected static final String LIST_COUNT_KEY = "total";
    protected static final String separator = "::";
    protected static final String PAGE_KEY = "PAGE";
    protected static final String LIST_KEY = "LIST";
    protected static final String ONE_KEY = "ONE";
    protected static final String EXPORT_KEY = "EXPORT";
    protected static final String THREAD_LOCK_KEY_PRE = "THREAD_LOCK";
    protected String group;

    public RedisCacheService() {
        ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
        group = ((Class<T>) type.getActualTypeArguments()[0]).getSimpleName();
    }

    protected Result cachePage(String group, PageDTO pageDTO, CallBack<Result> callBack) {
        String watchId = StringUtil.getUuid();
        try {
            if(!ValidateUtil.isAvailable(pageDTO.getOrderBy())) {
                pageDTO.setOrderBy("id desc");
            }
            String filter = pageDTO.getFilter();
            if(!ValidateUtil.isAvailable(filter)) {
                pageDTO.setFilter("[]");
            } else {
                pageDTO.setFilter(URLDecoder.decode(pageDTO.getFilter(), "utf-8"));
            }
            Integer pageSize = pageDTO.getPageSize();
            if(pageSize == null || pageSize <= 0) {
                pageDTO.setPageSize(10);
            }
            Integer currentPage = pageDTO.getCurrentPage();
            if(currentPage == null || currentPage <= 0) {
                pageDTO.setCurrentPage(1);
            }
            if(redisUtil == null) {
                StopWatchUtil.watch(watchId, "查询Page数据");
                return callBack.doCallBack();
            }
            StopWatchUtil.watch(watchId, "获取Page【" + group + "】缓存数据");
            String mapKey = group + separator + PAGE_KEY;
            String pageKeyFormat = getPreMethodName() + separator + "%s" + separator + "%s" + separator + "%s" + separator + "%s";
            String pageKey = String.format(pageKeyFormat,
                    pageDTO.getOrderBy(), pageDTO.getFilter(),
                    pageDTO.getPageSize(), pageDTO.getCurrentPage()
            );
            List<Map<String, Object>> dataList = (List<Map<String, Object>>) redisUtil.getFromMap(mapKey, pageKey);
            if(ValidateUtil.isAvailable(dataList)) {
                Integer total = (Integer) redisUtil.getFromMap(mapKey, LIST_COUNT_KEY);
                Page<Map<String, Object>> page = new Page<>();
                page.setData(dataList);
                page.setTotal(total);
                page.setPageSize(pageDTO.getPageSize());
                page.setCurrentPage(pageDTO.getCurrentPage());
                return Result.succ(page);
            }
            StopWatchUtil.watch(watchId, "查询Page【" + group + "】数据");
            Result result = callBack.doCallBack();
            Page page = (Page) result.getData();
            List<?> data = page.getData();
            if(data.isEmpty()) {
                log.info("数据库查询不到任何Page【" + group + "】数据【" + StringUtil.toJSONString(pageDTO) + "】");
            } else {
                Map<String, Object> map = new HashMap<>();
                map.put(LIST_COUNT_KEY, page.getTotal());
                map.put(pageKey, data);
                StopWatchUtil.watch(watchId, "缓存Page【" + group + "】数据");
                redisUtil.putIntoMap(mapKey, map);
            }
            return result;
        } catch (Exception e) {
            return Result.fail(e);
        } finally {
            StopWatchUtil.prettyPrint(watchId);
        }
    }

    protected Result cachePage(PageDTO pageDTO, CallBack<Result> callBack) {
        return cachePage(this.group, pageDTO, callBack);
    }

    protected Result cacheExport(String group, PageDTO pageDTO, CallBack<Result> callBack) {
        String watchId = StringUtil.getUuid();
        try {
            if(!ValidateUtil.isAvailable(pageDTO.getOrderBy())) {
                pageDTO.setOrderBy("id desc");
            }
            String filter = pageDTO.getFilter();
            if(!ValidateUtil.isAvailable(filter)) {
                pageDTO.setFilter("[]");
            } else {
                pageDTO.setFilter(URLDecoder.decode(pageDTO.getFilter(), "utf-8"));
            }
            if(redisUtil == null) {
                StopWatchUtil.watch(watchId, "查询Export数据");
                return callBack.doCallBack();
            }
            String threadLockKey = THREAD_LOCK_KEY_PRE + separator + group + "*";
            StopWatchUtil.watch(watchId, "获取线程锁【" + threadLockKey + "】");
            if (redisUtil.isThreadLock(threadLockKey, 20)) {
                return Result.fail("系统繁忙");
            }
            StopWatchUtil.watch(watchId, "获取Export【" + group + "】缓存数据");
            String mapKey = group + separator + EXPORT_KEY;
            String exportKeyFormat = getPreMethodName() + separator + "%s" + separator + "%s";
            String exportKey = String.format(exportKeyFormat, pageDTO.getOrderBy(), pageDTO.getFilter());
            List<Map<String, Object>> dataList = (List<Map<String, Object>>) redisUtil.getFromMap(mapKey, exportKey);
            if(ValidateUtil.isAvailable(dataList)) {
                return Result.succ(dataList);
            }
            StopWatchUtil.watch(watchId, "查询Export【" + group + "】数据");
            Result result = callBack.doCallBack();
            dataList = (List<Map<String, Object>>) result.getData();
            if(dataList.isEmpty()) {
                log.info("数据库查询不到任何Export【" + group + "】数据【" + StringUtil.toJSONString(pageDTO) + "】");
            } else {
                StopWatchUtil.watch(watchId, "缓存Export【" + group + "】数据");
                redisUtil.putIntoMap(mapKey, exportKey, dataList);
            }
            return result;
        } catch (Exception e) {
            return Result.fail(e);
        } finally {
            StopWatchUtil.prettyPrint(watchId);
        }
    }

    protected Result cacheExport(PageDTO pageDTO, CallBack<Result> callBack) {
        return cacheExport(this.group, pageDTO, callBack);
    }

    protected Result cacheList(String group, String listKey, CallBack<Result> callBack) {
        String watchId = StringUtil.getUuid();
        try {
            if(redisUtil == null) {
                StopWatchUtil.watch(watchId, "查询List【" + group + "】数据");
                return callBack.doCallBack();
            }
            String threadLockKey = THREAD_LOCK_KEY_PRE + separator + group + "*";
            StopWatchUtil.watch(watchId, "获取线程锁【" + threadLockKey + "】");
            if (redisUtil.isThreadLock(threadLockKey, 20)) {
                return Result.fail("系统繁忙");
            }
            StopWatchUtil.watch(watchId, "获取List【" + group + "】缓存数据");
            String mapKey = group + separator + LIST_KEY;
            listKey = getPreMethodName() + (ValidateUtil.isAvailable(listKey) ? separator + listKey : "");
            List<T> list = (List<T>) redisUtil.getFromMap(mapKey, listKey);
            if(ValidateUtil.isAvailable(list)) {
                return Result.succ(list);
            }
            StopWatchUtil.watch(watchId, "查询List【" + group + "】数据");
            Result result = callBack.doCallBack();
            List<?> data = (List<?>) result.getData();
            if(data.isEmpty()) {
                log.info("数据库查询不到任何List【" + group + "】数据【" + StringUtil.toJSONString(listKey) + "】");
            } else {
                StopWatchUtil.watch(watchId, "缓存List【" + group + "】数据");
                redisUtil.putIntoMap(mapKey, listKey, data);
            }
            return result;
        } catch (Exception e) {
            return Result.fail(e);
        } finally {
            StopWatchUtil.prettyPrint(watchId);
        }
    }

    protected Result cacheList(String listKey, CallBack<Result> callBack) {
        return cacheList(group, listKey, callBack);
    }

    protected Result cacheList(CallBack<Result> callBack) {
        return cacheList(group, null, callBack);
    }

    protected Result cacheOne(String group, Serializable id, CallBack<Result> callBack) {
        String watchId = StringUtil.getUuid();
        try {
            if(redisUtil == null) {
                StopWatchUtil.watch(watchId, "查询One【" + group + "】数据");
                return callBack.doCallBack();
            }
            String threadLockKey = THREAD_LOCK_KEY_PRE + separator + group + separator + id;
            StopWatchUtil.watch(watchId, "获取线程锁【" + threadLockKey + "】");
            if (redisUtil.isThreadLock(threadLockKey, 20)) {
                return Result.fail("系统繁忙");
            }
            StopWatchUtil.watch(watchId, "获取One【" + group + "】缓存数据");
            String mapKey = group + separator + ONE_KEY;
            id = getPreMethodName() + separator + id;
            Object data = redisUtil.getFromMap(mapKey, id);
            if(data != null) {
                return Result.succ(data);
            }
            StopWatchUtil.watch(watchId, "查询One【" + group + "】数据");
            Result result = callBack.doCallBack();
            data = (T) result.getData();
            if(data != null) {
                StopWatchUtil.watch(watchId, "缓存One【" + group + "】数据");
                redisUtil.putIntoMap(mapKey, id, result.getData());
            } else {
                log.info("数据库查询不到任何One【" + group + "】数据【" + id + "】");
            }
            return result;
        } catch (Exception e) {
            return Result.fail(e);
        } finally {
            StopWatchUtil.prettyPrint(watchId);
        }
    }

    protected Result cacheOne(Serializable id, CallBack<Result> callBack) {
        return cacheOne(group, id, callBack);
    }

    protected Result cacheSave(CallBack<Result> callBack, String... groups) {
        String watchId = StringUtil.getUuid();
        try {
            if(redisUtil == null) {
                StopWatchUtil.watch(watchId, "保存One【" + group + "】数据");
                return callBack.doCallBack();
            }
            StopWatchUtil.watch(watchId, "保存One【" + group + "】数据");
            Result result = callBack.doCallBack();
            if(groups.length > 0) {
                StopWatchUtil.watch(watchId, "移除缓存【" + StringUtil.toJSONString(groups) + "】");
                redisUtil.delete(Arrays.asList(groups).stream().map(group -> group + "*").toArray(String[]::new));
            } else {
                StopWatchUtil.watch(watchId, "移除缓存【" + group + "】");
                redisUtil.delete(group + "*");
            }
            return  result;
        } catch (Exception e) {
            return Result.fail(e);
        } finally {
            StopWatchUtil.prettyPrint(watchId);
        }
    }

    protected Result cacheUpdate(Serializable id, CallBack<Result> callBack, String... groups) {
        String watchId = StringUtil.getUuid();
        try {
            if(redisUtil == null) {
                StopWatchUtil.watch(watchId, "更新One【" + group + "】数据");
                return callBack.doCallBack();
            }
            String threadLockKey = THREAD_LOCK_KEY_PRE + separator + group + separator + id;
            StopWatchUtil.watch(watchId, "添加线程锁【" + threadLockKey + "】");
            return redisUtil.threadLock(threadLockKey, 50, "系统繁忙", (params) -> {
                StopWatchUtil.watch(watchId, "更新One【" + group + "】数据");
                Result result = callBack.doCallBack();
                if(result.isSuccess()) {
                    if(groups.length > 0) {
                        StopWatchUtil.watch(watchId, "移除缓存【" + StringUtil.toJSONString(groups) + "】");
                        redisUtil.delete(Arrays.asList(groups).stream().map(group -> group + "*").toArray(String[]::new));
                    } else {
                        StopWatchUtil.watch(watchId, "移除缓存【" + group + "】");
                        redisUtil.delete(group + "*");
                    }
                }
                StopWatchUtil.watch(watchId, "移除线程锁【" + threadLockKey + "】");
                return  result;
            });
        } catch (Exception e) {
            return Result.fail(e);
        } finally {
            StopWatchUtil.prettyPrint(watchId);
        }
    }

    private String getPreMethodName() {
        String thisClassName = RedisCacheService.class.getName();
        StackTraceElement[] stackTrace = new Exception().getStackTrace();
        for (int i = 0; i < stackTrace.length; i++) {
            StackTraceElement stackTraceElement = stackTrace[i];
            if(!thisClassName.equals(stackTraceElement.getClassName())) {
                return stackTraceElement.getMethodName();
            }
        }
        return "";
    }
}
