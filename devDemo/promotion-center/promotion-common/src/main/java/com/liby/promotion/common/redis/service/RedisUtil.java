package com.liby.promotion.common.redis.service;

import com.liby.promotion.common.result.Result;
import com.liby.promotion.common.util.CallBack;
import com.liby.promotion.common.util.StringUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Setter
@Getter
@Slf4j
public class RedisUtil<K, V> {

    private RedisTemplate<String, V> redisTemplate;

    public void set(String key, V value) {
        redisTemplate.opsForValue().set(key, value);
        log.info(String.format("缓存：key:%s", key));
        log.info(String.format("缓存：value:%s", StringUtil.toJSONString(value)));
    }

    public void set(String key, V value, long time, TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(key, value, time, timeUnit);
        log.info(String.format("缓存：key:%s", key));
        log.info(String.format("缓存：value:%s", StringUtil.toJSONString(value)));
    }

    private boolean setIfAbsent(String key, V value, long time, TimeUnit timeUnit) {
        Boolean flag = redisTemplate.execute(new SessionCallback<Boolean>() {
            @Override
            @SuppressWarnings("unchecked")
            public Boolean execute(RedisOperations operations) throws DataAccessException {
                operations.multi();
                redisTemplate.opsForValue().setIfAbsent(key, value);
                redisTemplate.expire(key, time, timeUnit);
                List<Object> exec = operations.exec();
                if (exec.size() > 0) {
                    return (Boolean) exec.get(0);
                }
                return false;
            }
        });
        if(flag) {
            log.info(String.format("缓存：key:%s", key));
            log.info(String.format("缓存：value:%s", StringUtil.toJSONString(value)));
        }
        return flag;
    }

    public boolean setIfAbsent(String key, V value) {
        return redisTemplate.opsForValue().setIfAbsent(key, value);
    }

    public Long increment(String key, long value) {
        return redisTemplate.opsForValue().increment(key, value);
    }

    public V get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public Collection<String> keys(String key) {
        return redisTemplate.keys(key);
    }

    public void delete(String... keys) {
        List<String> keyList = Arrays.asList(keys);
        redisTemplate.delete(keyList);
        log.info(String.format("移除缓存：keys:%s", keyList));
    }

    public void delete(String key) {
        if(key.contains("*")) {
            redisTemplate.delete(this.keys(key));
        } else {
            redisTemplate.delete(key);
        }
        log.info(String.format("移除缓存：key:%s", key));
    }

    public void putIntoMap(String mapKey, K key, V value) {
        redisTemplate.opsForHash().put(mapKey, key, value);
        log.info(String.format("缓存：mapKey:%s,key:%s", mapKey, key));
        log.info(String.format("缓存：value:%s", StringUtil.toJSONString(value)));
    }

    public void putIntoMap(String mapKey, Map<String, Object> map) {
        redisTemplate.opsForHash().putAll(mapKey, map);
        for(Map.Entry entry : map.entrySet()) {
            log.info(String.format("缓存：mapKey:%s,key:%s", mapKey, entry.getKey()));
            log.info(String.format("缓存：value:%s", StringUtil.toJSONString(entry.getValue())));
        }
    }

    public void removeFromMap(String mapKey, K... key) {
        redisTemplate.opsForHash().delete(mapKey, key);
        log.info(String.format("移除缓存：mapKey:%s,key:%s", mapKey, key));
    }

    public Object getFromMap(String mapKey, K key) {
        return redisTemplate.opsForHash().get(mapKey, key);
    }

    public Map<Object, Object> entriesFromMap(String mapKey) {
        return redisTemplate.opsForHash().entries(mapKey);
    }

    /*
     *   并发锁（同一个地方调用，key必定一样，一个线程抢到锁释放前其他线程均失败）
     */
    public Result concurrentLock (String key, String value, long time, TimeUnit timeUnit, int retryTime, String error, CallBack<Result> callBack, Object... params) throws Exception {
        if(value == null) {
            value = StringUtil.getUuid();
        }
        int tryTime = 0;
        while(!setIfAbsent(key, (V)value, time, timeUnit) && tryTime++ < retryTime) {
            Thread.sleep((int)(Math.random() * 200));
        }
        if(tryTime >= retryTime) {
            log.info("获取并发锁：" + key + "共【" + retryTime + "】次失败");
            return Result.fail(error);
        }
        log.info("获取到并发锁【" + key + "】");
        try {
            return callBack.doCallBack(params);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.fail(error);
        } finally {
            Object lockValue = this.get(key);
            if(value.equals(lockValue)) {
                log.info("移除并发锁【" + key + "】");
                this.delete(key);
            }
        }
    }

    /*
     *   线程锁(key跟业务相关，指定个业务或数据被锁定，一个业务被锁定，所有的相关操作都无法进行，例如商品锁，锁住后其他线程不给下单，不给修改商品的状态，如上下架等)
     * */
    public Result threadLock (String key, int retryTime, String error, CallBack<Result> callBack, Object...params) throws Exception {
        int tryTime = 0;
        while(!setIfAbsent(key, (V)"1") && tryTime++ < retryTime) {
            Thread.sleep((int)(Math.random() * 200));
        }
        if(tryTime >= retryTime) {
            log.info("获取线程锁：" + key + "共【" + retryTime + "】次失败");
            return Result.fail(error);
        }
        log.info("获取到线程锁【" + key + "】");
        try {
            return callBack.doCallBack(params);
        } catch (Throwable e) {
            return Result.fail(e);
        } finally {
            log.info("移除线程锁【" + key + "】");
            this.delete(key);
        }
    }

    public boolean isThreadLock(String key, int retryTime) throws Exception{
        Collection<String> keys = key.contains("*") ? keys(key) : Arrays.asList(new String[]{key});
        Iterator<String> iterator = keys.iterator();
        while(iterator.hasNext()) {
            int tryTime = 0;
            String keyI = iterator.next();
            V v = get(keyI);
            while(v != null && tryTime++ < retryTime) {
                Thread.sleep((int)(Math.random() * 200));
            }
            if(tryTime >= retryTime) {
                log.info("获取线程锁：" + keyI + "共【" + retryTime + "】次失败");
                return true;
            }
        }
        log.info("获取线程锁：" + key + "成功");
        return false;
    }

    public Boolean expire(String key, long timeout, TimeUnit timeUnit) {
        return redisTemplate.expire(key, timeout, timeUnit);
    }

    public Boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }
}
