package com.liby.promotion.common.vo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PageVO extends FileVO {

    @NotNull(groups = {PageVO.PageList.class}, message = "pageSize不能为空")
    @Min(groups = {PageVO.PageList.class}, value = 1, message = "pageSize必须为正整数")
    private Integer pageSize;
    private String filter;
    @NotNull(groups = {PageVO.PageList.class}, message = "currentPage不能为空")
    @Min(groups = {PageVO.PageList.class}, value = 1, message = "currentPage必须为正整数")
    private Integer currentPage;
    private String orderBy;

    public interface PageList {}
}
