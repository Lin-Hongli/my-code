package com.liby.promotion.common.id;

/**
 * @author wzt on 2020/3/9.
 * @version 1.0
 */
public interface IdGenerator {

    /**
     * ID生成接口（格式：时间戳毫秒-本机IP最后一段和8位随机字符）
     * @return ID
     */
    String generate();

    /**
     * 生成UUID（已替换中划线）
     * @return ID
     */
    String generateUuid();

}
