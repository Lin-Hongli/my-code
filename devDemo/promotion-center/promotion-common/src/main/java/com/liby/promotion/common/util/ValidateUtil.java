package com.liby.promotion.common.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public class ValidateUtil {
	
	public static boolean isAvailable(Object obj) {
		return obj != null;
	}

	public static boolean isAvailable(Serializable serializable) {
		return serializable instanceof String ? isAvailable((String) serializable) : isAvailable((Object)serializable);
	}
	
	public static boolean isAvailable(Map<?,?> obj) {
		return obj != null && !obj.isEmpty();
	}

	public static boolean isAvailable(Collection<?> collection) {
		return collection != null && !collection.isEmpty();
	}

	public static boolean isAvailable(String str) {
		return str != null && !"".equals(str) && !"null".equals(str) && !"undefined".equals(str);
	}

	public static boolean isAvailable(Object[] array) {
		return array != null && array.length > 0;
	}
	
	public static boolean isAvailableName(String str) {
		return str.matches("[a-zA-Z][a-zA-z0-9]*");
	}
}
