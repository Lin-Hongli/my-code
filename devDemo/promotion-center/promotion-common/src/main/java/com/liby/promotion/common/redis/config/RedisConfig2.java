package com.liby.promotion.common.redis.config;

import com.liby.promotion.common.redis.service.RedisUtil;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

@Configuration
@ConditionalOnProperty(prefix = "spring.redis2", name = {"host","password"})
@ConfigurationProperties(prefix = "spring.redis2")
@EnableCaching
public class RedisConfig2 extends AbstractRedisConfig {

    @Primary
    @Bean
    @Qualifier("redisTemplate2")
    public RedisTemplate<String, Object> redisTemplate2(@Qualifier("connectionFactory2") RedisConnectionFactory connectionFactory) {
        return createRedisTemplate(connectionFactory);
    }

    @Primary
    @Bean
    public RedisConnectionFactory connectionFactory2() {
        return createConnectionFactory();
    }

    @Bean
    @Primary
    public RedisUtil redisUtil2(@Qualifier("redisTemplate2") RedisTemplate<String, Object> redisTemplate) {
        RedisUtil<String, Object> redisUtil = new RedisUtil<>();
        redisUtil.setRedisTemplate(redisTemplate);
        return redisUtil;
    }

    @Bean
    RedisMessageListenerContainer container(@Qualifier("connectionFactory2") RedisConnectionFactory factory) {
        return createRedisMessageListenerContainer(factory);
    }

}
