package com.liby.promotion.common.service.rpc;

import com.liby.promotion.common.dto.CurrentUser;
import com.liby.promotion.common.entity.BaseEntity;
import com.liby.promotion.common.result.Result;

public interface IBaseExportService<DTO extends BaseEntity> {

    Result pageList(DTO dto);

    Result save(DTO dto, CurrentUser currentUser, String... groups);

    Result selectById(DTO dto);

    Result updateById(DTO dto, CurrentUser currentUser, String... groups);

    Result deleteById(DTO dto, CurrentUser currentUser, String... groups);

    Result batchDeleteById(DTO dto, CurrentUser currentUser, String... groups);

    Result getExportTableData(DTO dto);
}
