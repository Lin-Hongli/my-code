package com.liby.promotion.common.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class PageDTO extends ExportExcelDTO{
    @JsonIgnore
    @JSONField(serialize = false)
    private Integer pageSize;
    @JsonIgnore
    @JSONField(serialize = false)
    private String filter;
    @JsonIgnore
    @JSONField(serialize = false)
    private Integer currentPage;
    @JsonIgnore
    @JSONField(serialize = false)
    private String orderBy;
}
