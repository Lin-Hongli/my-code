package com.liby.promotion.common.redis.config;

import com.liby.promotion.common.redis.service.RedisUtil;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
@ConditionalOnProperty(prefix = "spring.redis1", name = {"host","password"})
@ConfigurationProperties(prefix = "spring.redis1")
@EnableCaching
public class RedisConfig1 extends AbstractRedisConfig {

    @Bean
    @Qualifier("redisTemplate")
    public RedisTemplate<String, Object> redisTemplate(@Qualifier("connectionFactory")RedisConnectionFactory connectionFactory) {
        return createRedisTemplate(connectionFactory);
    }

    @Bean
    public RedisConnectionFactory connectionFactory() {
        return createConnectionFactory();
    }

    @Bean
    public RedisUtil redisUtil1(@Qualifier("redisTemplate") RedisTemplate<String, Object> redisTemplate) {
        RedisUtil<String, Object> redisUtil = new RedisUtil<>();
        redisUtil.setRedisTemplate(redisTemplate);
        return redisUtil;
    }


}
