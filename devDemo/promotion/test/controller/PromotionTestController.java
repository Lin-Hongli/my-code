package com.liby.s2b.mop.web.promotion.test.controller;

import com.liby.common.data.ResponseData;
import com.liby.mop.promotion.test.service.ITestService;
import com.liby.mop.promotion.test.vo.TestVo;
import com.liby.promotion.common.vo.BaseVO;
import com.liby.promotion.common.vo.FileVO;
import com.liby.promotion.common.vo.PageVO;
import com.liby.s2b.mop.web.promotion.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class PromotionTestController extends BaseController<TestVo> {

    @Autowired
    private ITestService testService;

    @RequestMapping("/pageList")
    public ResponseData pageList(
            @Validated(PageVO.PageList.class) @RequestBody TestVo testVo, BindingResult bindingResult) {
        return getError(testVo, bindingResult, (params) -> testService.pageList(testVo));
    }

    @RequestMapping("/selectById")
    public ResponseData selectById(
            @Validated(BaseVO.SelectById.class) @RequestBody TestVo testVo, BindingResult bindingResult) {
        return getError(testVo, bindingResult, (params) -> testService.selectById(testVo));
    }

    @RequestMapping("/save")
    public ResponseData save(
            @Validated(TestVo.Save.class) @RequestBody TestVo testVo, BindingResult bindingResult) {
        return getError(testVo, bindingResult, (params) -> testService.save(testVo)
        );
    }

    @RequestMapping("/updateById")
    public ResponseData updateById(
            @Validated(BaseVO.UpdateById.class) @RequestBody TestVo testVo, BindingResult bindingResult) {
        return getError(testVo, bindingResult, (params) -> testService.updateById(testVo)
        );
    }

    @RequestMapping("/batchDeleteById")
    public ResponseData batchDeleteById(
            @Validated(BaseVO.BatchDelete.class) @RequestBody TestVo testVo, BindingResult bindingResult) {
        return getError(testVo, bindingResult, (params) -> testService.batchDeleteById(testVo));
    }

    @RequestMapping("/deleteById")
    public ResponseData deleteById(
            @Validated(BaseVO.DeleteById.class) @RequestBody TestVo testVo, BindingResult bindingResult) {
        return getError(testVo, bindingResult, (params) -> testService.deleteById(testVo));
    }

    @RequestMapping("/exportTableData")
    public ResponseData exportTableData(@Validated(FileVO.ExportExcel.class) @RequestBody TestVo testVo, BindingResult bindingResult) {
        return getError(testVo, bindingResult, (p1) -> testService.exportTableData(tempPath, (p2) ->
                testService.getExportTableData(testVo)
        ));
    }
}
