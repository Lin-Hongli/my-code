package com.liby.s2b.mop.web.promotion.base;

import com.liby.promotion.common.controller.BaseApiController;
import com.liby.promotion.common.vo.PageVO;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "promotion.center.file")
@Setter
public class BaseController<VO extends PageVO> extends BaseApiController<VO> {

    protected String tempPath;

}