package com.liby.s2b.mop.web.promotion.file;

import com.liby.common.data.ResponseData;
import com.liby.mop.promotion.file.service.IFileService;
import com.liby.promotion.common.result.Result;
import com.liby.promotion.common.vo.FileVO;
import com.liby.s2b.mop.web.promotion.base.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.OutputStream;

@RestController
@RequestMapping("/file")
@Slf4j
public class FileController extends BaseController {

    @Autowired
    private IFileService fileService;

    @RequestMapping("/download")
    public ResponseData download(@Validated(FileVO.DownloadFile.class) FileVO fileVO, BindingResult bindingResult, HttpServletResponse response) {
        return getError(fileVO, bindingResult, (params) -> {
            try {
                Result result = fileService.download(tempPath, fileVO);
                if(!result.isSuccess()) {
                    log.info(result.getError());
                    return result;
                }
                byte[] data = (byte[]) result.getData();
                String fileName = fileVO.getFileName();
                fileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1"); // 处理safari的乱码问题
                response.reset();
                response.setHeader("Content-disposition", String.format("attachment; filename=\"%s\"", fileName));
                response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
                response.setCharacterEncoding("utf-8");
                response.setContentType("application/OCTET-STREAM;charset=UTF-8");
                OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
                outputStream.write(data);
                outputStream.flush();
                outputStream.close();
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                return Result.fail(e);
            }
        });
    }
}
