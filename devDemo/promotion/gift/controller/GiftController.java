package com.liby.s2b.mop.web.promotion.gift.controller;

import com.alibaba.fastjson.JSONArray;
import com.liby.common.data.ResponseData;
import com.liby.mop.promotion.gift.entity.BigGoods;
import com.liby.mop.promotion.gift.service.GiftService;
import com.liby.mop.promotion.gift.vo.BigGoodsVo;
import com.liby.mop.promotion.gift.vo.BigPromotionInfoVo;
import com.liby.mop.promotion.gift.vo.PageVo;
import com.liby.promotion.gift.export.dto.BigPromotionInfoDto;
import com.liby.trading.order.export.dto.mdm.Distributor;
import com.liby.trading.order.export.dto.mdm.Material;
import com.liby.trading.order.export.dto.mdm.Response;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/gift")
public class GiftController {
    @Resource
    private GiftService giftService;


    /**
     * 分页查询所有活动列表
     *
     * @param pageVo
     * @param bindingResult
     * @param bigPromotionInfoVo（条件）
     * @return
     */
    @RequestMapping(value = "/selectAll", method = RequestMethod.POST)
    public ResponseData selectAll(@Valid PageVo pageVo, BindingResult bindingResult, @RequestBody(required = false) BigPromotionInfoVo bigPromotionInfoVo) {
        //获取错误信息
        List<ObjectError> allErrors = bindingResult.getAllErrors();
        String errorStr = "";
        //获取错误信息拼接到字符串
        for (ObjectError error :
                allErrors) {
            errorStr += error.getDefaultMessage();
        }
        //如果有错误，则直接返回错误信息
        if (!"".equals(errorStr))
            return new ResponseData(500, errorStr);
        List<BigPromotionInfoDto> bigPromotionInfoDtos = giftService.selectAll(pageVo.getPageNo(), pageVo.getPageSize(), bigPromotionInfoVo);
        return new ResponseData(bigPromotionInfoDtos);
    }

    /**
     * 根据id查询活动
     *
     * @param id（id）
     * @return
     */
    @RequestMapping(value = "/selectById", method = RequestMethod.GET)
    public ResponseData selectById(String id) {
        Long idNum = null;
        try {
            idNum = Long.valueOf(id);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseData(500, "错误的参数类型");
        }
        if (giftService.queryById(idNum) == null) {
            return new ResponseData(500, "活动id错误,请输入正确的活动id");
        }
        return new ResponseData(giftService.queryById(idNum));
    }

    /**
     * 根据id上下架活动
     *
     * @param id（id）
     * @return
     */
    @RequestMapping(value = "/updateStatus", method = RequestMethod.GET)
    public ResponseData updateStatus(Long id) {
        if (id == null)
            return new ResponseData(500, "或欧东id不能为空");
        return giftService.updateStatus(id);
    }

    /*
    //id为空则新增，不为空则修改
         {
              "type": 1,
              "gifType": 1,
              "name": "活动测试",
              "imgUrl": "www.img",
              "totalPrice": "999.99",
              "stock": 222222222,
              "status": 2,
              "isBuyGif": true,
              "isCanMaterialReplace": true,
              "quotaMode":1,
              "deployType": 1,
              "maxBuyCnt":"220",
              "bigGoodsList": [{
                  "name": "金桔精1.29kg*10桶每件内送(800g+150g)立白超洁熏衣香洗衣液1",
                  "materialCode": "80300626207",
                  "unit": "CAR",
                  "price": "999.99",
                  "type": 1
              }],
              "provincialDistributionVos":[{
                  "provincialName":"粤西省区",
                  "provincialCode":"1260",
                  "distributedVolum":20
              }]

          }*/

    /**
     * 保存（修改，新增）
     *
     * @param bigPromotionInfoVo 保存的大礼包
     * @param bindingResult
     * @return
     * @throws ParseException
     */
    @RequestMapping(value = "/saveGift", method = RequestMethod.POST)
    public ResponseData saveGift(@RequestBody @Valid BigPromotionInfoVo bigPromotionInfoVo, BindingResult bindingResult) throws Exception {
        //获取错误信息
        List<ObjectError> allErrors = bindingResult.getAllErrors();
        String errorStr = "";
        //获取错误信息拼接到字符串
        for (ObjectError error :
                allErrors) {
            errorStr += error.getDefaultMessage();
        }
        //如果有错误，则直接返回错误信息
        if (!"".equals(errorStr))
            return new ResponseData(500, errorStr);
        //参数校验开始
        //如果是买赠，则不能添加赠品
        if (bigPromotionInfoVo.getIsBuyGif() != true) {
            List<BigGoodsVo> bigGoodsVos = bigPromotionInfoVo.getBigGoodsList();
            for (BigGoodsVo obj :
                    bigGoodsVos) {
                if (obj.getType() == 2) {
                    return new ResponseData(500, "类型为不能买赠，不能存在赠品");
                }
            }
        }
        //如果是修改，则不能修改时间
        if (bigPromotionInfoVo.getId() != null) {
            if (bigPromotionInfoVo.getDeployType() == 1) {
                bigPromotionInfoVo.setDeployUpTime(null);
                bigPromotionInfoVo.setDeployDownTime(null);
            }
        }
        //如果商家类型为手动上架
        if (bigPromotionInfoVo.getDeployType() == 0) {
            if (bigPromotionInfoVo.getDeployUpTime() != null
                    || bigPromotionInfoVo.getDeployDownTime() != null) {
                return new ResponseData(500, "类型为手动上下架不能添加开始结束时间");
            }
        } else {//否则
            if (bigPromotionInfoVo.getDeployUpTime() == null
                    || bigPromotionInfoVo.getDeployDownTime() == null) {
                return new ResponseData(500, "开始和结束时间不能为空");
            }
        }

        if (bigPromotionInfoVo.getQuotaMode() == 1) {
            if (!bigPromotionInfoVo.getCustomerDistributionVos().isEmpty() || !bigPromotionInfoVo.getPromGradeQuotaVos().isEmpty()) {
                return new ResponseData(500, "你已选择统一配额，不能添加其他配额方式");
            }
        }

        if (bigPromotionInfoVo.getQuotaMode() == 2) {
            if (!bigPromotionInfoVo.getCustomerDistributionVos().isEmpty() || bigPromotionInfoVo.getMaxBuyCnt() != null) {
                return new ResponseData(500, "你已选择星级配额，不能添加其他配额方式");
            }
        }
        if (bigPromotionInfoVo.getQuotaMode() == 3) {
            if (!bigPromotionInfoVo.getPromGradeQuotaVos().isEmpty() || bigPromotionInfoVo.getMaxBuyCnt() != null) {
                return new ResponseData(500, "你已选择屁客户配额，不能添加其他配额方式");
            }
        }
//
//        LoginUser loginUser = ContextHelper.user();
//        if (loginUser == null)
//            return new ResponseData("当前未登陆");
//            //获取当前用户
//            String userId = loginUser.getUserId();
//            if (bigPromotionInfoVo == null)
//                bigPromotionInfoVo.setCreaterId(Long.valueOf(userId));
//            else
//                bigPromotionInfoVo.setUpdaterId(Long.valueOf(userId));


        //执行添加操作
        int row = giftService.saveGift(bigPromotionInfoVo);
        if (row == 0) {
            return new ResponseData(500, "保存失败");
        }

        return new ResponseData("保存成功");
    }

    /**
     * 查询商品列表
     *
     * @return
     */

    @RequestMapping(value = "/getBigGoodsList", method = RequestMethod.POST)
    public ResponseData getBigGoodsList(@Valid PageVo pageVo, BindingResult bindingResult, @RequestBody(required = false) Material material) {
        //获取错误信息
        List<ObjectError> allErrors = bindingResult.getAllErrors();
        String errorStr = "";
        //获取错误信息拼接到字符串
        for (ObjectError error :
                allErrors) {
            errorStr += error.getDefaultMessage();
        }
        //如果有错误，则直接返回错误信息
        if (!"".equals(errorStr))
            return new ResponseData(500, errorStr);

        Map<String,Object> rs = giftService.getBigGoodsList(pageVo.getPageNo(), pageVo.getPageSize(), material);
        List<BigGoods> bigGoodsList =(List<BigGoods>) rs.get("data");
        if(bigGoodsList!=null && bigGoodsList.size()>0){
            for (int i = 0; i < bigGoodsList.size(); i++) {
                BigGoods bigGoods = giftService.getBigGoodsByCode(bigGoodsList.get(i).getMaterialCode());
                if(bigGoods!=null && bigGoods.getPrice()!=null){
                    bigGoodsList.get(i).setPrice(bigGoods.getPrice());
                }
            }
        }
        rs.put("data",bigGoodsList);
        return new ResponseData(rs);
    }

    /**
     * 港剧ID查询商品
     *
     * @param code（商品80码）
     * @return
     */
    @RequestMapping(value = "/getBigGoodsByCode", method = RequestMethod.POST)
    public ResponseData getBigGoodsByCode(String code) {
        if (code == null && !"".equals(code.trim()))
            return new ResponseData(500, "查询编码不能为空");
        JSONArray arr = new JSONArray();
        arr.add(code);
        try{
            arr = JSONArray.parseArray(code);
        }catch (Exception e){}
        Map<String,Object> rs = new HashMap<String,Object>();
        if(arr!=null&&arr.size()>0){
            for (int i = 0; i <arr.size() ; i++) {
                if(arr.get(i)!=null&&!"".equals(String.valueOf(arr.get(i)).trim())){
                    String text=String.valueOf(arr.get(i)).trim();
                    BigGoods bigGoods = giftService.getBigGoodsByCode(text);
                    if(bigGoods!=null){
                        String materialCode=bigGoods.getMaterialCode();
                        rs.put(materialCode,bigGoods.getPrice());
                    }
                }
            }
        }

        return new ResponseData(rs);
    }

    /**
     * 查询经销商列表
     *
     * @param pageVo
     * @param bindingResult（条件）
     * @param distributor
     * @return
     */
    @RequestMapping("/getDistributorList")
    public ResponseData getDistributorList(@Valid PageVo pageVo, BindingResult bindingResult, @RequestBody(required = false) Distributor distributor) {
        //获取错误信息
        List<ObjectError> allErrors = bindingResult.getAllErrors();
        String errorStr = "";
        //获取错误信息拼接到字符串
        for (ObjectError error :
                allErrors) {
            errorStr += error.getDefaultMessage();
        }
        //如果有错误，则直接返回错误信息
        if (!"".equals(errorStr))
            return new ResponseData(500, errorStr);
        Response distributorList = giftService.getDistributorList(pageVo.getPageNo(), pageVo.getPageSize(), distributor);
        return new ResponseData(distributorList.getResult());
    }

}
