package com.liby.s2b.mop.web.promotion.order.controller;

import com.github.pagehelper.PageInfo;
import com.liby.common.data.ResponseData;
import com.liby.mop.promotion.order.service.IPromotionBigOrderMainService;
import com.liby.promotion.order.export.dto.QureyOrderRequest;
import com.liby.promotion.order.export.dto.QureyOrderResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
@RequestMapping("/promotion")
public class PromotionController {

    private static Logger logger = LoggerFactory.getLogger(PromotionController.class);
    @Autowired
    private IPromotionBigOrderMainService iPromotionBigOrderMainService;

    public PromotionController(){
        System.out.println(123);
    }
    /**
     * 查询大礼包订单
     * @param qureyOrderRequest
     * @return
     */
    @RequestMapping(value = "/queryGiftPacksOrder",method = RequestMethod.POST)
    @ResponseBody
    public ResponseData queryGiftPacksOrder(@RequestBody QureyOrderRequest qureyOrderRequest) {
        PageInfo<QureyOrderResponse> pageInfo =
                iPromotionBigOrderMainService.queryGiftPacksOrder(qureyOrderRequest);
        return new ResponseData(pageInfo);
    }

    /**
     * 导出大礼包订单
     * @return
     */
    @RequestMapping(value = "/exportGiftPacksOrder",method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> exportGiftPacksOrder(@RequestBody QureyOrderRequest qureyOrderRequest, HttpServletRequest request,
                                                       HttpServletResponse response){
        try {
            List<QureyOrderResponse> qureyOrderResponses =
                    iPromotionBigOrderMainService.exportGiftPacksOrder(qureyOrderRequest);
            return iPromotionBigOrderMainService.exportExcel(request,response,qureyOrderResponses);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
